using UnityEngine;
using System.Collections;

public class BlockCreator : MonoBehaviour {
	
	public GameObject blockPrefab;
	public Camera cam;
	
	private int timer;
	private bool wasClicked;
	private GameObject mainPlayer;
	
	void createBlock() {
		Ray ray = cam.ScreenPointToRay(Input.mousePosition);
		RaycastHit hit;
		int layerMask = 1 << 8;  // 8 = terrain layer
		if(Physics.Raycast(ray, out hit, 1000, layerMask)) {
			// Hit the terrain.  Create a box.
			Vector3 blockSpawnPoint = new Vector3(hit.point.x, hit.point.y + 50F, hit.point.z);
			Network.Instantiate(blockPrefab, blockSpawnPoint, Quaternion.identity, 0);
		}
	}

	// Use this for initialization
	void Start () {
		mainPlayer = GameObject.FindGameObjectWithTag("Player");
		timer = 0;
		wasClicked = false;
	}
	
	// Update is called once per frame
	void Update () {
		if(networkView.isMine) {
			this.transform.position = mainPlayer.transform.position;
			
			if(timer==20) {
				wasClicked = false;
				timer++;
			}
			
			if(wasClicked)
				timer++;
			
			if(Input.GetMouseButtonDown(0)) {
				if(!wasClicked) {
					wasClicked = true;
					timer = 0;
					createBlock();
				}
			}
		}  else {
			cam.enabled = false;
		}
	}
}