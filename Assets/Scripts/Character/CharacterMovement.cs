using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Collider))]
[RequireComponent (typeof (NetworkView))]
[RequireComponent (typeof (Rigidbody))]

public class CharacterMovement : MonoBehaviour {

	public string steeringAxis;
	public string forwardAxis;
	public string backwardAxis;
	public string resetButton;
	public Transform centerPoint;
	public float groundCheckDistance = 5.0f;
	
	private float disorientTimer;
	private bool disoriented;
	
	public float acceleration; //how fast do we accelerate?
	public float friction; //how fast do we naturally decelerate?
	public float topSpeed; //how fast are we allowed to go?
	public float sensitivity; //how quickly do we turn?
	public float adjustSpeed; //how quickly do we adjust to terrain?
	
	public bool frozen = false;
	public bool shouldUseGameValues = false;
	
	private bool startGame = false;
	private Vector3 startPosition;	
	private Vector3 targetNormal;
	private Quaternion fromRotation;
	private Quaternion toRotation;
	private float weight = 1.0f;
	
	private float topSpeedMultiplier = 1F;
	private float topSpeedDuration;
	
	public const string TOGGLE_CONTROLS = "Toggle Controls";
	
	public bool useAccel = false;
	
	private float tempFriction;
	public bool grounded = true;
	
	private bool limitAcceleration = false;
	
	void Start() {
		rigidbody.freezeRotation = true;
		startPosition = transform.position;
		rigidbody.interpolation = RigidbodyInterpolation.Interpolate;
		tempFriction = friction;
		// Minimap-related do.  All characters share this script so it goes here!
		GameObject.Find("MinimapGUI").GetComponent<MinimapGUI>()
			.setCharacter(this.gameObject, networkView.isMine==true);
	}
	
	public void Enable() {
		startGame = true;
	}
	
	public void Freeze() {
		frozen = true;
		friction = 1.0f;
	}
	
	public void Unfreeze() {
		frozen = false;
		friction = tempFriction;
	}
	
	void FixedUpdate () {
		if(startGame && networkView.isMine) {
			CountdownMovementAugments();
			//if (shouldUseGameValues)
			//	PullGameValues();						
			RotateCharacter();			
			AdjustToTerrain();
			
			if(!frozen) {
				Vector3 localVelocity = ApplyFriction(transform.InverseTransformDirection(rigidbody.velocity));
				localVelocity = Accelerate(localVelocity);
				
				Vector3 worldVelocity = transform.TransformDirection(localVelocity);
				//if we are in a hill zone AND we are moving upwards
				if(limitAcceleration && worldVelocity.y > 0 && gameObject.tag == "Player") {
					float currentYVelocity = rigidbody.velocity.y;
					worldVelocity.y = Mathf.Lerp(currentYVelocity, localVelocity.y, 0.2f);
				}
				
				rigidbody.velocity = worldVelocity;
			}
		} 
	}
	
	void Update () {
		if(networkView.isMine) {
			ToggleControls();
		}
		
		if(Input.GetButtonDown(resetButton)) {
			Reset();
		}
	}
	
	private void CountdownMovementAugments() {
		if(topSpeedDuration != 0) {
			topSpeedDuration -= Time.deltaTime;
		}
		if(topSpeedDuration < 0) {
			topSpeedDuration = 0;
			topSpeed /= topSpeedMultiplier;
		}
			
		if(disorientTimer > 0.0f) {
			disorientTimer -= Time.deltaTime;
		} else {
			disorientTimer = 0.0f;
			disoriented = false;
		}
	}
	
	private void RotateCharacter() {
		if (!frozen){
			Vector3 angles = transform.rotation.eulerAngles;
			float steeringModifier = disoriented ? -(Mathf.Abs(Mathf.Sin(3.0f * disorientTimer))) : 1.0f;
			float input = useAccel ? Input.acceleration.x : Input.GetAxis(steeringAxis);
			angles.y += input * sensitivity * steeringModifier;
			transform.localEulerAngles = angles;
		}
	}
	
	private void AdjustToTerrain() {
		RaycastHit hit;
		if( Physics.Raycast ( centerPoint.position, -centerPoint.up, out hit, groundCheckDistance ) ) {
			if(hit.normal != transform.up) {
				if(hit.normal != targetNormal) {
					targetNormal = transform.InverseTransformDirection(hit.normal);
					fromRotation = transform.rotation;
					toRotation = Quaternion.FromToRotation(Vector3.up, targetNormal);
					Vector3 temp = toRotation.eulerAngles;
					temp.y = transform.localEulerAngles.y;
					toRotation.eulerAngles = temp;
					weight = 0;
				}
				if(weight <= 1) {
					weight += Time.deltaTime * (limitAcceleration ? adjustSpeed / 2.0f : adjustSpeed);
					transform.rotation = Quaternion.Slerp(fromRotation, toRotation, weight);
				}
			}
		}
	}
	
	private Vector3 ApplyFriction(Vector3 velocity) {
		velocity.x *= friction;
		velocity.z *= friction;
		return velocity;
	}
	
	private Vector3 Accelerate(Vector3 velocity) {
		float accelerationValue = grounded ? 1.0f : 0.8f;
		
		Vector3 rotation = transform.rotation.eulerAngles;
		float pitch = rotation.x > 180F ? rotation.x - 360F : rotation.x;
		
		if(pitch < 0) {
			accelerationValue *= Mathf.Clamp (-Mathf.Log (-pitch / 30.0f), 0.0f, 1.0f);
		}
		
		if(useAccel) {
			if(Input.acceleration.y < 0.0f) {
				accelerationValue += (Mathf.Clamp(Input.acceleration.y, -1.0f, -0.5f) + 0.5f) / 0.5f;
			}
		} else {
			float input = Input.GetAxis (backwardAxis);
			if(Input.GetButton ("Idiot Button")) {
				input = 1.0f;	
			}
			accelerationValue -= input;
		}
		accelerationValue = Mathf.Clamp(accelerationValue, 0.3f, 1.0f);
		Vector3 accelerationVector = Vector3.forward * accelerationValue * acceleration;
		velocity += accelerationVector;
		velocity.z = Mathf.Clamp(velocity.z, -topSpeed, topSpeed);
		return velocity;
	}
	
	/*private void PullGameValues() {
		acceleration = GameValues.floatValues["acceleration"];
		adjustSpeed = GameValues.floatValues["adjustSpeed"];
		friction = GameValues.floatValues["friction"];
		sensitivity = GameValues.floatValues["sensitivity"];
		topSpeed = GameValues.floatValues["topSpeed"] * topSpeedMultiplier;
	}*/
	
	public void Reset() {
		networkView.RPC("ResetRPC", RPCMode.All);
	}
	
	[RPC]
	private void ResetRPC() {
		transform.position = startPosition;
	}
	
	public void IncreaseTopSpeed(float multiplier, float duration) {
		topSpeedMultiplier = multiplier;
		topSpeed *= multiplier;
		topSpeedDuration = duration;
	}
	
	public void Disorient(float duration) {
		disoriented = true;
		disorientTimer = duration;
	}
	
	// PRESS X ON THE KEYBOARD TO TOGGLE CONTROLS BETWEEN KEYBOARD AND CONTROLLER
	public void ToggleControls() {
		if (Input.GetButtonDown(TOGGLE_CONTROLS)){
			if (steeringAxis == "Character Turn"){
				steeringAxis = "Character Turn Keyboard";
				forwardAxis = "Character Forward Keyboard";
				backwardAxis = "Character Backward Keyboard";
				CharacterLight.useMouse = true;
			}
			else {
				steeringAxis = "Character Turn";
				forwardAxis = "Character Forward";
				backwardAxis = "Character Backward";
				CharacterLight.useMouse = false;
			}
		}
	}
	
	void OnCollisionEnter(Collision info) {
		if(networkView.isMine && info.collider.tag == "Terrain") {
			networkView.RPC("SetGrounded", RPCMode.All, true);
		}
	}
	
	void OnCollisionExit(Collision info) {
		if(networkView.isMine && info.collider.tag == "Terrain") {
			networkView.RPC("SetGrounded", RPCMode.All, false);
		}
	}
	
	[RPC]
	void SetGrounded(bool value) {
		grounded = value;
	}
	
	void OnTriggerEnter(Collider other) {
		if(networkView.isMine && other.tag == "Hills") {
			limitAcceleration = true;
		}
	}
	
	void OnTriggerExit(Collider other) {
		if(networkView.isMine && other.tag == "Hills") {
			limitAcceleration = false;
		}
	}
}
