using UnityEngine;
using System.Collections;

public class LagInterpolation : MonoBehaviour {

	public float updateTimer = 1.0f;
	public float adjustSpeed = 5.0f;
	public float closeness = 0.01f;
	private float timer = 0.0f;
	private Vector3 target;
	private Quaternion targetRot;
	private bool firstUpdate = true;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(networkView.isMine) {
			if(timer <= 0.0f) {
				networkView.RPC("UpdatePosition", RPCMode.Others, transform.position, transform.rotation);
				timer = updateTimer;
			} else {
				timer -= Time.deltaTime;
			}
		} else if(Mathf.Abs(transform.position.magnitude - target.magnitude) > closeness) {
			transform.position = Vector3.Lerp(transform.position, target, Time.deltaTime * adjustSpeed);
		}
	}
	
	[RPC]
	void UpdatePosition(Vector3 newPosition, Quaternion rotation) {
		if(firstUpdate) {
			transform.position = newPosition;
			target = newPosition;
			firstUpdate = false;
		}
		target = newPosition;
		transform.rotation = rotation;
	}
}
