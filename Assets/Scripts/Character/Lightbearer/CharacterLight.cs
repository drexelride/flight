using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;

public class CharacterLight : MonoBehaviour {

	private Dictionary<BeaconBehaviour, bool> beacons;
	private Vector3 point = Vector3.zero;
	public float degreesModifier = 1.0f;
	private Vector2 cursorPos = new Vector2(Screen.width/2, Screen.height/2);
	public float cursorSpeed = 1.0f;
	public Texture2D circleTexture;
	public MenuItem batteryText;
	public bool on = false;
	public float charge = 100F;
	public float maxCharge = 100F;
	public float drainSpeed = 5F;
	public float chargeSpeed = 15F;
	public static bool useMouse = false;
	public bool isShiningOnBeacon = false;
	public bool burntOut = false;
	public FillBarModel batteryMeter;
	
	private bool startGame = false;
	private bool endGame = false;
	
	private GUISkin batteryBG;
	private Texture2D batteryBGNormal, batteryBGDepleted;
	
	//Wii Attributes
	public bool useWiiRemote = true;
	//private float yOffset = (float) Screen.height;
	
	public AudioClip deadClip;
	public AudioClip revivedClip;
	
	private AudioClip originalClip;

	// Use this for initialization
	void Start () {
		beacons = new Dictionary<BeaconBehaviour, bool>();
		GetComponent<Light>().enabled = on;
		RotateLight();
		batteryText = GameObject.FindGameObjectWithTag("GUI").transform.FindChild("Battery")
			.GetComponentInChildren<MenuItem>();
		batteryMeter = GameObject.FindGameObjectWithTag("GUI").transform.FindChild("Battery")
			.GetComponentInChildren<FillBarModel>();
		batteryMeter.SetMaxUnits(maxCharge);
		batteryMeter.SetUnits(charge);
		
		batteryBG = Resources.Load("LightbearerHUDBackground") as GUISkin;
		batteryBGNormal = Resources.Load("LightbearerBG") as Texture2D;
		batteryBGDepleted = Resources.Load("LightbearerBGDepleted") as Texture2D;
		if (batteryBG && batteryBGNormal){
			batteryBG.box.normal.background = batteryBGNormal;
		}
		originalClip = audio.clip;
	}
	
	public void Enable() {
		startGame = true;
	}
	
	void OnGUI() {
		if(startGame && networkView.isMine) {
			float height = 0F;
			if(cursorPos.y >= Screen.height/2) {
				height = Screen.height/2 - Mathf.Abs(Screen.height/2 - cursorPos.y);
			} else {
				height = Screen.height/2 + Mathf.Abs(Screen.height/2 - cursorPos.y);
			}
			GUI.DrawTexture(new Rect(cursorPos.x - 10, height - 10, 20, 20), circleTexture);
		}
	}
	
	private void DetermineLightState() {
		bool buttonWasPressed = Input.GetButton("Toggle Light") || Wiimote.GetButtonA(0);
		if(buttonWasPressed && !burntOut && !endGame) {
			//turn on the light if it isn't already on
			if(!on) {
				networkView.RPC("SetLightOn", RPCMode.All, true);
			}
		}
		else if(on) { //the button has been released, the game has ended, or the battery has died.
			//turn off the light
			networkView.RPC("SetLightOn", RPCMode.All, false);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(startGame && networkView.isMine) {
			MoveCursor();
			//batteryText.text = "Battery: " + (int)charge + "/" + (int)maxCharge;
			batteryMeter.SetUnits(charge);
			
			DetermineLightState(); //should the light turn on or off?
			
			if(on) {
				RotateLight();
				Ray detectionRay = new Ray(transform.position, (point - transform.position).normalized);
				foreach(BeaconBehaviour key in beacons.Keys.ToList()) {
					beacons[key] = false;
				}
				bool didHit = false;
				didHit = DetectBeacon(detectionRay, Vector3.zero, 0.0f) || didHit;
				didHit = DetectBeacon(detectionRay, Vector3.left, degreesModifier) || didHit; //up
				didHit = DetectBeacon(detectionRay, Vector3.left, -degreesModifier) || didHit; //down
				didHit = DetectBeacon(detectionRay, Vector3.up, degreesModifier) || didHit; //right
				didHit = DetectBeacon(detectionRay, Vector3.up, -degreesModifier) || didHit; //left
				isShiningOnBeacon = didHit;
				foreach(BeaconBehaviour key in beacons.Keys.ToList()) {
					if(!beacons[key]) {
						key.StopLighting();
						beacons.Remove(key);
					}
				}
				charge -= drainSpeed * Time.deltaTime;
				if(charge <= 0) {
					networkView.RPC("SetLightOn", RPCMode.All, false);
					burntOut = true;
					audio.clip = deadClip;
					audio.loop = true;
					audio.Stop ();
					audio.Play (); //are you being serious with me right now, Unity?
					audio.Stop (); //I have to play the sound, then stop it, then play it again in order to make it loop
					audio.Play ();
					batteryText.text = "Recharging...";
					if (batteryBG && batteryBGDepleted){
						batteryBG.box.normal.background = batteryBGDepleted;
					}
				}
			} else {
				foreach(BeaconBehaviour key in beacons.Keys.ToList()) {
					key.StopLighting();
					beacons.Remove(key);
				}
				charge += chargeSpeed * Time.deltaTime;
				if(burntOut && charge >= 100f) {
					burntOut = false;
					batteryText.text = "";
					audio.Stop ();
					audio.clip = originalClip;
					audio.loop = true;
					audio.PlayOneShot (revivedClip);
					if (batteryBG && batteryBGNormal){
						batteryBG.box.normal.background = batteryBGNormal;
					}
				}
				isShiningOnBeacon = false;
			}
			charge = Mathf.Clamp(charge, 0, maxCharge);
		}
	}
	
	[RPC]
	private void SetLightOn(bool enabled) {
		on = enabled;
		GetComponent<Light>().enabled = on;
		ParticleSystem particle = GetComponent<ParticleSystem>();
		if(on) {
			particle.Stop();
			particle.Play();
			if(networkView.isMine) {
				audio.Stop ();
				audio.Play ();
			}
		} else {
			particle.Stop();
			particle.Clear();
			if(networkView.isMine) {
				audio.Stop ();
			}
		}
		//if(!networkView.isMine) {
			//GetComponent<LineRenderer>().enabled = on;
		//}
	}
	
	private void MoveCursor() {
		if(useWiiRemote) {
			float screenX = cursorPos.x;
			float screenY = cursorPos.y;
			
			Vector2 ir = Wiimote.GetIR(0);	//values are -1.0 to 1.0
			if(ir.x != -100.0 && ir.y != -100.0) {				
				if(ir.x < 0) {
					//in this case, 1.0 is the left side of the screen
					screenX = Mathf.Lerp(Screen.width / 2.0f, 0, Mathf.Abs(ir.x));
				} else {
					//in this case, 1.0 is the right side of the screen
					screenX = Mathf.Lerp(Screen.width / 2.0f, (float)Screen.width, Mathf.Abs(ir.x));
				}
				
				if(ir.y < 0) {
					screenY = Mathf.Lerp(Screen.height / 2.0f, 0, Mathf.Abs(ir.y));
				} else {
					screenY = Mathf.Lerp(Screen.height / 2.0f, (float)Screen.height, Mathf.Abs(ir.y));
				}
			}
			cursorPos = Vector2.Lerp(cursorPos, new Vector2(screenX, screenY), Time.deltaTime * 20.0f);
		} else if(useMouse) {
			cursorPos = Input.mousePosition;
		} else {
			float horizontal = Input.GetAxis("Lightbearer Light Horizontal");
			float vertical = Input.GetAxis("Lightbearer Light Vertical");
			cursorPos += new Vector2(horizontal, -vertical) * cursorSpeed;
			cursorPos.x = Mathf.Clamp(cursorPos.x, 0, Screen.width);
			cursorPos.y = Mathf.Clamp(cursorPos.y, 0, Screen.height);
		}
	}
	
	private void RotateLight() {
		RaycastHit hit;
		Ray ray = transform.parent.Find("Camera").GetComponent<Camera>().ScreenPointToRay(cursorPos);
		//this one's for rotating the spotlight
		if(Physics.Raycast(ray, out hit, 50000.0f)) {
			transform.LookAt(hit.point);
			point = hit.point;
			//networkView.RPC("RotateLaser", RPCMode.Others, hit.point, hit.distance);
		}
	}
	
	[RPC]
	private void RotateLaser(Vector3 to, float distance) {
		LineRenderer r = GetComponent<LineRenderer>();
		r.SetPosition(0, transform.position);
		r.SetPosition(1, to);
	}
	
	private bool DetectBeacon(Ray centerRay, Vector3 around, float modifier) {
		Light spotlight = GetComponent<Light>();
		float degrees = spotlight.spotAngle * modifier;
		Vector3 newDirection = Quaternion.AngleAxis(degrees, around) * centerRay.direction;
		Ray newRay = new Ray(centerRay.origin, newDirection);
		RaycastHit hit;
		bool didHit = false;
		if(Physics.Raycast(newRay, out hit, spotlight.range)) {
			if(hit.collider.tag == "Beacon") {
				didHit = !hit.collider.GetComponent<BeaconBehaviour>().GetFilled();
				BeaconBehaviour beacon = hit.transform.GetComponent<BeaconBehaviour>();
				if(!beacons.ContainsKey(beacon)) {
					beacons.Add(beacon, true);
					beacon.BeginLighting();
				} else {
					beacons[beacon] = true;
				}
			} else if(hit.collider.tag == "Shadow") {
				hit.collider.GetComponent<Shadow>().Damage();
			}
		}
		return didHit;
	}
	
	void OnDrawGizmos() {
		if(startGame && networkView.isMine) {
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(point, 5.0f);
		}
	}
	
	public void SetEndGame(bool end){
		endGame = end;
	}
}
