using UnityEngine;
using System.Collections;
using System.Runtime.InteropServices;

public class Wiimote : MonoBehaviour {

//	IF YOU DON'T WANT ANY WIIMOTE STUFF TO HAPPEN,

//***********DISABLE THIS COMPONENT!!!********************

	#if UNITY_STANDALONE

	[DllImport ("UniWii")]
	private static extern void wiimote_start();
 
	[DllImport ("UniWii")]
	private static extern void wiimote_stop();
 
	[DllImport ("UniWii")]
	private static extern int wiimote_count();

	[DllImport ("UniWii")]	
	private static extern bool wiimote_enableIR( int which );

	[DllImport ("UniWii")]
	private static extern float wiimote_getIrX(int which);
	
	[DllImport ("UniWii")]
	private static extern float wiimote_getIrY(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonA(int which);
	
	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonUp(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonLeft(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonRight(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonDown(int which);
	
	#else
	

	private static void wiimote_start() {
		//noop
	}
 
	private static void wiimote_stop() {
		//noop
	}
 
	private static int wiimote_count() {
		return 0;
	}

	private static bool wiimote_enableIR( int which ) {
		return false;
	}

	private static float wiimote_getIrX(int which) {
		return 0.0f;
	}
	
	private static float wiimote_getIrY(int which) {
		return 0.0f;
	}

	private static bool wiimote_getButtonA(int which) {
		return false;
	}
	
	private static bool wiimote_getButtonUp(int which) {
		return false;
	}

	private static bool wiimote_getButtonLeft(int which) {
		return false;
	}

	private static bool wiimote_getButtonRight(int which) {
		return false;
	}

	private static bool wiimote_getButtonDown(int which) {
		return false;
	}
	
	#endif
	
	
	private static bool initialized = false;
	
	void Start() {
		StartCoroutine(LoadWiimotes());
		DontDestroyOnLoad(gameObject);
	}
	
	//wiimote initialization seems to be either slow or downright unreliable
	//keep trying until our wiimote_count gets something
	IEnumerator LoadWiimotes() {
		wiimote_start();
		float timer = 3.0f;
		while(wiimote_count() <= 0) {
			timer -= Time.deltaTime;
			if(timer <= 0.0f) {
				timer = 3.0f;
				wiimote_start();
			}
			yield return null;
		}
		wiimote_enableIR(0);
		initialized = true;
	}
	
	void OnApplicationQuit() {
		wiimote_stop();
	}
	
	public static Vector2 GetIR(int which) {
		if(initialized) {
			return new Vector2(wiimote_getIrX(which), wiimote_getIrY(which));
		} else {
			return Vector2.zero;
		}
	}
	
	public static bool GetButtonA(int which) {
		if(initialized) {
			return wiimote_getButtonA(which);
		} else {
			return false;
		}
	}
	
	public static bool GetDPadUp(int which) {
		if(initialized) {
			return wiimote_getButtonUp(which);
		} else {
			return false;
		}
	}
	
	public static bool GetDPadDown(int which) {
		if(initialized) {
			return wiimote_getButtonDown(which);
		} else {
			return false;
		}
	}
	
	public static bool GetDPadLeft(int which) {
		if(initialized) {
			return wiimote_getButtonLeft(which);
		} else {
			return false;
		}
	}
	
	public static bool GetDPadRight(int which) {
		if(initialized) {
			return wiimote_getButtonRight(which);
		} else {
			return false;
		}
	}
	
	
}
