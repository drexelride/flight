using UnityEngine;
using System.Collections;
 
[RequireComponent (typeof (CharacterMovement))]
[RequireComponent (typeof (NetworkView))]
 
public class SimCharacter : MonoBehaviour {

	private Camera myCamera;
	private CharacterMovement movement;
	private CharacterLight myLight;
	private Chain chainController;
	
	public float blindDuration;
	private float blindTimer;
	public Texture2D blindTexture;
	
	public float disorientDuration = 10.0f;
	private float disorientTimer;
	public Animation myAnimation;
	public float walkAnimationSpeed = 1.5f;
	
	public Transform leftFoot;
	private bool leftFootPlay = true;
	public Transform rightFoot;
	private bool rightFootPlay = true;
	
	public GameObject footstep;
	
	public bool startGame = false;
	
	public AudioClip sirenScream;
	public AudioClip hammerheadHit;
	public AudioClip genericDamage;
	
	void Start() {
		myCamera = GetComponentInChildren<Camera>();
		myCamera.enabled = false;
		myLight = GetComponentInChildren<CharacterLight>();
		movement = GetComponent<CharacterMovement>();
		chainController = GameObject.Find("Chain").GetComponent<Chain>();
		
		myAnimation["run_cycle"].speed = walkAnimationSpeed;
		if(networkView.isMine) {
			myCamera.enabled = true;
			myCamera.gameObject.AddComponent<AudioListener>();
		}
	}
	
	void Update() {
		if(networkView.isMine) {
			if(blindTimer > 0.0f) {
				blindTimer -= Time.deltaTime;
			} else {
				blindTimer = 0.0f;
			}
		}
		myAnimation["run_cycle"].speed = networkView.isMine ? DetermineAnimationSpeed() : walkAnimationSpeed;
		if(movement.grounded) {
			/*sorry for the magic numbers but this is really specific work here
			24.0 is the frame rate of the animation
			27 is the number of frames
			first convert time to a frame number by multiplying by frame rate
			then mod it with the number of frames in the animation
			to get a relative frame number (0-27)
			play footstep on the frames when the foot hits the ground*/
			float frame = (myAnimation["run_cycle"].time * 24.0f) % 27.0f;
			if(frame >= 0 && frame <= 1 && rightFootPlay) {
				Instantiate(footstep, rightFoot.position, rightFoot.rotation);
				leftFootPlay = true;
				rightFootPlay = false;
			}
			else if(frame >= 16 && frame <= 17 && leftFootPlay) {
				Instantiate(footstep, leftFoot.position, leftFoot.rotation);
				rightFootPlay = true;
				leftFootPlay = false;
			}
		}
	}
	
	private float DetermineAnimationSpeed() {
		Vector3 velocity = transform.InverseTransformDirection(rigidbody.velocity);
		float result =(Mathf.Abs(velocity.z) / movement.topSpeed) * walkAnimationSpeed;
		return result;
	}
	
	void OnGUI() {
		if(networkView.isMine) {
			GUI.color = new Color(0, 0, 0,  blindTimer * 1.5f / blindDuration);
			GUI.DrawTexture( new Rect(0, 0, Screen.width, Screen.height ), blindTexture );
		}
	}
	
	public void Enable() {
		movement.Enable();
		if(myLight != null) {
			myLight.Enable();
		}
		if (chainController){
			chainController.SetSimCharacter(this);
		}
		GameObject[] gos = GameObject.FindGameObjectsWithTag("Shadow");
		foreach(GameObject go in gos) {
			go.networkView.RPC("ShadowEnable", RPCMode.All);
		}
	}
	
	public void ResetPosition() {
		movement.Reset();
	}
	
	public CharacterLight GetMyLight(){
		return myLight;
	}
	
	public void Blind() {
		audio.PlayOneShot (genericDamage);
		blindTimer = blindDuration;
	}
	
	public void Disorient() {
		networkView.RPC("DisorientRPC", RPCMode.Server);
	}
	
	[RPC]
	private void DisorientRPC() {
		if(networkView.isMine) {
			audio.PlayOneShot (sirenScream);
			movement.Disorient(disorientDuration);
		}
	}
	
	public void Throw() {
		networkView.RPC("ThrowRPC", RPCMode.Server);
	}
	
	[RPC]
	private void ThrowRPC() {
		if(networkView.isMine) {
			gameObject.AddComponent("AnglerThrow");
		}
	}
	
	public void Hammerflip() {
		networkView.RPC("HammerflipRPC", RPCMode.Server);
	}
	
	[RPC]
	private void HammerflipRPC() {
		if(networkView.isMine) {
			audio.PlayOneShot (hammerheadHit);
			gameObject.AddComponent("Hammerflip");
		}
	}
	
	public void PlayDamageSound() {
		audio.PlayOneShot (genericDamage);	
	}
}