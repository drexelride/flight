using UnityEngine;
using System.Collections;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.InteropServices;

public class CharacterLightWii : MonoBehaviour {

	private Dictionary<BeaconBehaviour, bool> beacons;
	private Vector3 point = Vector3.zero;
	public float degreesModifier = 1.0f;
	private Vector2 cursorPos = new Vector2(Screen.width/2, Screen.height/2);
	public float cursorSpeed = 1.0f;
	public Texture2D circleTexture;
	public MenuItem batteryMeter;
	public bool on = false;
	public float charge = 100F;
	public float maxCharge = 100F;
	public float drainSpeed = 5F;
	public float chargeSpeed = 15F;
	public static bool useMouse = false;
	public bool isShiningOnBeacon = false;
	
	private bool startGame = false;

	//Wii Attributes
	public static bool useWiiRemote = false;
	private int primaryWiimote = 0;
	private float yOffset = (float) Screen.height;

	[DllImport ("UniWii")]
	private static extern void wiimote_start();
 
	//[DllImport ("UniWii")]
	//private static extern void wiimote_stop();
 
	[DllImport ("UniWii")]
	private static extern int wiimote_count();

	[DllImport ("UniWii")]	
	private static extern bool wiimote_enableIR( int which );

	[DllImport ("UniWii")]
	private static extern float wiimote_getIrX(int which);
	
	[DllImport ("UniWii")]
	private static extern float wiimote_getIrY(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonA(int which);

	// Use this for initialization
	void Start () {
		beacons = new Dictionary<BeaconBehaviour, bool>();
		GetComponent<Light>().enabled = on;
		RotateLight();
		batteryMeter = GameObject.FindGameObjectWithTag("GUI").transform.FindChild("Battery")
			.GetComponentInChildren<MenuItem>();

		//Enable IR
		if (useWiiRemote) {
			//Initialize Wiimote
			wiimote_start();

			int c = wiimote_count();
			if (c > 0)
				wiimote_enableIR(primaryWiimote);
		}
	}
	
	public void Enable() {
		startGame = true;
	}
	
	void OnGUI() {
		if(startGame && networkView.isMine) {
			float height = 0F;
			if(cursorPos.y >= Screen.height/2) {
				height = Screen.height/2 - Mathf.Abs(Screen.height/2 - cursorPos.y);
			} else {
				height = Screen.height/2 + Mathf.Abs(Screen.height/2 - cursorPos.y);
			}
			GUI.DrawTexture(new Rect(cursorPos.x - 10, height - 10, 20, 20), circleTexture);
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(startGame && networkView.isMine) {
			MoveCursor();
			batteryMeter.text = "Battery: " + (int)charge + "/" + (int)maxCharge;
			if(Input.GetButtonDown("Toggle Light") || wiimote_getButtonA(primaryWiimote)) {
				networkView.RPC("SetLightOn", RPCMode.All, !on);
			}
			if(on) {
				RotateLight();
				Ray detectionRay = new Ray(transform.position, (point - transform.position).normalized);
				foreach(BeaconBehaviour key in beacons.Keys.ToList()) {
					beacons[key] = false;
				}
				bool didHit = false;
				didHit = DetectBeacon(detectionRay, Vector3.zero, 0.0f) || didHit;
				didHit = DetectBeacon(detectionRay, Vector3.left, degreesModifier) || didHit; //up
				didHit = DetectBeacon(detectionRay, Vector3.left, -degreesModifier) || didHit; //down
				didHit = DetectBeacon(detectionRay, Vector3.up, degreesModifier) || didHit; //right
				didHit = DetectBeacon(detectionRay, Vector3.up, -degreesModifier) || didHit; //left
				isShiningOnBeacon = didHit;
				foreach(BeaconBehaviour key in beacons.Keys.ToList()) {
					if(!beacons[key]) {
						key.StopLighting();
						beacons.Remove(key);
					}
				}
				charge -= drainSpeed * Time.deltaTime;
				if(charge <= 0) {
					networkView.RPC("SetLightOn", RPCMode.All, false);
				}
			} else {
				charge += chargeSpeed * Time.deltaTime;
				isShiningOnBeacon = false;
			}
			charge = Mathf.Clamp(charge, 0, maxCharge);
		}
	}
	
	[RPC]
	private void SetLightOn(bool enabled) {
		on = enabled;
		GetComponent<Light>().enabled = on;
		if(!networkView.isMine) {
			//GetComponent<LineRenderer>().enabled = on;
		}
	}
	
	private void MoveCursor() {
		if (useWiiRemote) {
			int c = wiimote_count();
			if (c > 0) {
				float wiiIRX = wiimote_getIrX(primaryWiimote);
				float wiiIRY = wiimote_getIrY(primaryWiimote) * -1.0f;

				if ( (wiiIRX != -100) && (wiiIRY != -100) ) {
					float tmpX = ((wiiIRX + (float) 1.0)/ (float)2.0) * (float) Screen.width;
				 	float tmpY = yOffset - (((wiiIRY + (float) 1.0)/ (float)2.0) * (float) Screen.height);
					tmpX = Mathf.RoundToInt(tmpX);
					tmpY = Mathf.RoundToInt(tmpY);
					cursorPos = new Vector2(tmpX, tmpY);
				}
			}
		}
		else if(useMouse) {
			cursorPos = Input.mousePosition;
		} else {
			float horizontal = Input.GetAxis("Lightbearer Light Horizontal");
			float vertical = Input.GetAxis("Lightbearer Light Vertical");
			cursorPos += new Vector2(horizontal, -vertical) * cursorSpeed;
			cursorPos.x = Mathf.Clamp(cursorPos.x, 0, Screen.width);
			cursorPos.y = Mathf.Clamp(cursorPos.y, 0, Screen.height);
		}
	}
	
	private void RotateLight() {
		RaycastHit hit;
		Ray ray = transform.parent.Find("Camera").GetComponent<Camera>().ScreenPointToRay(cursorPos);
		//this one's for rotating the spotlight
		if(Physics.Raycast(ray, out hit, 50000.0f)) {
			transform.LookAt(hit.point);
			point = hit.point;
			//networkView.RPC("RotateLaser", RPCMode.Others, hit.point, hit.distance);
		}
	}
	
	[RPC]
	private void RotateLaser(Vector3 to, float distance) {
		LineRenderer r = GetComponent<LineRenderer>();
		r.SetPosition(0, transform.position);
		r.SetPosition(1, to);
	}
	
	private bool DetectBeacon(Ray centerRay, Vector3 around, float modifier) {
		Light spotlight = GetComponent<Light>();
		float degrees = spotlight.spotAngle * modifier;
		Vector3 newDirection = Quaternion.AngleAxis(degrees, around) * centerRay.direction;
		Ray newRay = new Ray(centerRay.origin, newDirection);
		RaycastHit hit;
		bool didHit = false;
		if(Physics.Raycast(newRay, out hit, spotlight.range)) {
			if(hit.collider.tag == "Beacon") {
				didHit = !hit.collider.GetComponent<BeaconBehaviour>().GetFilled();
				BeaconBehaviour beacon = hit.transform.GetComponent<BeaconBehaviour>();
				if(!beacons.ContainsKey(beacon)) {
					beacons.Add(beacon, true);
					beacon.BeginLighting();
				} else {
					beacons[beacon] = true;
				}
			} else if(hit.collider.tag == "Shadow") {
				hit.collider.GetComponent<Shadow>().Damage();
			}
		}
		return didHit;
	}
	
	void OnDrawGizmos() {
		if(startGame && networkView.isMine) {
			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere(point, 5.0f);
		}
	}

	/*
	void OnApplicationQuit() {
		wiimote_stop();
	}
*/
}
