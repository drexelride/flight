using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Spawner : MonoBehaviour {
	public static string character = "";
	public Transform playerSpawnPoint;
	public static Dictionary<string, NetworkPlayer> shadowNetworkPlayers;
	public static bool joined = false;
	public static bool shadowStaticFieldsInitialized = false;

	// Use this for initialization
	void Start () {
		shadowNetworkPlayers = new Dictionary<string, NetworkPlayer>();
		DontDestroyOnLoad(gameObject);
	}
	
	void OnLevelWasLoaded(int level) {
		if(Application.loadedLevelName == ConnectionManager.GAME_SCENE) {
			if (Network.isServer)
				MusicController.GetInstance().PlayBGM3();
		
			if (joined || Network.isServer){
				GameObject[] spawnPoints = GameObject.FindGameObjectsWithTag("PlayerSpawn");
				foreach(GameObject spawnPoint in spawnPoints) {
					if(spawnPoint.GetComponent<SpawnPoint>().character == character) {
						playerSpawnPoint = spawnPoint.transform;
						break;
					}
				}
			}
			else{
				playerSpawnPoint = GameObject.FindGameObjectsWithTag("NotJoinedSpawn")[0].transform;
			}
			Network.Instantiate(Resources.Load(character) as GameObject, playerSpawnPoint.position, playerSpawnPoint.rotation, 0);
		}
	}
}
