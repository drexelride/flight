using UnityEngine;
using System.Collections;

public class AnglerThrow : MonoBehaviour {
	
	public float pullDuration = 2.0f;
	public float holdDuration = 3.0f;
	public float throwForce = 50000;
	private float pullTimer = 0.0f;
	private float holdTimer = 0.0f;
	private Transform underground;
	
	private bool showAnglerScreen = true;

	// Use this for initialization
	void Start () {
		GetComponent<CharacterMovement>().Freeze();
		GetComponent<Collider>().enabled = false;
		underground = GameObject.FindWithTag("AnglerUnderground").transform;
	}
	
	// Update is called once per frame
	void Update () {
		if(networkView.isMine) {
			if(pullTimer <= pullDuration) {
				transform.position = Vector3.Lerp(transform.position, underground.position, pullTimer / pullDuration);
				pullTimer += Time.deltaTime;
			} else if(holdTimer <= holdDuration) {
				transform.position = underground.position;
				holdTimer += Time.deltaTime;
			} else {
				holdTimer = 0.0f;
				pullTimer = 0.0f;
				GetComponent<Collider>().enabled = true;
				Transform throwSpot = GameObject.FindWithTag("AnglerThrow").transform;
				transform.position = throwSpot.position;
				rigidbody.AddForce(throwSpot.forward * throwForce);
				GetComponent<CharacterMovement>().Unfreeze();
				Destroy(this);
			}
		}
	}
	
	void OnGUI() {
		if(showAnglerScreen) {
			GUI.color = new Color(1.0f, 1.0f, 1.0f, pullTimer / 0.5f);
			GUI.DrawTexture (new Rect(0, 0, Screen.width, Screen.height), Resources.Load ("AnglerGrab") as Texture2D);
		}
	}
}
