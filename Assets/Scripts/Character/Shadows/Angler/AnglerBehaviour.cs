using UnityEngine;
using System.Collections;

public class AnglerBehaviour : Shadow {

	public Targeting targeting;
	public float holdTimer = 5.0f;
	public float attackWindup = 0.792f;

	private bool attacking;
	private float windupTimer = 0.0f;
	
	protected override void Init() {
		modelAnimation["run_cycle"].wrapMode = WrapMode.Loop;
		this.maxHealth = 175f;
		this.abilityCooldown = 10f;		
	}
	
	protected override void ShadowUpdate () {
		if(networkView.isMine) {
			if(UseAbility()) {
				PlayAttack();
				attacking = true;
				windupTimer = attackWindup;
			}
			if(attacking && windupTimer <= 0.0f) {
				if(targeting.playerInRange) {
					ShowDamageToLightbearer();
					PlayHit();
					StartCoroutine(ThrowCharacter());
					score.AddToScore(pointsPerAttack);
					showPoints = true;
					StartCoroutine(PlayDelayedWalk(holdTimer));
				} else {
					PlayMiss();
					StartCoroutine(PlayDelayedWalk(1.5f));
					movement.Freeze();
				}
				attacking = false;
			} else if(attacking) {
				windupTimer -= Time.deltaTime;
			}
		}
	}
	
	private IEnumerator PlayDelayedWalk(float time) {
		yield return new WaitForSeconds(time);
		movement.Unfreeze();
		PlayWalk();
	}
	
	private IEnumerator ThrowCharacter() {
		yield return new WaitForSeconds(0.5f);
		GameObject.FindWithTag("Player").GetComponent<SimCharacter>().Throw();
		yield return new WaitForSeconds(5.0f);
		PlayAttack ();
		yield return new WaitForSeconds(attackWindup);
		PlayWalk ();
	}
	
	private void PlayAttack() {
		networkView.RPC("BlendAnimation", RPCMode.All, "attack_launch");
	}
	
	private void PlayMiss() {
		networkView.RPC("BlendAnimation", RPCMode.All, "attack_miss");
	}
	
	private void PlayHit() {
		networkView.RPC("BlendAnimation", RPCMode.All, "attck_hit");
	}
	
	private void PlayWalk() {
		networkView.RPC("BlendAnimation", RPCMode.All, "run_cycle");
	}
	
	[RPC]
	private void BlendAnimation(string animationName) {
		modelAnimation.Play(animationName);
	}
}
