using UnityEngine;
using System.Collections;

public class RoachBehaviour : Shadow {

	public float hitForce = 50000F;
	public float tackleForce = 20000F;
	public bool isAttacking = false;
	private bool didScore = false;
	

	// Use this for initialization
	protected override void Init () {
		modelAnimation["run_cycle"].wrapMode = WrapMode.Loop;
		modelAnimation["run_cycle"].speed = runAnimationSpeed;
	}
	
	// Update is called once per frame
	protected override void ShadowUpdate () {
		if(networkView.isMine && UseAbility()) {
			StartCoroutine(Attack());
		}
	}
	
	void OnCollisionEnter(Collision c) {
		if(!networkView.isMine && isAttacking && c.collider.tag == "Player") {
			c.collider.rigidbody.AddForce(c.contacts[0].normal * -hitForce);
		}
		else if (networkView.isMine && isAttacking && c.collider.tag == "Player" && !didScore) {
			ShowDamageToLightbearer();
			score.AddToScore(pointsPerAttack);
			didScore = true;
			showPoints = true;
		}
	}
	
	private IEnumerator Attack() {
		PlayLaunch();
		GetComponent<CharacterMovement>().IncreaseTopSpeed(3.0f, 1.2f);
		rigidbody.AddForce(transform.forward * tackleForce);
		networkView.RPC("IsAttacking", RPCMode.All, true);
		yield return new WaitForSeconds(1.2f);
		PlayRun();
		networkView.RPC("IsAttacking", RPCMode.All, false);
		didScore = false;
	}
	
	private void PlayLaunch() {
		networkView.RPC("PlayAnimation", RPCMode.All, "attack_launch");
	}
	
	private void PlayRun() {
		networkView.RPC("BlendAnimation", RPCMode.All, "run_cycle");
	}
	
	[RPC]
	private void IsAttacking(bool value) {
		isAttacking = value;
	}
	
	[RPC]
	private void PlayAnimation(string animationName) {
		modelAnimation.CrossFade(animationName);
	}
	
	[RPC]
	private void BlendAnimation(string animationName) {
		modelAnimation.Blend(animationName);
	}
}
