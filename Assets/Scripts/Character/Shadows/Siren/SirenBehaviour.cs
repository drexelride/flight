using UnityEngine;
using System.Collections;

public class SirenBehaviour : Shadow {

	public Targeting targeting;
	
	
	public float windupDuration = 1.0f;
	public float attackDuration = 0.33f;
	public float flopDuration = 1.0f;

	// Use this for initialization
	protected override void Init () {
		modelAnimation["run_cycle"].wrapMode = WrapMode.Loop;
		modelAnimation["attack"].speed = 2.0f;
	}
	
	// Update is called once per frame
	protected override void ShadowUpdate () {
		if(networkView.isMine && UseAbility()) {
			StartCoroutine(Attack());
		}
	}
	
	private IEnumerator Attack() {
		PlayAttack();
		movement.Freeze();
		yield return new WaitForSeconds(windupDuration);
		audio.Play ();
		networkView.RPC("PlayParticle", RPCMode.All);
		if(targeting.playerInRange) {
			ShowDamageToLightbearer();
			GameObject.FindWithTag("Player").GetComponent<SimCharacter>().Disorient();
			score.AddToScore(pointsPerAttack);
			showPoints = true;
		}
		yield return new WaitForSeconds(attackDuration);
		PlayFlop();
		yield return new WaitForSeconds(flopDuration);
		movement.Unfreeze();
		PlayWalk();
	}
	
	[RPC]
	private void PlayParticle() {
		Transform target = transform.Find("Target");
		target.particleSystem.Stop();
		target.particleSystem.Play();
	}
	
	private void PlayWalk() {
		networkView.RPC("PlayAnimation", RPCMode.All, "run_cycle");
	}	
	
	private void PlayAttack() {
		networkView.RPC("PlayAnimation", RPCMode.All, "attack");
	}
	
	private void PlayFlop() {
		networkView.RPC("PlayAnimation", RPCMode.All, "flop");
	}
	
	[RPC]
	private void PlayAnimation(string animationName) {
		modelAnimation.CrossFade(animationName);
	}
}
