using UnityEngine;
using System.Collections;

public class Hammerflip : MonoBehaviour {

	float timer = 0.0f;

	// Use this for initialization
	void Start () {
		rigidbody.AddForce(Vector3.up * 3000);
	}
	
	// Update is called once per frame
	void Update () {
		timer += Time.deltaTime;
		transform.Rotate(0, 540 * Time.deltaTime, 0);
		if(timer >= 1.0f) {
			Destroy(this);
		}
	}
}
