using UnityEngine;
using System.Collections;

public class HammerheadBehaviour : Shadow {

	public GameObject earthShard;
	public Targeting targeting;
	public float attackDelay = 0.8f;
	
	protected override void Init() {
		modelAnimation["run_cycle"].wrapMode = WrapMode.Loop;
		modelAnimation["attack"].speed = 2.0f;
	}
	
	// Update is called once per frame
	protected override void ShadowUpdate () {
		if(networkView.isMine && UseAbility()) {
			StartCoroutine(Attack());
		}
	}
	
	private IEnumerator Attack() {
		movement.Freeze();
		PlayAttack();
		yield return new WaitForSeconds(attackDelay);
		audio.Play ();
		networkView.RPC("PlayParticle", RPCMode.All);
		if(targeting.playerInRange) {
			ShowDamageToLightbearer();
			Transform player = GameObject.FindWithTag("Player").transform;
			player.GetComponent<SimCharacter>().Hammerflip();
			score.AddToScore(pointsPerAttack);
			showPoints = true;
		}
		movement.Unfreeze();
		PlayRun();
	}
	
	[RPC]
	private void PlayParticle() {
		Transform target = transform.Find("Target");
		target.particleSystem.Stop();
		target.particleSystem.Play();
	}
	
	private void PlayAttack() {
		networkView.RPC("BlendAnimation", RPCMode.All, "attack", 0.3f);
	}
	
	private void PlayRun() {
		networkView.RPC("BlendAnimation", RPCMode.All, "run_cycle", 0.3f);
	}
	
	[RPC]
	private void PlayAnimation(string animationName) {
		modelAnimation.Play(animationName);
	}
	
	[RPC]
	private void BlendAnimation(string animationName, float duration) {
		modelAnimation.CrossFade(animationName, duration);
	}
	
	
}
