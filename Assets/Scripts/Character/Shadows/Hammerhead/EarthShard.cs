using UnityEngine;
using System.Collections;

public class EarthShard : MonoBehaviour {

	public float riseSpeed = 5.0f;
	public float riseTime = 1.0f;
	public float stayTime = 10.0f;
	
	// Update is called once per frame
	void Update () {
		if(networkView.isMine) {
			if(riseTime > 0.0f) {
				transform.Translate(0F, riseSpeed, 0F);
				riseTime -= Time.deltaTime;
			} else if(stayTime > 0.0f) {
				stayTime -= Time.deltaTime;
			} else {
				Network.Destroy(gameObject);
			}
		}
	}
}
