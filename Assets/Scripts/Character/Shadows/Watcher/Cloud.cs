using UnityEngine;
using System.Collections;

public class Cloud : MonoBehaviour {

	public float duration = 10.0f;
	private float timer;
	private Shadow shadow;
	private bool hasScored = false;
	
	void Awake() {
		timer = duration;
	}
	
	void Update() {
		timer -= Time.deltaTime;
		if(timer <= 0.0f) {
			Destroy(gameObject);
		}
	}

	void OnTriggerStay(Collider other) {
		if(other.tag == "Player") {
			other.GetComponent<SimCharacter>().Blind();
			if (shadow){
				if (!hasScored){
					shadow.ShowDamageToLightbearer();
					shadow.GetShadowScore().AddToScore(shadow.pointsPerAttack);
					hasScored = true;
					shadow.SetShowPoints(true);
				}
			}
		}
	}
	
	public void SetShadow(Shadow shadow){
		this.shadow = shadow;
	}
}
