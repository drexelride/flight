using UnityEngine;
using System.Collections;

public class WatcherBehaviour : Shadow {

	public GameObject blindingCloud;
	public Transform cloudPoint;

	// Use this for initialization
	protected override void Init () {
		//no op
	}
	
	// Update is called once per frame
	protected override void ShadowUpdate () {
		if(networkView.isMine && UseAbility()) {
			StartCoroutine (Attack());
		}
	}
	
	private IEnumerator Attack() {
		movement.Freeze();
		PlayAnimation ("attack");
		GameObject cloud = (GameObject)Network.Instantiate(blindingCloud, cloudPoint.position, cloudPoint.rotation, 0);
		cloud.GetComponent<Cloud>().SetShadow(this);
		yield return new WaitForSeconds(1.0f);
		movement.Unfreeze();
		PlayAnimation ("run_cycle");
	}
	
	[RPC]
	private void PlayAnimation(string animationName) {
		modelAnimation.CrossFade (animationName);	
	}
}
