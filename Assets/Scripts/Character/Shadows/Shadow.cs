using UnityEngine;
using System.Collections;

[RequireComponent (typeof (CharacterMovement))]
[RequireComponent (typeof (NetworkView))]

public abstract class Shadow : MonoBehaviour {

	public float maxHealth = 100f;
	public float abilityCooldown = 5.0f;
	public float topSpeed = 100f;
	
	protected float damagePerFrame = 1f;	
	protected float abilityTimer = 0.0f;
	
	protected Camera myCamera;
	protected CharacterMovement movement;
	
	protected FillBarModel health, cooldown;
	protected ShadowScore score;
	
	public int pointsPerAttack = 1000;
	
	private bool invulnerable = false;
	private const float INVULNERABLE_SECONDS = 2.0f;
	
	public LightbearerScore lbScore;
	public int pointValue = 1000;
	
	// points feedback display
	public GUISkin scoreSkin;
	protected bool showPoints;
	protected Vector2 pointsScreenPos;
	private Color purple, gold;
	private float pointsDisplayTimeCounter;
	private const float POINTS_DISPLAY_DIRECTION_CHANGE_TIME = 0.25f;
	private const float POINTS_DISPLAY_END_TIME = 2f;
	private bool showPointsToLightbearer = false;
	
	public GameObject attackGlow, damageLeftGameObject, damageRightGameObject, lbDamageGlow;
	private ShadowDamage damageLeft, damageRight;
	private float damageFeedbackTimer = 0.0f;
	private const float DAMAGE_FEEDBACK_TIME = 0.1f;
	
	private GameObject lightbearer;
	
	public Animation modelAnimation;
	
	public bool startGame = false;
	
	public float runAnimationSpeed = 1.0f;

	void Start () {
		Init();
		myCamera = GetComponentInChildren<Camera>();
		myCamera.enabled = false;
		movement = GetComponent<CharacterMovement>();
		
		if (networkView.isMine){
			if (!Spawner.joined && !Network.isServer){
				movement.Freeze();
			}
		
			// spawn individual shadow UI
			GameObject shadowUIClone = (GameObject)Instantiate(Resources.Load("IndividualShadowGUI") as GameObject, new Vector3(0,0,0), Quaternion.identity);
			GameObject shadowScoreClone = (GameObject)Instantiate(Resources.Load("ShadowScore") as GameObject, new Vector3(0,0,0), Quaternion.identity);
			GameObject healthClone = (GameObject)Instantiate(Resources.Load("Health") as GameObject, new Vector3(0,0,0), Quaternion.identity);
			GameObject cooldownClone = (GameObject)Instantiate(Resources.Load("Cooldown") as GameObject, new Vector3(0,0,0), Quaternion.identity);
			shadowUIClone.GetComponent<Menu>().menuItems.Add(shadowScoreClone.GetComponent<MenuItem>());
			//shadowUIClone.GetComponent<Menu>().menuItems.Add(healthClone.GetComponent<MenuItem>());
			//shadowUIClone.GetComponent<Menu>().menuItems[0] = healthClone.GetComponent<MenuItem>();
			shadowUIClone.GetComponent<Menu>().menuItems[0] = shadowScoreClone.GetComponent<MenuItem>();
			shadowUIClone.GetComponent<Menu>().Restart();
			
			// set up individual shadow UI health
			//health = shadowUIClone.GetComponent<Menu>().menuItems[0].GetComponent<Health>();
			health = healthClone.GetComponent<FillBarModel>();
			//health.healthUIElement = shadowUIClone.GetComponent<Menu>().menuItems[0].GetComponent<MenuItem>();
			//health.RefreshOriginalUIText();
			health.SetMaxUnits(maxHealth);
			health.SetUnits(maxHealth);
			
			// set up individual shadow UI cooldown
			cooldown = cooldownClone.GetComponent<FillBarModel>();
			cooldown.SetMaxUnits(abilityCooldown);
			cooldown.SetUnits(abilityCooldown);
			
			// set up individual shadow UI score
			score = shadowUIClone.GetComponent<Menu>().menuItems[0].GetComponent<ShadowScore>();
			score.scoreUIElement = shadowUIClone.GetComponent<Menu>().menuItems[0].GetComponent<MenuItem>();
			score.RefreshOriginalUIText();
		}
		
		// set up for death giving points to Lightbearer
		GameObject lbScoreGameObject = GameObject.Find("LightbearerScore");
		if (lbScoreGameObject)
			lbScore = lbScoreGameObject.GetComponent<LightbearerScore>();

		// setup for points display when hitting Lightbearer 
		showPoints = false;
		gold = new Color(212.0f/255.0f, 192.0f/255.0f, 1.0f/255.0f, 0.5f);
		purple = new Color(110.0f/255.0f, 0, 218.0f/255.0f, 0.5f);
		pointsScreenPos = new Vector2(Screen.width/2-38, 125);
		if (Network.isServer){
			showPointsToLightbearer = true;
		}
		
		if (networkView.isMine && damageLeftGameObject && damageRightGameObject){
			damageLeftGameObject = (GameObject)Instantiate(damageLeftGameObject, damageLeftGameObject.transform.position, damageLeftGameObject.transform.rotation);
			damageRightGameObject = (GameObject)Instantiate(damageRightGameObject, damageRightGameObject.transform.position, damageRightGameObject.transform.rotation);
			damageLeft = damageLeftGameObject.GetComponent<ShadowDamage>();
			damageRight = damageRightGameObject.GetComponent<ShadowDamage>();
		}
		
		lightbearer = GameObject.FindWithTag("Player");
		if(networkView.isMine) {
			myCamera.enabled = true;
			myCamera.gameObject.AddComponent<AudioListener>();
		}
		
		if(Network.isServer) { //we are in solo mode
			ShadowEnable();
		}
		
	}
	
	protected abstract void Init(); //override this to add extra init stuff
	protected abstract void ShadowUpdate(); //override this to add extra update stuff	
	
	private float DetermineAnimationSpeed() {
		Vector3 velocity = transform.InverseTransformDirection(rigidbody.velocity);
		float result = (Mathf.Abs(velocity.z) / movement.topSpeed) * runAnimationSpeed;
		return result;
	}
	
	void Update () {
		if(abilityTimer > 0.0f) {
			abilityTimer -= Time.deltaTime;
			cooldown.SetUnits(abilityCooldown-abilityTimer);
		} else {
			abilityTimer = 0.0f;
		}
		
		modelAnimation["run_cycle"].speed = networkView.isMine ? DetermineAnimationSpeed() : 1.0f;
		
		ShadowUpdate();
		
		if (showPoints && (networkView.isMine || Network.isServer)){
			pointsDisplayTimeCounter += Time.deltaTime;
			if (pointsDisplayTimeCounter > POINTS_DISPLAY_END_TIME){
				showPoints = false;
				pointsScreenPos.x = Screen.width/2-38;
				pointsScreenPos.y = 125;
				gold.a = 0.5f;
				purple.a = 0.5f;
				pointsDisplayTimeCounter = 0;
			}
			else if (pointsDisplayTimeCounter < POINTS_DISPLAY_DIRECTION_CHANGE_TIME){
				pointsScreenPos.y -= (Time.deltaTime/pointsDisplayTimeCounter)*5;
				purple.a += Time.deltaTime;
				gold.a += Time.deltaTime;
			}
			else{
				pointsScreenPos.y += Time.deltaTime*pointsDisplayTimeCounter*250;
				purple.a -= Time.deltaTime;
				gold.a -= Time.deltaTime;
			}
		}
		
		if (damageFeedbackTimer > 0){
			damageFeedbackTimer -= Time.deltaTime;
			if (damageFeedbackTimer <= 0){
				if(networkView.isMine) {
					damageLeft.on = false;
					damageRight.on = false;
					networkView.RPC ("TurnOffDamageParticles", RPCMode.All);
				}
			}
		}
	}
	
	public bool UseAbility() {
		if((Input.GetButtonDown("Shadow Ability") || Input.touches.Length > 0) && startGame) {
			if(abilityTimer == 0.0f) {
				Instantiate(attackGlow, attackGlow.transform.position, attackGlow.transform.rotation); 
				abilityTimer = abilityCooldown;
				cooldown.SetUnits(0);
				return true;
			}
		}
		return false;
	}
	
	public void Damage() {
		networkView.RPC("TakeDamage", RPCMode.Others);
		networkView.RPC("DisplayDamage", RPCMode.All);
	}
	
	[RPC]
	public void TurnOffDamageParticles() {
		particleSystem.Stop ();
	}
	
	[RPC]
	public void TakeDamage() {
		if (networkView.isMine && !invulnerable){
			// take damage
			health.SubtractFromUnits(damagePerFrame);
			if(health.GetUnits() <= 0) {
				networkView.RPC("GivePoints", RPCMode.Server);
				invulnerable = true;
				networkView.RPC ("Die", RPCMode.All);
			}
		}
	}
	
	[RPC]
	public IEnumerator Die() {
		particleSystem.Stop ();
		
		if(networkView.isMine) {
			movement.Freeze ();
		}
		
		modelAnimation.CrossFade ("Die");
		yield return new WaitForSeconds(3.0f);
		modelAnimation.CrossFade ("run_cycle");
		
		if(networkView.isMine) {
			movement.Reset();
			movement.Unfreeze ();
			health.SetUnits(maxHealth);
			yield return new WaitForSeconds(INVULNERABLE_SECONDS);
			invulnerable = false;
		}
	}
	
	[RPC]
	public void DisplayDamage() {
		if (networkView.isMine && !invulnerable){
			// display damage feedback
			if (!lightbearer) // this doesn't seem to bet set properly from a Shadow's perspective when called in Start
				lightbearer = GameObject.FindWithTag("Player");
			if (lightbearer){
				Vector3 localPoint = gameObject.transform.InverseTransformPoint(lightbearer.transform.position);
				Vector2 lightbearerPoint = new Vector2(localPoint.x, localPoint.z);
				float angle = Vector2.Angle(-Vector2.right, lightbearerPoint);
				damageLeft.on = (angle <= 135);
				damageRight.on = (angle >= 45);
			}
		}
		damageFeedbackTimer = DAMAGE_FEEDBACK_TIME;
		particleSystem.Play ();
	}
	
	[RPC]
	public void ShadowEnable() {
		if(networkView.isMine) {
			movement.Enable();
			startGame = true;
		}
	}
	
	public ShadowScore GetShadowScore(){
		return score;
	}
	
	[RPC]
	void GivePoints() {
		if (lbScore){
			lbScore.AddFlatAmountToScore(pointValue);
			lbScore.AddToShadowsBanished(1);
			Vector3 newScreenPos = lightbearer.transform.Find("Camera").GetComponent<Camera>().WorldToScreenPoint(gameObject.transform.position);
			// MAGIC NUMBERS GALORE! - ask Lou if you really want to know what this does
			pointsScreenPos.x = newScreenPos.x-32;
			pointsScreenPos.y = Screen.height-newScreenPos.y-150;
			if (newScreenPos.z > 32){
				pointsScreenPos.y += (newScreenPos.z < 100) ? newScreenPos.z : Mathf.Log(newScreenPos.z)*20;
			}
			else if (newScreenPos.z < 29){
				pointsScreenPos.y = Screen.height/2-40;
			}
			showPoints = true;
		}
	}
	
	void OnGUI() {
		if (scoreSkin){
			GUI.skin = scoreSkin;
		}
		
		if (showPoints){
			GUI.depth = -100;
			int points = (showPointsToLightbearer) ? pointValue : pointsPerAttack;
			string pointsStr = "+"+points;

			Color outlineColor = purple;
			GUI.skin.label.normal.textColor = outlineColor;
			GUI.skin.label.hover.textColor = outlineColor;
			GUI.skin.label.active.textColor = outlineColor;
			
			GUI.Label(new Rect(pointsScreenPos.x+1, pointsScreenPos.y, 92, 30), pointsStr);
			GUI.Label(new Rect(pointsScreenPos.x+2, pointsScreenPos.y, 92, 30), pointsStr);
			GUI.Label(new Rect(pointsScreenPos.x-1, pointsScreenPos.y, 92, 30), pointsStr);
			GUI.Label(new Rect(pointsScreenPos.x, pointsScreenPos.y+1, 92, 30), pointsStr);
			GUI.Label(new Rect(pointsScreenPos.x, pointsScreenPos.y-1, 92, 30), pointsStr);
			
			GUI.skin.label.normal.textColor = gold;
			GUI.skin.label.hover.textColor = gold;
			GUI.skin.label.active.textColor = gold;
			GUI.Label(new Rect(pointsScreenPos.x, pointsScreenPos.y, 92, 30), pointsStr);
		}
	}
	
	public void SetShowPoints(bool show){
		showPoints = show;
	}
	
	public void ShowDamageToLightbearer(){
		networkView.RPC("ShowLightbearerDamage", RPCMode.Server);
	}
	
	[RPC]
	void ShowLightbearerDamage(){
		if (lbDamageGlow)
			Instantiate(lbDamageGlow, lbDamageGlow.transform.position, lbDamageGlow.transform.rotation);
		lightbearer.GetComponent<SimCharacter>().PlayDamageSound();
	}
}
