using UnityEngine;
using System.Collections;

[RequireComponent (typeof (Collider))]

public class Targeting : MonoBehaviour {

	public bool playerInRange = false;

	void OnTriggerEnter(Collider other) {
		if(other.tag == "Player") {
			playerInRange = true;
		}
	}
	
	void OnTriggerExit(Collider other) {
		if(other.tag == "Player") {
			playerInRange = false;
		}
	}
}
