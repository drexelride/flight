using UnityEngine;
using System.Collections;

public class StartGameAction : DefaultMenuAction {

	public bool forceStart = true;
	public Timer timer;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override void Action(){
		if (forceStart){
			networkView.RPC("EndTimer", RPCMode.All);
		}
		else{
			networkView.RPC("LoadGame", RPCMode.All);
		}
	}
	
	[RPC]
	public void LoadGame(){
		GameResults.shadowsInGame = ShadowSelect.shadowsSelected;
		Application.LoadLevel(ConnectionManager.GAME_SCENE);
	}
	
	[RPC]
	void EndTimer(){
		if (timer){
			timer.minutes = 0;
			timer.seconds = 0;
			if (timer.UIElement){
				timer.UIElement.text = "0";
			}
		}
	}
}
