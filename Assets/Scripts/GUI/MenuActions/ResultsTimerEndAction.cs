using UnityEngine;
using System.Collections;

public class ResultsTimerEndAction : DefaultMenuAction {

	public ResultsController resultsController;
	public const string TITLE_SCENE = "Title";
	public bool resultsTimer; // this timer is being used for the entire results screen - this boolean indicates whether it is counting down results or high scores

	private int shadowScoresWritten;
	private bool resultsEnd = false;

	// Use this for initialization
	void Start () {
		shadowScoresWritten = 0;
		resultsTimer = true;
	}
	
	// Update is called once per frame
	void Update () {
		// switch to high scores
		if (resultsController && Network.isServer && resultsController.numShadowHighScoresTotaled && (resultsController.numShadowHighScores == shadowScoresWritten) && resultsEnd){
			networkView.RPC("SetFadeResults", RPCMode.All, true);
			resultsController.endResults = true;
			resultsEnd = false;
		}
	}
	
	public override void Action(){
		if (resultsTimer){
			// write any high scores
			if (resultsController && resultsController.highScore){
				if (Network.isServer)
					resultsController.highScoreIO.AddScore(resultsController.initials1.Trim()+"/"+resultsController.initials2.Trim(), GameResults.lbFinalScore, "lightbearer", "",
						GameResults.lbBeaconsLit, GameResults.lbChain, GameResults.lbShadowsBanished);
				else{
					if (Spawner.character == "Roach"){
						networkView.RPC("WriteShadowScore", RPCMode.Server, resultsController.initials1, GameResults.roachScore, "Roach");
					}
					else if (Spawner.character == "Watcher"){
						networkView.RPC("WriteShadowScore", RPCMode.Server, resultsController.initials1, GameResults.watcherScore, "Watcher");
					}
					else if (Spawner.character == "Angler"){
						networkView.RPC("WriteShadowScore", RPCMode.Server, resultsController.initials1, GameResults.anglerScore, "Angler");
					}
					else if (Spawner.character == "Hammerhead"){
						networkView.RPC("WriteShadowScore", RPCMode.Server, resultsController.initials1, GameResults.hammerheadScore, "Hammerhead");
					}
					else if (Spawner.character == "Siren"){
						networkView.RPC("WriteShadowScore", RPCMode.Server, resultsController.initials1, GameResults.sirenScore, "Siren");
					}
				}
			}
			resultsEnd = true;
		}
		else{		
			// back to title for next game
			if (Network.isServer)
				networkView.RPC("ToTitle", RPCMode.All);
		}
	}
	
	[RPC]
	private void ToTitle() {
		Application.LoadLevel(TITLE_SCENE);
	}

	[RPC]
	private void WriteShadowScore(string initials, int score, string character){
		resultsController.highScoreIO.AddScore(initials.Trim(), score, "shadows", character);
		shadowScoresWritten++;
	}
	
	[RPC]
	private void SetFadeResults(bool setting){
		if (resultsController)
			resultsController.SetFadeResults(setting);
	}
}
