using UnityEngine;
using System.Collections;

public class JoinAction : DefaultMenuAction {

	public MenuItem join;
	public GUISkin joined;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override void Action(){
		if (join && joined){
			join.guiSkin = joined;
			join.text = "Waiting...";
			Spawner.joined = true;
		}
	}
}
