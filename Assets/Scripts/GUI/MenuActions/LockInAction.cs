using UnityEngine;
using System.Collections;

public class LockInAction : DefaultMenuAction {

	public static MenuItem lockInButtonMenuItem;
	public ShadowSelect shadowSelectAction;
	public static Color gold, purple;

	// Use this for initialization
	void Start () {
		lockInButtonMenuItem = GameObject.Find("LockIn").GetComponent<MenuItem>();
		gold = new Color(212.0f/255.0f, 192.0f/255.0f, 1.0f/255.0f);
		purple = new Color(110.0f/255.0f, 0, 218.0f/255.0f);
		if (!ShadowSelect.lockInButton){
			ShadowSelect.lockInButton = Resources.Load("LargeText") as GUISkin;
		}
		if (ShadowSelect.lockInButton){
			ShadowSelect.lockInButton.button.normal.textColor = purple;
			ShadowSelect.lockInButton.button.hover.textColor = purple;
			ShadowSelect.lockInButton.button.active.textColor = purple;
			ShadowSelect.lockInButton.button.active.textColor = purple;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override void Action(){
		audio.PlayOneShot (click);
		// update other clients over network
		if (ConnectionManager.soloMode){
			Spawner.character = ShadowSelect.selectedCharacter;
			Application.LoadLevel(ConnectionManager.GAME_SCENE);
		}
		else{
			if (!ShadowSelect.shadowsSelected.Contains(ShadowSelect.selectedCharacter)){			
				// update client Shadow select state
				ShadowSelect.lockedIn = true;
				if (lockInButtonMenuItem){
					if (ShadowSelect.lockInButton){
						ShadowSelect.lockInButton.button.normal.textColor = gold;
						ShadowSelect.lockInButton.button.hover.textColor = gold;
						ShadowSelect.lockInButton.button.active.textColor = gold;
						ShadowSelect.lockInButton.button.normal.background = Resources.Load("LockBGSelected") as Texture2D;
						ShadowSelect.lockInButton.button.hover.background = Resources.Load("LockBGSelected") as Texture2D;
						ShadowSelect.lockInButton.button.active.background = Resources.Load("LockBGSelected") as Texture2D;
					}
					lockInButtonMenuItem.text = "Locked In";
				}
				Spawner.character = ShadowSelect.selectedCharacter;
			
				// add Shadow and refresh UI of other clients
				networkView.RPC("AddShadow", RPCMode.All, Spawner.character, Network.player.guid, Network.player, Spawner.joined);
				networkView.RPC("RefreshSelectionButtons", RPCMode.Others, Spawner.character);
			}
			else{
				Debug.LogError("Character already chosen.");
			}
		}
	}
	
	[RPC]
	void AddShadow(string character, string guid, NetworkPlayer player, bool joined){
		// add Shadow to list of Shadows selected
		if (joined)
			ShadowSelect.shadowsSelected.Add(character);
		
		// create Shadow to NetworkPlayer association
		Spawner.shadowNetworkPlayers.Add(character, player);
		
		// remove client from undecided clients
		if (Network.isServer)
			ConnectionManager.undecidedClients.Remove(guid);
	}
	
	[RPC]
	void RefreshSelectionButtons(string lockedInCharacter){
		if (shadowSelectAction){
			shadowSelectAction.RefreshSelectionButtons(lockedInCharacter);
		}
	}
}
