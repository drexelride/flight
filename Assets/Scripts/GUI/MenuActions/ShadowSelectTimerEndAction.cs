using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShadowSelectTimerEndAction : DefaultMenuAction {

	public MenuItem anglerButtonMenuItem, roachButtonMenuItem, sirenButtonMenuItem, hammerheadButtonMenuItem, watcherButtonMenuItem, lockInButtonMenuItem;
	private List<string> autoSelectedShadows;
	private int confirmedLockIns = 0;
	public StartGameAction gameStarter;
	private int originalNumUndecidedClients = -1;
	private int shadowTimeouts = 0;
	public Instructions instructions;

	// Use this for initialization
	void Start () {
		autoSelectedShadows = new List<string>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// automatically assign any unassigned Shadow clients and move on
	public override void Action(){
		if (!Network.isServer){
			// tell server this Shadow's timer has reached 0
			networkView.RPC("IncrementShadowTimeouts", RPCMode.Server);
		}
	}
	
	// increment Shadow timeouts
	[RPC]
	void IncrementShadowTimeouts(){
		shadowTimeouts++;
		
		if (Network.isServer && (shadowTimeouts >= Network.connections.Length)){
			if (ConnectionManager.undecidedClients.Count > 0){
				originalNumUndecidedClients = ConnectionManager.undecidedClients.Count;
				foreach (string guid in ConnectionManager.undecidedClients){
					NetworkPlayer client = Network.player;
					foreach (NetworkPlayer n in Network.connections){
						if (n.guid == guid){
							client = n;
							break;
						}
					}
					if (client.guid != Network.player.guid){
						if (!ShadowSelect.shadowsSelected.Contains("Roach") && !autoSelectedShadows.Contains("Roach")){
							autoSelectedShadows.Add("Roach");
							networkView.RPC("AutoSelect", client, "Roach");
						}
						else if (!ShadowSelect.shadowsSelected.Contains("Watcher") && !autoSelectedShadows.Contains("Watcher")){
							autoSelectedShadows.Add("Watcher");
							networkView.RPC("AutoSelect", client, "Watcher");
						}
						else if (!ShadowSelect.shadowsSelected.Contains("Angler") && !autoSelectedShadows.Contains("Angler")){
							autoSelectedShadows.Add("Angler");
							networkView.RPC("AutoSelect", client, "Angler");
						}
						else if (!ShadowSelect.shadowsSelected.Contains("Hammerhead") && !autoSelectedShadows.Contains("Hammerhead")){
							autoSelectedShadows.Add("Hammerhead");
							networkView.RPC("AutoSelect", client, "Hammerhead");
						}
						else{
							networkView.RPC("AutoSelect", client, "Siren");
						}
					}
				}
			}
			else{
				StartCoroutine(MoveOn());
			}
		}
	}
	
	// automatically select Shadow on client
	[RPC]
	void AutoSelect(string shadow){
		if (shadow == "Roach"){
			roachButtonMenuItem.action.Action();
		}
		else if (shadow == "Watcher"){
			watcherButtonMenuItem.action.Action();
		}
		else if (shadow == "Angler"){
			anglerButtonMenuItem.action.Action();
		}
		else if (shadow == "Hammerhead"){
			hammerheadButtonMenuItem.action.Action();
		}
		else{
			sirenButtonMenuItem.action.Action();
		}
		
		lockInButtonMenuItem.action.Action();
		
		// let the server know an undecided client is now locked in
		networkView.RPC("ConfirmLockIn", RPCMode.Server);
	}
	
	[RPC]
	void ConfirmLockIn(){
		confirmedLockIns++;
		if (originalNumUndecidedClients == -1){
			Debug.Log("Something went wrong: originalNumUndecidedClients was never initialized");
		}
		else if (confirmedLockIns >= originalNumUndecidedClients){
			//Debug.Log("Confirmed lockins: " + confirmedLockIns);
			
			// all clients are locked in - move to instructions
			StartCoroutine(MoveOn());
		}
	}
	
	private IEnumerator MoveOn(){
		yield return new WaitForSeconds(1.0f);
		if (instructions){
			networkView.RPC("StartInstructions", RPCMode.Others);
		}
		else{
			if (gameStarter){
				gameStarter.forceStart = false;
				gameStarter.Action();
			}
		}
	}
	
	[RPC]
	void StartInstructions(){
		instructions.state = Instructions.InstructionsState.Shadow1;
		instructions.started = true;
	}
}
