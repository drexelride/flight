using UnityEngine;
using System.Collections;

public class GameTimerEndAction : DefaultMenuAction {

	// for killing data
	public GameObject wrapperObject;
	
	// for ending gameplay
	private GameObject[] lightbearer, shadows;
	public MenuItem timeUp;
	public float waitSeconds = 3.0f;
	private float waitTimer = 0.0f;
	private bool startWait = false;
	public const string RESULTS_SCENE = "Results";
	
	// for setting results
	public Chain chain;
	public LightbearerBeaconCount beaconCount;
	public LightbearerScore lbScore;
	
	private int shadowScoresRegistered;

	// Use this for initialization
	void Start () {
		shadowScoresRegistered = 0;
	}
	
	// Update is called once per frame
	void Update () {
		// wait and continue to results screen
		if (startWait){
			waitTimer += Time.deltaTime;
			if (waitTimer >= waitSeconds && Network.isServer && (Network.connections.Length == shadowScoresRegistered)){
				networkView.RPC("LoadScene", RPCMode.All, RESULTS_SCENE);
			}
		}
	}
	
	public override void Action(){
		wrapperObject.GetComponent<RigidbodyWrapper>().kill = true; // kill data to simulator
		
		// stop movement
		lightbearer = GameObject.FindGameObjectsWithTag("Player");
		shadows = GameObject.FindGameObjectsWithTag("Shadow");
		if (lightbearer.Length > 0){
			foreach (GameObject g in lightbearer){
				g.GetComponent<CharacterMovement>().Freeze();
				g.GetComponent<LagInterpolation>().enabled = false;
				g.GetComponent<SimCharacter>().GetMyLight().SetEndGame(true);
			}
		}
		if (shadows.Length > 0){
			foreach (GameObject g in shadows){
				g.GetComponent<CharacterMovement>().Freeze();
				g.GetComponent<LagInterpolation>().enabled = false;
			}
		}
		
		// display time up message
		if (timeUp)
			timeUp.visible = true;
			
		// set results
		if (Spawner.character == "Lightbearer"){
			int beaconBonusMult = (beaconCount.GetCount()/10 > 0) ? (beaconCount.GetCount()/10) : 1;
			if (chain && beaconCount && lbScore){
				networkView.RPC("SetLightbearerResults", RPCMode.All, beaconCount.GetCount(), chain.GetLongest(), lbScore.GetShadowsBanished(), lbScore.score, beaconBonusMult, lbScore.score*beaconBonusMult);
			}
		}
		else{
			foreach (GameObject g in shadows){
				if (g.networkView.isMine){
					networkView.RPC("SetShadowResult", RPCMode.All, Spawner.character, g.GetComponent<Shadow>().GetShadowScore().score);
				}
			}
		}
		
		// continue to results scene
		startWait = true;
	}
	
	[RPC]
	private void LoadScene(string sceneName) {
		Application.LoadLevel(sceneName);
	}

	[RPC]
	private void SetLightbearerResults(int beaconsLit, int longestChain, int shadowsBanished, int rawScore, int beaconBonus, int finalScore){
		GameResults.lbBeaconsLit = beaconsLit;
		GameResults.lbChain = longestChain;
		GameResults.lbShadowsBanished = shadowsBanished;
		GameResults.lbRawScore = rawScore;
		GameResults.lbBeaconBonus = beaconBonus;
		GameResults.lbFinalScore = finalScore;
	}
	
	[RPC]
	private void SetShadowResult(string shadow, int score){
		if (shadow == "Roach"){
			GameResults.roachScore = score;
		}
		else if (shadow == "Watcher"){
			GameResults.watcherScore = score;
		}
		else if (shadow == "Angler"){
			GameResults.anglerScore = score;
		}
		else if (shadow == "Hammerhead"){
			GameResults.hammerheadScore = score;
		}
		else if (shadow == "Siren"){
			GameResults.sirenScore = score;
		}
		shadowScoresRegistered++;
	}
}
