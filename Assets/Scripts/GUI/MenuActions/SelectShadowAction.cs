using UnityEngine;
using System.Collections;

public class SelectShadowAction : DefaultMenuAction {

	// references to menu items whose values need to change
	public MenuItem charName, charDesc, healthGreen, healthYellow, healthRed, speedGreen, speedYellow, speedRed;
	
	// stat values
	public int health, speed = 0;
	
	// values with which to change screen
	public string charNameText, charDescText;
	
	// models to display and toggle
	public GameObject roachObject, watcherObject, anglerObject, hammerheadObject, sirenObject;
	
	// names for menu item game objects in case you don't feel like filling them in in the inspector
	public string charNameGameObject = "CharacterName",
				charDescGameObject = "CharacterDescription",
				charStatsGameObject = "CharacterStats",
				healthGreenGameObject = "HealthGreen",
				healthYellowGameObject = "HealthYellow",
				healthRedGameObject = "HealthRed",
				speedGreenGameObject = "SpeedGreen",
				speedYellowGameObject = "SpeedYellow",
				speedRedGameObject = "SpeedRed";

	// Use this for initialization
	void Start () {	
		// change name/description/stats details
		if (!charName)
			charName = GameObject.Find(charNameGameObject).GetComponent<MenuItem>();
		if (!charDesc)
			charDesc = GameObject.Find(charDescGameObject).GetComponent<MenuItem>();
		if (!healthGreen)
			healthGreen = GameObject.Find(healthGreenGameObject).GetComponent<MenuItem>();
		if (!healthYellow)
			healthYellow = GameObject.Find(healthYellowGameObject).GetComponent<MenuItem>();
		if (!healthRed)
			healthRed = GameObject.Find(healthRedGameObject).GetComponent<MenuItem>();
		if (!speedGreen)
			speedGreen = GameObject.Find(speedGreenGameObject).GetComponent<MenuItem>();
		if (!speedYellow)
			speedYellow = GameObject.Find(speedYellowGameObject).GetComponent<MenuItem>();
		if (!speedRed)
			speedRed = GameObject.Find(speedRedGameObject).GetComponent<MenuItem>();
		
		// initialize selection buttons
		SetSelectionButtons("Roach");
		
		// initialize display model objects
		if (watcherObject)
			watcherObject.SetActive(false);
		if (anglerObject)
			anglerObject.SetActive(false);
		if (hammerheadObject)
			hammerheadObject.SetActive(false);
		if (sirenObject)
			sirenObject.SetActive(false);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public override void Action(){
		if (!ShadowSelect.lockedIn){
			audio.PlayOneShot (click);
			// change name and description text
			if (charName)
				charName.text = charNameText;
			if (charDesc)
				charDesc.text = charDescText;
				
			// change stat values
			healthRed.visible = (health > 0) ? true : false;
			healthYellow.visible = (health > 1) ? true : false;
			healthGreen.visible = (health > 2) ? true : false;
			speedRed.visible = (speed > 0) ? true : false;
			speedYellow.visible = (speed > 1) ? true : false;
			speedGreen.visible = (speed > 2) ? true : false;
			
			// change spawner character
			ShadowSelect.selectedCharacter = charNameText;
			
			// change selection buttons
			SetSelectionButtons(charNameText);
			
			// change displayed model
			if (roachObject)
				roachObject.SetActive(charNameText == "Roach");
			if (watcherObject)
				watcherObject.SetActive(charNameText == "Watcher");
			if (anglerObject)
				anglerObject.SetActive(charNameText == "Angler");
			if (hammerheadObject)
				hammerheadObject.SetActive(charNameText == "Hammerhead");
			if (sirenObject)
				sirenObject.SetActive(charNameText == "Siren");
		}
	}
	
	// set selection button colors on client
	private void SetSelectionButtons(string selected){
		// set side buttons
		if (ShadowSelect.anglerButton){
			if (!ShadowSelect.shadowsSelected.Contains("Angler")){
				ShadowSelect.anglerButton.button.normal.background = (selected == "Angler") ? Resources.Load("AnglerIconActive") as Texture2D : Resources.Load("AnglerIconIdle") as Texture2D;
				ShadowSelect.anglerButton.button.hover.background = (selected == "Angler") ? Resources.Load("AnglerIconActive") as Texture2D : Resources.Load("AnglerIconIdle") as Texture2D;
				ShadowSelect.anglerButton.button.active.background = (selected == "Angler") ? Resources.Load("AnglerIconActive") as Texture2D : Resources.Load("AnglerIconIdle") as Texture2D;
			}
		}
		if (ShadowSelect.roachButton){
			if (!ShadowSelect.shadowsSelected.Contains("Roach")){
				ShadowSelect.roachButton.button.normal.background = (selected == "Roach") ? Resources.Load("RoachIconActive") as Texture2D : Resources.Load("RoachIconIdle") as Texture2D;
				ShadowSelect.roachButton.button.hover.background = (selected == "Roach") ? Resources.Load("RoachIconActive") as Texture2D : Resources.Load("RoachIconIdle") as Texture2D;
				ShadowSelect.roachButton.button.active.background = (selected == "Roach") ? Resources.Load("RoachIconActive") as Texture2D : Resources.Load("RoachIconIdle") as Texture2D;
			}
		}
		if (ShadowSelect.sirenButton){
			if (!ShadowSelect.shadowsSelected.Contains("Siren")){
				ShadowSelect.sirenButton.button.normal.background = (selected == "Siren") ? Resources.Load("SirenIconActive") as Texture2D : Resources.Load("SirenIconIdle") as Texture2D;
				ShadowSelect.sirenButton.button.hover.background = (selected == "Siren") ? Resources.Load("SirenIconActive") as Texture2D : Resources.Load("SirenIconIdle") as Texture2D;
				ShadowSelect.sirenButton.button.active.background = (selected == "Siren") ? Resources.Load("SirenIconActive") as Texture2D : Resources.Load("SirenIconIdle") as Texture2D;
			}
		}
		if (ShadowSelect.hammerheadButton){
			if (!ShadowSelect.shadowsSelected.Contains("Hammerhead")){
				ShadowSelect.hammerheadButton.button.normal.background = (selected == "Hammerhead") ? Resources.Load("HammerheadIconActive") as Texture2D : Resources.Load("HammerheadIconIdle") as Texture2D;
				ShadowSelect.hammerheadButton.button.hover.background = (selected == "Hammerhead") ? Resources.Load("HammerheadIconActive") as Texture2D : Resources.Load("HammerheadIconIdle") as Texture2D;
				ShadowSelect.hammerheadButton.button.active.background = (selected == "Hammerhead") ? Resources.Load("HammerheadIconActive") as Texture2D : Resources.Load("HammerheadIconIdle") as Texture2D;
			}
		}
		if (ShadowSelect.watcherButton){
			if (!ShadowSelect.shadowsSelected.Contains("Watcher")){
				ShadowSelect.watcherButton.button.normal.background = (selected == "Watcher") ? Resources.Load("WatcherIconActive") as Texture2D : Resources.Load("WatcherIconIdle") as Texture2D;
				ShadowSelect.watcherButton.button.hover.background = (selected == "Watcher") ? Resources.Load("WatcherIconActive") as Texture2D : Resources.Load("WatcherIconIdle") as Texture2D;
				ShadowSelect.watcherButton.button.active.background = (selected == "Watcher") ? Resources.Load("WatcherIconActive") as Texture2D : Resources.Load("WatcherIconIdle") as Texture2D;
			}
		}
		
		// set Lock In button
		if (ShadowSelect.shadowsSelected != null){
			if (ShadowSelect.shadowsSelected.Contains(selected)){
				ShadowSelect.lockInButton.button.normal.background = Resources.Load("LockBGGray") as Texture2D;
				ShadowSelect.lockInButton.button.hover.background = Resources.Load("LockBGGray") as Texture2D;
				ShadowSelect.lockInButton.button.active.background = Resources.Load("LockBGGray") as Texture2D;
				ShadowSelect.lockInButton.button.normal.textColor = ShadowSelect.gray;
				ShadowSelect.lockInButton.button.hover.textColor = ShadowSelect.gray;
				ShadowSelect.lockInButton.button.active.textColor = ShadowSelect.gray;
				if (LockInAction.lockInButtonMenuItem){
					LockInAction.lockInButtonMenuItem.text = "Taken";
				}
			}
			else{
				ShadowSelect.lockInButton.button.normal.background = Resources.Load("LockBG") as Texture2D;
				ShadowSelect.lockInButton.button.hover.background = Resources.Load("LockBG") as Texture2D;
				ShadowSelect.lockInButton.button.active.background = Resources.Load("LockBG") as Texture2D;
				ShadowSelect.lockInButton.button.normal.textColor = LockInAction.purple;
				ShadowSelect.lockInButton.button.hover.textColor = LockInAction.purple;
				ShadowSelect.lockInButton.button.active.textColor = LockInAction.purple;
				if (LockInAction.lockInButtonMenuItem){
					LockInAction.lockInButtonMenuItem.text = "Lock In";
				}
			}
		}
	}
}
