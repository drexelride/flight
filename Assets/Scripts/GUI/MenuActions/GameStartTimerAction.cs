using UnityEngine;
using System.Collections;

public class GameStartTimerAction : DefaultMenuAction {

	public Timer gameTimer;
	
	void Start() {
		if(Network.isClient) {
			GetComponent<Timer>().hideTime = true;
			GetComponent<Timer>().timerMask = "Get Ready";
			GetComponent<Timer>().UIElement.text = "Get Ready";
		}	
	}

	public override void Action() {
		if (Network.isServer) {
			if (Spawner.character == "Lightbearer"){
				SimCharacter lightbearer = GameObject.FindGameObjectsWithTag("Player")[0].GetComponent<SimCharacter>();
				lightbearer.Enable();
				networkView.RPC("StartTimer", RPCMode.All);
			}
			else{ // solo shadow mode
				Shadow shadow = GameObject.FindGameObjectsWithTag("Shadow")[0].GetComponent<Shadow>();
				shadow.ShadowEnable();
			}
		}
		GetComponent<MenuItem>().visible = false;
	}
	
	[RPC]
	private void StartTimer() {
		gameTimer.on = true;
		GetComponent<MenuItem>().visible = false;
	}
	
}
