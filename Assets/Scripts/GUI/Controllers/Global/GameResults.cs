using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameResults : MonoBehaviour {

	public static int lbBeaconsLit, lbChain, lbShadowsBanished, 
		lbRawScore, lbBeaconBonus, lbFinalScore, roachScore,
		watcherScore, anglerScore, hammerheadScore, sirenScore;
	public static List<string> shadowsInGame = new List<string>();

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad(gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
