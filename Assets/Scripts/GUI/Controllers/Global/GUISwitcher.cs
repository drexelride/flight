using UnityEngine;
using System.Collections;

public class GUISwitcher : MonoBehaviour {

	public Menu lightbearerGUI, shadowGUI;
	public FillBar lbBattery, lbChain;
	public Camera guiCam;

	// Use this for initialization
	void Start () {
		lightbearerGUI.on = Spawner.character == "Lightbearer";
		lbBattery.on = Spawner.character == "Lightbearer";
		lbChain.on = Spawner.character == "Lightbearer";
		shadowGUI.on = Spawner.character != "Lightbearer";
		guiCam.enabled = Spawner.character != "Lightbearer";
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
