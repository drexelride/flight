using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PauseMenu : MonoBehaviour {

	/*
	private bool paused = false;
	public List<MenuItem> sliders = new List<MenuItem>();
	public List<MenuItem> sliderArrows = new List<MenuItem>();
	private int currentSlider = 0;
	private bool scrolling = false;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {	
		if (Input.GetButtonDown("Pause Button")){
			gameObject.GetComponent<Menu>().on = gameObject.GetComponent<Menu>().on ? false : true;
			//Time.timeScale = gameObject.GetComponent<Menu>().on ? 0.0f : 1.0f; changing time scale interrupts the data stream
			paused = gameObject.GetComponent<Menu>().on ? true : false;
		}
		
		if (paused){
			if (Input.GetAxis("Directional Vertical") > 0) {
				if (!scrolling){
					sliderArrows[currentSlider].visible = false;
					currentSlider = (currentSlider == 0) ? (sliderArrows.Count-1) : currentSlider-1;
					sliderArrows[currentSlider].visible = true;
					scrolling = true;
				}
			}
			else if (Input.GetAxis("Directional Vertical") < 0) {
				if (!scrolling){
					sliderArrows[currentSlider].visible = false;
					currentSlider = (currentSlider == sliderArrows.Count-1) ? 0 : currentSlider+1;
					sliderArrows[currentSlider].visible = true;
					scrolling = true;
				}
			}
			else{
				scrolling = false;
				float sliderMax = sliders[currentSlider].sliderMax;
				float sliderMin = sliders[currentSlider].sliderMin;
				float delta = 0.01f*(sliderMax - sliderMin);
				if (Input.GetAxis("Directional Horizontal") > 0){
					GameValues.floatValues[sliders[currentSlider].sliderGameValue] += delta;
					//SimCharacter.
				}
				
				if (Input.GetAxis("Directional Horizontal") < 0){
					GameValues.floatValues[sliders[currentSlider].sliderGameValue] -= delta;
				}
				GameValues.floatValues[sliders[currentSlider].sliderGameValue] = Mathf.Clamp(GameValues.floatValues[sliders[currentSlider].sliderGameValue], sliderMin, sliderMax);
			}
		}
	}
	*/
}
