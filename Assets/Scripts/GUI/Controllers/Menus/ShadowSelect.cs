using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ShadowSelect : MonoBehaviour {

	public bool lightbearerStartButton = false;
	public Menu shadowSelectMenu, lightbearerStarter;
	public static List<string> shadowsSelected;
	public static bool lockedIn;
	public static Color gray;
	public MenuItem lockInButtonMenuItem;
	
	public static string selectedCharacter = "Roach";
	
	public Texture2D notJoinedScreen;
	
	// selection button skins
	public static GUISkin anglerButton, roachButton, sirenButton, hammerheadButton, watcherButton, lockInButton;

	// Use this for initialization
	void Start () {
		if (Network.isServer)
			MusicController.GetInstance().PlayBGM2();
	
		lockInButtonMenuItem.visible = (Spawner.joined || Network.isServer);
		
		shadowSelectMenu.on = (Spawner.character != "Lightbearer");
		lightbearerStarter.on = ((lightbearerStartButton) && (Spawner.character == "Lightbearer"));
		shadowsSelected = new List<string>();
		lockedIn = false;
		gray = new Color(77.0f/255.0f, 77.0f/255.0f, 77.0f/255.0f);
		
		// load selection buttons
		anglerButton = Resources.Load("AnglerButton") as GUISkin;
		roachButton = Resources.Load("RoachButton") as GUISkin; 
		sirenButton = Resources.Load("SirenButton") as GUISkin;
		hammerheadButton = Resources.Load("HammerheadButton") as GUISkin;
		watcherButton = Resources.Load("WatcherButton") as GUISkin;
		lockInButton = Resources.Load("LargeText") as GUISkin;
		
		Spawner.shadowStaticFieldsInitialized = true;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// refresh selection buttons (and Lock In button) to gray out buttons appropriately (called from RPC)
	public void RefreshSelectionButtons(string lockedInCharacter){
		// refresh side buttons
		if (anglerButton){
			if (shadowsSelected.Contains("Angler") && !(Spawner.character == "Angler")){
				anglerButton.button.normal.background = Resources.Load("AnglerIcon") as Texture2D;
				anglerButton.button.hover.background = Resources.Load("AnglerIcon") as Texture2D;
				anglerButton.button.active.background = Resources.Load("AnglerIcon") as Texture2D;
			}
		}
		if (roachButton){
			if (shadowsSelected.Contains("Roach") && !(Spawner.character == "Roach")){
				roachButton.button.normal.background = Resources.Load("RoachIcon") as Texture2D;
				roachButton.button.hover.background = Resources.Load("RoachIcon") as Texture2D;
				roachButton.button.active.background = Resources.Load("RoachIcon") as Texture2D;
			}
		}
		if (sirenButton){
			if (shadowsSelected.Contains("Siren") && !(Spawner.character == "Siren")){
				sirenButton.button.normal.background = Resources.Load("SirenIcon") as Texture2D;
				sirenButton.button.hover.background = Resources.Load("SirenIcon") as Texture2D;
				sirenButton.button.active.background = Resources.Load("SirenIcon") as Texture2D;
			}
		}
		if (hammerheadButton){
			if (shadowsSelected.Contains("Hammerhead") && !(Spawner.character == "Hammerhead")){
				hammerheadButton.button.normal.background = Resources.Load("HammerheadIcon") as Texture2D;
				hammerheadButton.button.hover.background = Resources.Load("HammerheadIcon") as Texture2D;
				hammerheadButton.button.active.background = Resources.Load("HammerheadIcon") as Texture2D;
			}
		}
		if (watcherButton){
			if (shadowsSelected.Contains("Watcher") && !(Spawner.character == "Watcher")){
				watcherButton.button.normal.background = Resources.Load("WatcherIcon") as Texture2D;
				watcherButton.button.hover.background = Resources.Load("WatcherIcon") as Texture2D;
				watcherButton.button.active.background = Resources.Load("WatcherIcon") as Texture2D;
			}
		}
		
		// refresh Lock In button
		if (selectedCharacter == lockedInCharacter){
			lockInButton.button.normal.background = Resources.Load("LockBGGray") as Texture2D;
			lockInButton.button.hover.background = Resources.Load("LockBGGray") as Texture2D;
			lockInButton.button.active.background = Resources.Load("LockBGGray") as Texture2D;
			lockInButton.button.normal.textColor = gray;
			lockInButton.button.hover.textColor = gray;
			lockInButton.button.active.textColor = gray;
			if (LockInAction.lockInButtonMenuItem){
				LockInAction.lockInButtonMenuItem.text = "Taken";
			}
		}
	}
	
	void OnGUI(){
		GUI.depth = -5000;
		if (!Spawner.joined){
			if (notJoinedScreen)
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), notJoinedScreen);
		}
	}
}
