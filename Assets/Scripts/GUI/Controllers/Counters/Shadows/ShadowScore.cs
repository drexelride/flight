using UnityEngine;
using System.Collections;

public class ShadowScore : MonoBehaviour {

	public MenuItem scoreUIElement;
	public int score = 0;
	private string originalUIText = "";

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// add to score value
	public void AddToScore(int points){
		score = score + points;
		if (scoreUIElement){
			scoreUIElement.text = originalUIText+score;
		}
	}
	
	public void RefreshOriginalUIText(){
		if (scoreUIElement){
			originalUIText = scoreUIElement.text;
			scoreUIElement.text = originalUIText+score;
		}
	}
}
