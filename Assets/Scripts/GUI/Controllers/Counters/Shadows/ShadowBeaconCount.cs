using UnityEngine;
using System.Collections;

public class ShadowBeaconCount : MonoBehaviour {

	public MenuItem beaconCountUIElement;
	public int count = 0; // number of beacons retained
	private string originalUIText = "";

	// Use this for initialization
	void Start () {
		if (beaconCountUIElement){
			originalUIText = beaconCountUIElement.text;
			beaconCountUIElement.text = originalUIText+count;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// subtract from to beacon count
	public void SubtractFromBeaconCount(int num){
		count = (count - num >= 0) ? (count - num) : count;
		if (beaconCountUIElement){
			beaconCountUIElement.text = originalUIText+count;
		}
	}
}
