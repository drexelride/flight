using UnityEngine;
using System.Collections;

public class Timer : MonoBehaviour {

	public MenuItem UIElement;
	public DefaultMenuAction timeoutAction; // what to do when the timer times out
	public int minutes = 5;
	public int seconds = 0;
	private string originalText = "";
	public bool on = true;
	public bool showInSeconds = false;
	private int initMinutes, initSeconds;
	
	public bool useWarnings = false;
	public GUISkin warningSkin1, warningSkin2;
	public int warningTimeSeconds1, warningTimeSeconds2 = 0;
	public bool hideTime = false; //set this to true to mask the timer text
	public string timerMask;
	
	// Use this for initialization
	void Start () {
		initMinutes = minutes;
		initSeconds = seconds;
	
		if (UIElement){
			if (seconds > 59){
				seconds = 59;
			}
			originalText = UIElement.text;
			if (showInSeconds){
				string secondsText = (minutes * 60) + seconds + "";
				UIElement.text = originalText+secondsText;
			}
			else{
				UIElement.text = (seconds < 10) ? originalText+minutes+":0"+seconds : originalText+minutes+":"+seconds;
			}
			StartCoroutine(CountDown());
		}
		else{
			Debug.Log("Could not start timer! No associated UI element (menu item)!");
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
	
	public IEnumerator CountDown(){
		while (true){
			yield return new WaitForSeconds(1.0f);
			if (on){
				if(!hideTime) {
					seconds--;
					if (seconds < 0 && minutes > 0){
						seconds = 59;
						minutes--;
					}
					if (useWarnings){
						if (((minutes*60 + seconds) <= warningTimeSeconds2) && warningSkin2){
							UIElement.guiSkin = warningSkin2;
							UIElement.overrideOutlineColor = true;
							UIElement.outlineROverride = 0;
							UIElement.outlineGOverride = 0;
							UIElement.outlineBOverride = 0;
						}
						else if (((minutes*60 + seconds) <= warningTimeSeconds1) && warningSkin1){
							UIElement.guiSkin = warningSkin1;
							UIElement.overrideOutlineColor = true;
							UIElement.outlineROverride = 0;
							UIElement.outlineGOverride = 0;
							UIElement.outlineBOverride = 0;
						}
					}
					if (seconds >= 0){
						if (showInSeconds){
							string secondsText = (minutes * 60) + seconds + "";
							UIElement.text = originalText+secondsText;
						}
						else{
							UIElement.text = (seconds < 10) ? originalText+minutes+":0"+seconds : originalText+minutes+":"+seconds;
						}
					}
					if (seconds <= 0 && minutes <= 0)
						break;
				}
			}
		}
		if (timeoutAction)
			timeoutAction.Action();
	}
	
	// for timer ring
	public float timeToAlphaCutoff(){
		float time = (minutes * 60 + seconds);
		float duration = (initMinutes * 60 + initSeconds);
		return time/duration;
	}
	
	public void ForceRefresh(){
		if (UIElement){
			if (seconds > 59){
				seconds = 59;
			}
			if (showInSeconds){
				string secondsText = (minutes * 60) + seconds + "";
				UIElement.text = originalText+secondsText;
			}
			else{
				UIElement.text = (seconds < 10) ? originalText+minutes+":0"+seconds : originalText+minutes+":"+seconds;
			}
		}
		else{
			Debug.Log("Could not start timer! No associated UI element (menu item)!");
		}
	}
}
