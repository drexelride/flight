using UnityEngine;
using System.Collections;

public class LightbearerBeaconCount : MonoBehaviour {

	public MenuItem beaconCountUIElement;
	private int count = 0; // total beacons captured
	//private int numBeacons = 0; // total number of beacons
	private string originalUIText = "";

	// Use this for initialization
	void Start () {
		//numBeacons = GameObject.FindGameObjectsWithTag(BeaconManager.BEACON_TAG).Length;
		if (beaconCountUIElement){
			originalUIText = beaconCountUIElement.text;
			//Debug.Log(count);
			//beaconCountUIElement.text = originalUIText+count+"/"+numBeacons;
			beaconCountUIElement.text = originalUIText+count;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// add to beacon count
	public void AddToBeaconCount(int num){
		count = count + num;
		if (beaconCountUIElement){
			//beaconCountUIElement.text = originalUIText+count+"/"+numBeacons;
			beaconCountUIElement.text = originalUIText+count;
		}
	}
	
	public int GetCount(){
		return count;
	}
}
