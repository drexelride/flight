using UnityEngine;
using System.Collections;

public class Chain : MonoBehaviour {

	public MenuItem chainUIElement;
	private SimCharacter simCharacter;
	public int chainBonus = 200; // extra points for each chain level
	private int chain = 0; // current chain level
	private float time = 0.0f; // current chain gauge time
	public const float MAX_TIME = 5.0f; // time (in seconds) for how long chain gauge lasts
	private string originalUIText = "";
	public FillBarModel chainGauge;
	private int longest = 0;

	// Use this for initialization
	void Start () {
		if (chainUIElement){
			originalUIText = chainUIElement.text;
			//Debug.Log(count);
			chainUIElement.text = originalUIText+chain;
			if (chainGauge){
				chainGauge.SetMaxUnits(MAX_TIME);
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (simCharacter){
			if ((time > 0) && !(simCharacter.GetMyLight().isShiningOnBeacon)){
				time -= Time.deltaTime; // subtract 1 from chain gauge time per second
				if (chainGauge){
					chainGauge.SetUnits(time);
				}
				if (time <= 0){
					chain = 0;
					chainUIElement.visible = false;
					audio.Play ();
				}
			}
		}
	}
	
	// add to chain level
	public void AddToChain(int num){
		chain = chain + num;
		if (chain > longest){
			longest = chain;
		}
		time = MAX_TIME;
		if (chainGauge){
			chainGauge.SetUnits(time);
		}
		if (chainUIElement){
			chainUIElement.visible = true;
			chainUIElement.text = originalUIText+chain;
		}
	}
	
	public int GetChain(){
		return chain;
	}
	
	public int GetLongest(){
		return longest;
	}
	
	public void SetSimCharacter(SimCharacter simCharacter){
		this.simCharacter = simCharacter;
	}
}
