using UnityEngine;
using System.Collections;

public class LightbearerScore : MonoBehaviour {

	public MenuItem scoreUIElement;
	public int score = 0;
	private string originalUIText = "";
	private Chain chain; // chain controller
	private int shadowsBanished = 0;

	// Use this for initialization
	void Start () {
		chain = GameObject.Find("Chain").GetComponent<Chain>();
		if (scoreUIElement){
			originalUIText = scoreUIElement.text;
			//Debug.Log(score);
			scoreUIElement.text = originalUIText+score;
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// add to score value (for lighting beacons)
	public void AddToScore(int points){
		score = (chain) ? (score + points + (chain.GetChain()*chain.chainBonus)) : (score + points);
		if (scoreUIElement){
			scoreUIElement.text = originalUIText+score;
		}
	}
	
	// add flat amount to score value (for defeating Shadows)
	public void AddFlatAmountToScore(int points){
		score += points;
		if (scoreUIElement){
			scoreUIElement.text = originalUIText+score;
		}
	}
	
	// add to shadows banished
	public void AddToShadowsBanished(int num){
		shadowsBanished += num;
	}
	
	public int GetShadowsBanished(){
		return shadowsBanished;
	}
}
