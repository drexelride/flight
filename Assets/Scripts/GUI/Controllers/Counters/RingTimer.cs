using UnityEngine;
using System.Collections;

public class RingTimer : MonoBehaviour {

	public Timer timer;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		if (timer){
			float cutoff = timer.timeToAlphaCutoff();
			if (cutoff < 0)
				cutoff = 0;
			renderer.material.SetFloat("_Cutoff", 1.0f - cutoff);
		}
	}
}
