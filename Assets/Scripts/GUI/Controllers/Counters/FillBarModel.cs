using UnityEngine;
using System.Collections;

public class FillBarModel : MonoBehaviour {

	public FillBar fillbarUIElement;
	private float units, maxUnits;

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	// subtract from unit value
	public void SubtractFromUnits(float amount){
		units = units - amount;
		RefreshUIElement();
	}
	
	public void SetUnits(float amount){
		units = amount;
		RefreshUIElement();
	}
	
	public void SetMaxUnits(float amount){
		maxUnits = amount;
		RefreshUIElement();
	}
	
	public float GetUnits(){
		return units;
	}
	
	public void RefreshUIElement(){
		if (fillbarUIElement){
			int fillbarUIElementSize = (fillbarUIElement.direction == FillBar.DrainDirection.LeftToRight || fillbarUIElement.direction == FillBar.DrainDirection.RightToLeft) ? 
				fillbarUIElement.texture.width : fillbarUIElement.texture.height;
			fillbarUIElement.clipAmount = fillbarUIElementSize - ((units/maxUnits) * fillbarUIElementSize);
		}
	}
}
