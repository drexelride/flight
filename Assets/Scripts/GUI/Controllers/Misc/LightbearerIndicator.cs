using UnityEngine;
using System.Collections;

public class LightbearerIndicator : MonoBehaviour {

	private Transform lightbearer, shadow;
	private GameObject[] shadowGameObjects;
	public GameObject indicator;
	private float angle;

	// Use this for initialization
	void Start () {
		angle = 0.0f;
	}
	
	// Update is called once per frame
	// continually update the indicator plane
	void Update () {
		// get transforms of lightbearer and shadow player
		GameObject lbGameObject = GameObject.FindWithTag("Player");
		if (lbGameObject){
			lightbearer = lbGameObject.transform;
		}
		shadowGameObjects = GameObject.FindGameObjectsWithTag("Shadow");
		foreach (GameObject g in  shadowGameObjects){
			if (g.networkView.isMine){
				shadow = g.transform;
			}
		}
		
		if (shadow && lightbearer){
			// find Lightbearer's relative position to the Shadow
			Vector3 localPoint = shadow.InverseTransformPoint(lightbearer.position);
			Vector2 lightbearerPoint = new Vector2(localPoint.x, localPoint.z);
			
			// calculate angle
			if (lightbearerPoint.y >= 0){
				angle = Vector2.Angle(-Vector2.right, lightbearerPoint)-90;
			}
			else{
				if (lightbearerPoint.x < 0){
					angle = -90;
				}
				else{
					angle = 90;
				}
			}

			// set angle of indicator plane
			indicator.transform.rotation = Quaternion.Euler(0, angle, 0);
		}
	}
}
