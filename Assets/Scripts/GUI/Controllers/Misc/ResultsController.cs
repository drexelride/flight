using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using System.Linq;

public class ResultsController : MonoBehaviour {

	public Menu results, lbHighScores, shadowHighScores;
	public MenuItem roachScore, watcherScore, anglerScore, hammerheadScore, sirenScore, beaconsLit, longestChain, shadowsBanished, rawScore, beaconBonus, finalScore, finalScoreLabel,
		roach, watcher, angler, hammerhead, siren, highScoreLabel, initialEntryLabel1, initialEntryLabel2, first1, middle1, last1, first2, middle2, last2, initialHighlighter1, initialHighlighter2,
		roachStar, watcherStar, anglerStar, hammerheadStar, sirenStar;
	public GUISkin resultsTextHighlighted;
	public FileIOScript highScoreIO;
	public bool highScore = false;
	public string initials1 = "AAA";
	public string initials2 = "AAA";
	private Dictionary<int, char> alphabet = new Dictionary<int, char> {
		{0, 'A'},
		{1, 'B'},
		{2, 'C'},
		{3, 'D'},
		{4, 'E'},
		{5, 'F'},
		{6, 'G'},
		{7, 'H'},
		{8, 'I'},
		{9, 'J'},
		{10, 'K'},
		{11, 'L'},
		{12, 'M'},
		{13, 'N'},
		{14, 'O'},
		{15, 'P'},
		{16, 'Q'},
		{17, 'R'},
		{18, 'S'},
		{19, 'T'},
		{20, 'U'},
		{21, 'V'},
		{22, 'W'},
		{23, 'X'},
		{24, 'Y'},
		{25, 'Z'},
		{26, ' '}
	};
	private int highlightedLetter1 = 1, highlightedLetter2 = 1, firstLetter1 = 0, secondLetter1 = 0, thirdLetter1 = 0,
		firstLetter2 = 0, secondLetter2 = 0, thirdLetter2 = 0;
	private bool scrolling1 = false, scrolling2 = false, showShadowArrows = false;
	public GUISkin upArrow, downArrow;
	
	private bool fadeResults;
	private Color originalResultsSkinColor, originalHighlightedResultsSkinColor, fadingResultsSkinColor, fadingResultsHighlightedSkinColor;
	private bool fadingResultsSkinColorExists = false;
	private bool fadeHighScores;
	public ResultsTimerEndAction timerAction;
	public Timer timer;
	public int shadowScoreboardsUpdated;
	public bool scoreboardsLoaded = false;
	private bool calledSwitchToHighScores = false;
	public int numShadowHighScores = 0;
	public bool numShadowHighScoresTotaled = false;
	public bool endResults = false;
	
	public Texture2D notJoinedScreen;

	// Use this for initialization
	void Start () {
		if (Network.isServer)
			MusicController.GetInstance().PlayBGM1();
	
		shadowScoreboardsUpdated = 0;
	
		// store data for fading text
		fadeResults = false;
		fadeHighScores = false;
		if (results && results.menuItems[0] && results.menuItems[0].guiSkin){
			Color orig = results.menuItems[0].guiSkin.label.normal.textColor;
			originalResultsSkinColor = new Color(orig.r, orig.g, orig.b, orig.a);
			Color origHighlighted = resultsTextHighlighted.label.normal.textColor;
			originalHighlightedResultsSkinColor = new Color(origHighlighted.r, origHighlighted.g, origHighlighted.b, origHighlighted.a); 
			fadingResultsSkinColor = new Color(originalResultsSkinColor.r, originalResultsSkinColor.g, originalResultsSkinColor.b, originalResultsSkinColor.a);
			fadingResultsHighlightedSkinColor = new Color(originalHighlightedResultsSkinColor.r, originalHighlightedResultsSkinColor.g,
				originalHighlightedResultsSkinColor.b, originalHighlightedResultsSkinColor.a);
			fadingResultsSkinColorExists = true;
			foreach (MenuItem m in results.menuItems){
				if (!m.overrideOutlineColor){
					m.outlineROverride = results.defaultOutlineR;
					m.outlineGOverride = results.defaultOutlineG;
					m.outlineBOverride = results.defaultOutlineB;
					m.overrideOutlineColor = true;
				}
			}
		}
		if (lbHighScores){
			lbHighScores.on = false;
			foreach (MenuItem m in lbHighScores.menuItems){
				if (!m.overrideOutlineColor){
					m.outlineROverride = results.defaultOutlineR;
					m.outlineGOverride = results.defaultOutlineG;
					m.outlineBOverride = results.defaultOutlineB;
					m.overrideOutlineColor = true;
				}
			}
		}
		if (shadowHighScores){
			shadowHighScores.on = false;
			foreach (MenuItem m in shadowHighScores.menuItems){
				if (!m.overrideOutlineColor){
					m.outlineROverride = results.defaultOutlineR;
					m.outlineGOverride = results.defaultOutlineG;
					m.outlineBOverride = results.defaultOutlineB;
					m.overrideOutlineColor = true;
				}
			}
		}
	
		// determine any high scores
		if (Network.isServer){
			List<string> existingLbScores, existingShadowScores;
			existingLbScores = highScoreIO.GetScores("lightbearer");
			existingShadowScores = highScoreIO.GetScores("shadows");
			List<int> currentShadowScores = new List<int>();
			if (existingShadowScores != null){
				foreach (string shadowScore in existingShadowScores){
					currentShadowScores.Add(Convert.ToInt32(shadowScore.Split(' ')[1]));
				}
			}
			
			// check for lightbearer high score
			if (existingLbScores != null){
				if (existingLbScores.Count < highScoreIO.maxNumScores){
					highScore = true;
					if (highScoreLabel)
						highScoreLabel.visible = true;
					if (initialEntryLabel1)
						initialEntryLabel1.visible = true;
					if (initialEntryLabel2)
						initialEntryLabel2.visible = true;
					if (first1)
						first1.visible = true;
					if (middle1)
						middle1.visible = true;
					if (last1)
						last1.visible = true;
					if (first2)
						first2.visible = true;
					if (middle2)
						middle2.visible = true;
					if (last2)
						last2.visible = true;
					if (initialHighlighter1)
						initialHighlighter1.visible = true;
					if (initialHighlighter2)
						initialHighlighter2.visible = true;
				}
				else{
					int lowestTotalScore = Convert.ToInt32(existingLbScores[existingLbScores.Count-1].Split(' ')[1]);
					if (GameResults.lbFinalScore > lowestTotalScore){
						highScore = true;
						if (highScoreLabel)
							highScoreLabel.visible = true;
						if (initialEntryLabel1)
							initialEntryLabel1.visible = true;
						if (initialEntryLabel2)
							initialEntryLabel2.visible = true;
						if (first1)
							first1.visible = true;
						if (middle1)
							middle1.visible = true;
						if (last1)
							last1.visible = true;
						if (first2)
							first2.visible = true;
						if (middle2)
							middle2.visible = true;
						if (last2)
							last2.visible = true;
						if (initialHighlighter1)
							initialHighlighter1.visible = true;
						if (initialHighlighter2)
							initialHighlighter2.visible = true;
					}
				}
			}
			else{
				highScore = true;
				if (highScoreLabel)
					highScoreLabel.visible = true;
				if (initialEntryLabel1)
					initialEntryLabel1.visible = true;
				if (initialEntryLabel2)
					initialEntryLabel2.visible = true;
				if (first1)
					first1.visible = true;
				if (middle1)
					middle1.visible = true;
				if (last1)
					last1.visible = true;
				if (first2)
					first2.visible = true;
				if (middle2)
					middle2.visible = true;
				if (last2)
					last2.visible = true;
				if (initialHighlighter1)
					initialHighlighter1.visible = true;
				if (initialHighlighter2)
					initialHighlighter2.visible = true;
			}
			
			// check for shadow high scores in order of shadow scores
			Dictionary<string, int> shadowScores = new Dictionary<string, int>();
			if (GameResults.shadowsInGame.Contains("Roach"))
				shadowScores.Add("Roach", GameResults.roachScore);
			if (GameResults.shadowsInGame.Contains("Watcher"))
				shadowScores.Add("Watcher", GameResults.watcherScore);
			if (GameResults.shadowsInGame.Contains("Angler"))
				shadowScores.Add("Angler", GameResults.anglerScore);
			if (GameResults.shadowsInGame.Contains("Hammerhead"))
				shadowScores.Add("Hammerhead", GameResults.hammerheadScore);
			if (GameResults.shadowsInGame.Contains("Siren"))
				shadowScores.Add("Siren", GameResults.sirenScore);
			IOrderedEnumerable<KeyValuePair<string, int>> sortedScores = shadowScores.OrderByDescending(pair => pair.Value);
			int highestGameScore = 0; // used in determining which Shadow(s) have a star for highest score in the game
			foreach (KeyValuePair<string, int> score in sortedScores){
				//Debug.Log(score.Key + " " + score.Value);
				if (currentShadowScores.Count < highScoreIO.maxNumScores){
					networkView.RPC("SetClientHighScore", Spawner.shadowNetworkPlayers[score.Key], true);
					numShadowHighScores++;
					currentShadowScores.Add(score.Value);
					currentShadowScores.Sort();
					currentShadowScores.Reverse();
				}
				else{
					int lowest = currentShadowScores[currentShadowScores.Count-1];
					if (score.Value > lowest){
						networkView.RPC("SetClientHighScore", Spawner.shadowNetworkPlayers[score.Key], true);
						numShadowHighScores++;
						currentShadowScores[currentShadowScores.Count-1] = score.Value;
						currentShadowScores.Sort();
						currentShadowScores.Reverse();
					}
				}
				
				// keep track of high score in this game
				if (score.Value > highestGameScore){
					highestGameScore = score.Value;
				}
			}
			
			// show stars next to Shadow scores as needed
			if (GameResults.shadowsInGame.Contains("Roach") && GameResults.roachScore >= highestGameScore)
				networkView.RPC("DisplayStar", RPCMode.All, "Roach");
			if (GameResults.shadowsInGame.Contains("Watcher") && GameResults.watcherScore >= highestGameScore)
				networkView.RPC("DisplayStar", RPCMode.All, "Watcher");
			if (GameResults.shadowsInGame.Contains("Angler") && GameResults.anglerScore >= highestGameScore)
				networkView.RPC("DisplayStar", RPCMode.All, "Angler");
			if (GameResults.shadowsInGame.Contains("Hammerhead") && GameResults.hammerheadScore >= highestGameScore)
				networkView.RPC("DisplayStar", RPCMode.All, "Hammerhead");
			if (GameResults.shadowsInGame.Contains("Siren") && GameResults.sirenScore >= highestGameScore)
				networkView.RPC("DisplayStar", RPCMode.All, "Siren");
			
			numShadowHighScoresTotaled = true;
		}
	
		if (Spawner.character == "Lightbearer" && finalScore && finalScoreLabel){
			finalScore.overrideOutlineColor = true;
			finalScoreLabel.overrideOutlineColor = true;
			finalScore.guiSkin = resultsTextHighlighted;
			finalScoreLabel.guiSkin = resultsTextHighlighted;
			finalScore.outlineROverride = 0;
			finalScore.outlineGOverride = 0;
			finalScore.outlineBOverride = 0;
			finalScoreLabel.outlineROverride = 0;
			finalScoreLabel.outlineGOverride = 0;
			finalScoreLabel.outlineBOverride = 0;			
		}
	
		if (roachScore && roach){
			roachScore.text = GameResults.roachScore+"";
			roachScore.visible = GameResults.shadowsInGame.Contains("Roach");
			roach.visible = GameResults.shadowsInGame.Contains("Roach");
			if (Spawner.character == "Roach"){
				roach.overrideOutlineColor = true;
				roachScore.overrideOutlineColor = true;
				roach.guiSkin = resultsTextHighlighted;
				roachScore.guiSkin = resultsTextHighlighted;
				roach.outlineROverride = 0;
				roach.outlineGOverride = 0;
				roach.outlineBOverride = 0;
				roachScore.outlineROverride = 0;
				roachScore.outlineGOverride = 0;
				roachScore.outlineBOverride = 0;
			}
		}
		if (watcherScore && watcher){
			watcherScore.text = GameResults.watcherScore+"";
			watcherScore.visible = GameResults.shadowsInGame.Contains("Watcher");
			watcher.visible = GameResults.shadowsInGame.Contains("Watcher");
			if (Spawner.character == "Watcher"){
				watcher.overrideOutlineColor = true;
				watcherScore.overrideOutlineColor = true;
				watcher.guiSkin = resultsTextHighlighted;
				watcherScore.guiSkin = resultsTextHighlighted;
				watcher.outlineROverride = 0;
				watcher.outlineGOverride = 0;
				watcher.outlineBOverride = 0;
				watcherScore.outlineROverride = 0;
				watcherScore.outlineGOverride = 0;
				watcherScore.outlineBOverride = 0;
			}
		}
		if (anglerScore && angler){
			anglerScore.text = GameResults.anglerScore+"";
			anglerScore.visible = GameResults.shadowsInGame.Contains("Angler");
			angler.visible = GameResults.shadowsInGame.Contains("Angler");
			if (Spawner.character == "Angler"){
				angler.overrideOutlineColor = true;
				anglerScore.overrideOutlineColor = true;
				angler.guiSkin = resultsTextHighlighted;
				anglerScore.guiSkin = resultsTextHighlighted;
				angler.outlineROverride = 0;
				angler.outlineGOverride = 0;
				angler.outlineBOverride = 0;
				anglerScore.outlineROverride = 0;
				anglerScore.outlineGOverride = 0;
				anglerScore.outlineBOverride = 0;
			}
		}
		if (hammerheadScore && hammerhead){
			hammerheadScore.text = GameResults.hammerheadScore+"";
			hammerheadScore.visible = GameResults.shadowsInGame.Contains("Hammerhead");
			hammerhead.visible = GameResults.shadowsInGame.Contains("Hammerhead");
			if (Spawner.character == "Hammerhead"){
				hammerhead.overrideOutlineColor = true;
				hammerheadScore.overrideOutlineColor = true;
				hammerhead.guiSkin = resultsTextHighlighted;
				hammerheadScore.guiSkin = resultsTextHighlighted;
				hammerhead.outlineROverride = 0;
				hammerhead.outlineGOverride = 0;
				hammerhead.outlineBOverride = 0;
				hammerheadScore.outlineROverride = 0;
				hammerheadScore.outlineGOverride = 0;
				hammerheadScore.outlineBOverride = 0;
			}
		}
		if (sirenScore && siren){
			sirenScore.text = GameResults.sirenScore+"";
			sirenScore.visible = GameResults.shadowsInGame.Contains("Siren");
			siren.visible = GameResults.shadowsInGame.Contains("Siren");
			if (Spawner.character == "Siren"){
				siren.overrideOutlineColor = true;
				sirenScore.overrideOutlineColor = true;
				siren.guiSkin = resultsTextHighlighted;
				sirenScore.guiSkin = resultsTextHighlighted;
				siren.outlineROverride = 0;
				siren.outlineGOverride = 0;
				siren.outlineBOverride = 0;
				sirenScore.outlineROverride = 0;
				sirenScore.outlineGOverride = 0;
				sirenScore.outlineBOverride = 0;
			}
		}
		if (beaconsLit)
			beaconsLit.text = GameResults.lbBeaconsLit + "/50";
		if (longestChain)
			longestChain.text = GameResults.lbChain+"";
		if (shadowsBanished)
			shadowsBanished.text = GameResults.lbShadowsBanished+"";
		if (rawScore)
			rawScore.text = GameResults.lbRawScore+"";
		if (beaconBonus)
			beaconBonus.text = GameResults.lbBeaconBonus+"x";
		if (finalScore)
			finalScore.text = GameResults.lbFinalScore+"";
	}
	
	// Update is called once per frame
	void Update () {
	
		// check for input for changing initials only before the results timer runs out
		if (!fadeResults && Network.isServer){
			GetInitialsInput();
		}
	
		// fade out results, fade in high scores
		if (fadeResults && results && fadingResultsSkinColorExists){
			initialHighlighter1.visible = false;
			initialHighlighter2.visible = false;
			showShadowArrows = false;
			fadingResultsSkinColor.a -= Time.deltaTime;
			fadingResultsHighlightedSkinColor.a -= Time.deltaTime;
			foreach (MenuItem m in results.menuItems){
				if (m.guiSkin != null){
					m.outlineAOverride -= Time.deltaTime;
					m.guiSkin.label.normal.textColor = (m.guiSkin.name == "ResultsTextHighlighted") ? fadingResultsHighlightedSkinColor : fadingResultsSkinColor;
					m.guiSkin.label.active.textColor = (m.guiSkin.name == "ResultsTextHighlighted") ? fadingResultsHighlightedSkinColor : fadingResultsSkinColor;
					m.guiSkin.label.hover.textColor = (m.guiSkin.name == "ResultsTextHighlighted") ? fadingResultsHighlightedSkinColor : fadingResultsSkinColor;
				}
			}
			if (fadingResultsSkinColor.a <= 0){
				fadeResults = false;
			
				if (Network.isServer && !scoreboardsLoaded) {
					LoadScoreBoards();
					scoreboardsLoaded = true;
				}
			
				results.on = false;
				foreach (MenuItem m in results.menuItems){
					m.guiSkin.label.normal.textColor = originalResultsSkinColor;
					m.guiSkin.label.active.textColor = originalResultsSkinColor;
					m.guiSkin.label.hover.textColor = originalResultsSkinColor;
					resultsTextHighlighted.label.normal.textColor = originalHighlightedResultsSkinColor;
					resultsTextHighlighted.label.active.textColor = originalHighlightedResultsSkinColor;
					resultsTextHighlighted.label.hover.textColor = originalHighlightedResultsSkinColor;
				}
				
				if (Network.isServer && lbHighScores){
					lbHighScores.guiSkin.label.normal.textColor = fadingResultsSkinColor;
					lbHighScores.guiSkin.label.active.textColor = fadingResultsSkinColor;
					lbHighScores.guiSkin.label.hover.textColor = fadingResultsSkinColor;
					foreach (MenuItem m in lbHighScores.menuItems){
						m.outlineAOverride = 0;
						if (m.guiSkin != null){
							m.guiSkin.label.normal.textColor = fadingResultsSkinColor;
							m.guiSkin.label.active.textColor = fadingResultsSkinColor;
							m.guiSkin.label.hover.textColor = fadingResultsSkinColor;
						}
					}
					lbHighScores.on = true;
				}
				else if (shadowHighScores){
					shadowHighScores.guiSkin.label.normal.textColor = fadingResultsSkinColor;
					shadowHighScores.guiSkin.label.active.textColor = fadingResultsSkinColor;
					shadowHighScores.guiSkin.label.hover.textColor = fadingResultsSkinColor;
					foreach (MenuItem m in shadowHighScores.menuItems){
						m.outlineAOverride = 0;
						if (m.guiSkin != null){
							m.guiSkin.label.normal.textColor = fadingResultsSkinColor;
							m.guiSkin.label.active.textColor = fadingResultsSkinColor;
							m.guiSkin.label.hover.textColor = fadingResultsSkinColor;
						}
					}
					shadowHighScores.on = true;
				}
				
				if (timer){
					timer.seconds = 10;
					timer.ForceRefresh();
				}
			}
		}
		else if (fadeHighScores){
			fadingResultsSkinColor.a += Time.deltaTime;
			if (Network.isServer && lbHighScores){
				lbHighScores.guiSkin.label.normal.textColor = fadingResultsSkinColor;
				lbHighScores.guiSkin.label.active.textColor = fadingResultsSkinColor;
				lbHighScores.guiSkin.label.hover.textColor = fadingResultsSkinColor;
				foreach (MenuItem m in lbHighScores.menuItems){
					m.outlineAOverride += Time.deltaTime;
					if (m.guiSkin != null){
						m.guiSkin.label.normal.textColor = fadingResultsSkinColor;
						m.guiSkin.label.active.textColor = fadingResultsSkinColor;
						m.guiSkin.label.hover.textColor = fadingResultsSkinColor;
					}
				}
			}
			else if (shadowHighScores){
				shadowHighScores.guiSkin.label.normal.textColor = fadingResultsSkinColor;
				shadowHighScores.guiSkin.label.active.textColor = fadingResultsSkinColor;
				shadowHighScores.guiSkin.label.hover.textColor = fadingResultsSkinColor;
				foreach (MenuItem m in shadowHighScores.menuItems){
					m.outlineAOverride += Time.deltaTime;
					if (m.guiSkin != null){
						m.guiSkin.label.normal.textColor = fadingResultsSkinColor;
						m.guiSkin.label.active.textColor = fadingResultsSkinColor;
						m.guiSkin.label.hover.textColor = fadingResultsSkinColor;
					}
				}
			}
			if (fadingResultsSkinColor.a >= 1){
				if (timer && timerAction){
					timerAction.resultsTimer = false;
					timer.StartCoroutine(timer.CountDown());
				}
				fadeHighScores = false;
			}
		}
		else if (Network.isServer && numShadowHighScoresTotaled && (Network.connections.Length == shadowScoreboardsUpdated) && !calledSwitchToHighScores && endResults){
			networkView.RPC("SetFadeHighScores", RPCMode.All, true);
			calledSwitchToHighScores = true;
		}
	}
	
	[RPC]
	private void SetFadeHighScores(bool setting){
		fadeHighScores = true;
	}
	
	[RPC]
	private void SetClientHighScore(bool gotHighScore) {
		highScore = gotHighScore;
		if (highScore && highScoreLabel && upArrow && downArrow){
			highScoreLabel.visible = true;
			if (first1 && middle1 && last1){
				first1.setLeftI(438);
				middle1.setLeftI(488);
				last1.setLeftI(538);
				first1.visible = true;
				middle1.visible = true;
				last1.visible = true;
				
				showShadowArrows = true;
			}
		}
	}
	
	void OnGUI(){
		if (showShadowArrows && (upArrow != null) && (downArrow != null)){
			GUI.skin = upArrow;
			if (GUI.Button(new Rect(444, 552, 36, 44), "")){
				firstLetter1 = (firstLetter1 >= 26) ? 0 : firstLetter1+1;
				UpdateInitialsDisplay();
			}
			if (GUI.Button(new Rect(494, 552, 36, 44), "")){
				secondLetter1 = (secondLetter1 >= 26) ? 0 : secondLetter1+1;
				UpdateInitialsDisplay();
			}
			if (GUI.Button(new Rect(544, 552, 36, 44), "")){
				thirdLetter1 = (thirdLetter1 >= 26) ? 0 : thirdLetter1+1;
				UpdateInitialsDisplay();
			}
			
			GUI.skin = downArrow;
			if (GUI.Button(new Rect(444, 648, 36, 44), "")){
				firstLetter1 = (firstLetter1 <= 0) ? 26 : firstLetter1-1;
				UpdateInitialsDisplay();
			}
			if (GUI.Button(new Rect(494, 648, 36, 44), "")){
				secondLetter1 = (secondLetter1 <= 0) ? 26 : secondLetter1-1;
				UpdateInitialsDisplay();
			}
			if (GUI.Button(new Rect(544, 648, 36, 44), "")){
				thirdLetter1 = (thirdLetter1 <= 0) ? 26 : thirdLetter1-1;
				UpdateInitialsDisplay();
			}
		}
		
		if (!Spawner.joined){
			GUI.depth = -5000;
			if (notJoinedScreen)
				GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), notJoinedScreen);
		}
	}
	
	public void SetFadeResults(bool fade){
		fadeResults = fade;
	}
	
	private void LoadScoreBoards(){
		// populate lightbearer high scores
		List<string> lbHighScoreList = highScoreIO.GetScores("lightbearer");
		if (lbHighScoreList != null){
			int count = 0;
			foreach (string score in lbHighScoreList){
				string[] scoreElements = score.Split(' ');
				if (lbHighScores){
					lbHighScores.menuItems[count+17].text = scoreElements[0];
					lbHighScores.menuItems[count+27].text = scoreElements[1];
					lbHighScores.menuItems[count+37].text = scoreElements[2];
					lbHighScores.menuItems[count+47].text = scoreElements[3];
					lbHighScores.menuItems[count+57].text = scoreElements[4];
				}
				count++;
				if (count > highScoreIO.maxNumScores-1)
					break;
			}
		}
		
		// populate shadow high scores
		List<string> shadowHighScoreList = highScoreIO.GetScores("shadows");
		if (shadowHighScoreList != null){
			string shadowHighScoresString = "";
			foreach (string score in shadowHighScoreList){
				shadowHighScoresString += score + '\n';
			}
			networkView.RPC("SetShadowHighScores", RPCMode.Others, shadowHighScoresString);
		}
	}
	
	[RPC]
	private void SetShadowHighScores(string shadowHighScoresString){
		int count = 0;
		string[] shadowHighScoreList = shadowHighScoresString.Split('\n');
		foreach (string score in shadowHighScoreList){
			if (score == "")
				break;
			string[] scoreElements = score.Split(' ');
			if (shadowHighScores){
				shadowHighScores.menuItems[count+17].text = scoreElements[0];
				shadowHighScores.menuItems[count+27].text = scoreElements[1];
				shadowHighScores.menuItems[count+37].text = scoreElements[2];
			}
			count++;
			if (count > highScoreIO.maxNumScores-1)
				break;
		}
		networkView.RPC("ConfirmShadowScoreboardUpdate", RPCMode.Server);
	}
	
	[RPC]
	private void ConfirmShadowScoreboardUpdate(){
		shadowScoreboardsUpdated++;
	}
	
	private void GetInitialsInput(){
		// driver initials input
		if (Input.GetAxis("Driver Initials Scroll") < -0.5){
			if (!scrolling1){
				if (highlightedLetter1 == 1){
					firstLetter1 = (firstLetter1 >= 26) ? 0 : firstLetter1+1;
				}
				else if (highlightedLetter1 == 2){
					secondLetter1 = (secondLetter1 >= 26) ? 0 : secondLetter1+1;
				}
				else{
					thirdLetter1 = (thirdLetter1 >= 26) ? 0 : thirdLetter1+1;
				}
				scrolling1 = true;
				UpdateInitialsDisplay();
			}
		}
		else if (Input.GetAxis("Driver Initials Scroll") > 0.5){
			if (!scrolling1){
				if (highlightedLetter1 == 1){
					firstLetter1 = (firstLetter1 <= 0) ? 26 : firstLetter1-1;
				}
				else if (highlightedLetter1 == 2){
					secondLetter1 = (secondLetter1 <= 0) ? 26 : secondLetter1-1;
				}
				else{
					thirdLetter1 = (thirdLetter1 <= 0) ? 26 : thirdLetter1-1;
				}
				scrolling1 = true;
				UpdateInitialsDisplay();
			}
		}
		else if (Input.GetAxis("Character Turn") > 0.1){
			if (!scrolling1){
				if (highlightedLetter1 >= 3){
					highlightedLetter1 = 1;
					if (initialHighlighter1)
						initialHighlighter1.setLeftI(initialHighlighter1.getLeftI()-100);
				}
				else{
					highlightedLetter1++;
					if (initialHighlighter1)
						initialHighlighter1.setLeftI(initialHighlighter1.getLeftI()+50);
				}
				scrolling1 = true;
			}
		}
		else if (Input.GetAxis("Character Turn") < -0.1){
			if (!scrolling1){
				if (highlightedLetter1 <= 1){
					highlightedLetter1 = 3;
					if (initialHighlighter1)
						initialHighlighter1.setLeftI(initialHighlighter1.getLeftI()+100);
				}
				else{
					highlightedLetter1--;
					if (initialHighlighter1)
						initialHighlighter1.setLeftI(initialHighlighter1.getLeftI()-50);
				}
				scrolling1 = true;
			}
		}
		else{
			scrolling1 = false;
		}
		
		// lighter initials input
		if (Wiimote.GetDPadUp(0)){
			if (!scrolling2){
				if (highlightedLetter2 == 1){
					firstLetter2 = (firstLetter2 >= 26) ? 0 : firstLetter2+1;
				}
				else if (highlightedLetter2 == 2){
					secondLetter2 = (secondLetter2 >= 26) ? 0 : secondLetter2+1;
				}
				else{
					thirdLetter2 = (thirdLetter2 >= 26) ? 0 : thirdLetter2+1;
				}
				scrolling2 = true;
				UpdateInitialsDisplay();
			}
		}
		else if (Wiimote.GetDPadDown(0)){
			if (!scrolling2){
				if (highlightedLetter2 == 1){
					firstLetter2 = (firstLetter2 <= 0) ? 26 : firstLetter2-1;
				}
				else if (highlightedLetter2 == 2){
					secondLetter2 = (secondLetter2 <= 0) ? 26 : secondLetter2-1;
				}
				else{
					thirdLetter2 = (thirdLetter2 <= 0) ? 26 : thirdLetter2-1;
				}
				scrolling2 = true;
				UpdateInitialsDisplay();
			}
		}
		else if (Wiimote.GetDPadRight(0)){
			if (!scrolling2){
				if (highlightedLetter2 >= 3){
					highlightedLetter2 = 1;
					if (initialHighlighter2)
						initialHighlighter2.setLeftI(initialHighlighter2.getLeftI()-100);
				}
				else{
					highlightedLetter2++;
					if (initialHighlighter2)
						initialHighlighter2.setLeftI(initialHighlighter2.getLeftI()+50);
				}
				scrolling2 = true;
			}
		}
		else if (Wiimote.GetDPadLeft(0)){
			if (!scrolling2){
				if (highlightedLetter2 <= 1){
					highlightedLetter2 = 3;
					if (initialHighlighter2)
						initialHighlighter2.setLeftI(initialHighlighter2.getLeftI()+100);
				}
				else{
					highlightedLetter2--;
					if (initialHighlighter2)
						initialHighlighter2.setLeftI(initialHighlighter2.getLeftI()-50);
				}
				scrolling2 = true;
			}
		}
		else{
			scrolling2 = false;
		}
	}
	
	private void UpdateInitialsDisplay(){
		initials1 = "" + alphabet[firstLetter1] + alphabet[secondLetter1] + alphabet[thirdLetter1];
		initials2 = "" + alphabet[firstLetter2] + alphabet[secondLetter2] + alphabet[thirdLetter2];
		if (first1 && middle1 && last1){
			first1.text = ""+initials1[0];
			middle1.text = ""+initials1[1];
			last1.text = ""+initials1[2];
		}
		if (first2 && middle2 && last2){
			first2.text = ""+initials2[0];
			middle2.text = ""+initials2[1];
			last2.text = ""+initials2[2];
		}
	}
	
	[RPC]
	void DisplayStar(string character){
		if (character == "Roach" && roachStar)
			roachStar.visible = true;
		else if (character == "Watcher" && watcherStar)
			watcherStar.visible = true;
		else if (character == "Angler" && anglerStar)
			anglerStar.visible = true;
		else if (character == "Hammerhead" && hammerheadStar)
			hammerheadStar.visible = true;
		else if (character == "Siren" && sirenStar)
			sirenStar.visible = true;
	}
}
