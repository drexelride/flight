using UnityEngine;
using System.Collections;

public class ShadowDamage : MonoBehaviour {

	public Texture2D damageTexture;
	public int left = 37;
	public int top = 138;
	public bool on = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI() {
		if (on)
			GUI.DrawTexture(new Rect(left, top, damageTexture.width, damageTexture.height), damageTexture);
	}
}
