using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class TitleController : MonoBehaviour {

	public Menu titleCard, lbHighScores, shadowHighScores;
	private bool showTitle; // maintain state of alternating title and high scores
	private float timer;
	private const float ONSCREEN_TIME = 10.0f; // how long the title and high scores remain on the screen in seconds
	public FileIOScript highScoreIO;
	private int titleStartY;
	public int titleBobAmp = 10;
	public int titleBobOmega = 2;
	
	public MenuItem joinButton;

	// Use this for initialization
	void Start () {
		if (Network.isServer)
			MusicController.GetInstance().PlayBGM1();
	
		if (Spawner.shadowStaticFieldsInitialized){
			// clear shadow data static fields
			if (Spawner.shadowNetworkPlayers != null)
				Spawner.shadowNetworkPlayers.Clear();
			if (!Network.isServer){
				Spawner.character = "";
			}
			else{
				ConnectionManager.undecidedClients.Clear(); // just in case
				foreach (NetworkPlayer n in Network.connections){
					ConnectionManager.undecidedClients.Add(n.guid);
				}
			}
			ShadowSelect.shadowsSelected.Clear();
			ShadowSelect.lockedIn = false;
			ShadowSelect.selectedCharacter = "Roach";
			Spawner.joined = false;
		}
	
		if (joinButton)
			joinButton.visible = !Network.isServer;
		Spawner.joined = Network.isServer;
	
		showTitle = true;
		timer = 0.0f;
		titleStartY = (titleCard && titleCard.menuItems[0]) ? titleCard.menuItems[0].getTopI() : 0;
		
		//highScoreIO.AddScore("LJM", 1000, "shadows", "Roach");
		
		if (lbHighScores)
			lbHighScores.on = Spawner.character == "Lightbearer";
		if (shadowHighScores)
			shadowHighScores.on = Spawner.character != "Lightbearer";
		
		// load and set high scores
		if (Network.isServer){
			// populate lightbearer high scores
			List<string> lbHighScoreList = highScoreIO.GetScores("lightbearer");
			if (lbHighScoreList != null){
				int count = 0;
				foreach (string score in lbHighScoreList){
					string[] scoreElements = score.Split(' ');
					if (lbHighScores){
						lbHighScores.menuItems[count+17].text = scoreElements[0];
						lbHighScores.menuItems[count+27].text = scoreElements[1];
						lbHighScores.menuItems[count+37].text = scoreElements[2];
						lbHighScores.menuItems[count+47].text = scoreElements[3];
						lbHighScores.menuItems[count+57].text = scoreElements[4];
					}
					count++;
					if (count > highScoreIO.maxNumScores-1)
						break;
				}
			}
			
			// populate shadow high scores
			List<string> shadowHighScoreList = highScoreIO.GetScores("shadows");
			if (shadowHighScoreList != null){
				string shadowHighScoresString = "";
				foreach (string score in shadowHighScoreList){
					shadowHighScoresString += score + '\n';
				}
				networkView.RPC("SetShadowHighScores", RPCMode.All, shadowHighScoresString);
			}
		}
	}
	
	void FixedUpdate () {
		// alternate title and high scores
		if (timer >= ONSCREEN_TIME){
			if (showTitle){
				if (titleCard){
					titleCard.xOffset -= 8;
				}
				if (lbHighScores && shadowHighScores){
					lbHighScores.xOffset -= 8;
					shadowHighScores.xOffset -= 8;
					if (lbHighScores.xOffset == 0){
						//titleCard.setLeftI(Screen.width+255);
						if (titleCard)
							titleCard.xOffset = Screen.width;
						showTitle = false;
						timer = 0.0f;
					}
				}
			}
			else{
				if (lbHighScores && shadowHighScores){
					lbHighScores.xOffset -= 8;
					shadowHighScores.xOffset -= 8;
				}
				if (titleCard){
					titleCard.xOffset -= 8;
					if (titleCard.xOffset == 0){
						if (lbHighScores && shadowHighScores){
							lbHighScores.xOffset = Screen.width;
							shadowHighScores.xOffset = Screen.width;
						}
						showTitle = true;
						timer = 0.0f;
					}
				}
			}
		}
		else{
			timer += Time.fixedDeltaTime;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetButtonDown(CharacterMovement.TOGGLE_CONTROLS) && Network.isServer){
			networkView.RPC("LoadScene", RPCMode.All, ConnectionManager.SHADOW_SELECT_SCENE);
		}
		
		// bob title card
		if (titleCard && titleCard.menuItems[0])
			titleCard.menuItems[0].setTopI((int)(titleStartY+titleBobAmp*Mathf.Sin(titleBobOmega*Time.time)));
	}
	
	[RPC]
	private void LoadScene(string sceneName) {
		Application.LoadLevel(sceneName);
	}
	
	[RPC]
	private void SetShadowHighScores(string shadowHighScoresString){
		int count = 0;
		string[] shadowHighScoreList = shadowHighScoresString.Split('\n');
		foreach (string score in shadowHighScoreList){
			if (score == "")
				break;
			string[] scoreElements = score.Split(' ');
			if (shadowHighScores){
				shadowHighScores.menuItems[count+17].text = scoreElements[0];
				shadowHighScores.menuItems[count+27].text = scoreElements[1];
				shadowHighScores.menuItems[count+37].text = scoreElements[2];
			}
			count++;
			if (count > highScoreIO.maxNumScores-1)
				break;
		}
	}
}
