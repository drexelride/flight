using UnityEngine;
using System.Collections;

public class GlowFade : MonoBehaviour {

	public Texture2D glow;
	private float alpha;
	public float fadeFactor = 0.5f;

	// Use this for initialization
	void Start () {
		alpha = 1.0f;
	}
	
	// Update is called once per frame
	void Update () {
		alpha -= Time.deltaTime*fadeFactor;
		if (alpha <= 0){
			Destroy(gameObject);
		}
	}
	
	void OnGUI(){
		Color color = GUI.color;
		color.a = alpha;
		GUI.color = color;
		GUI.depth = 200;
		GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), glow);
	}
}
