using UnityEngine;
using System.Collections;

public class Instructions : MonoBehaviour {
	public bool started = false;
	public Texture2D lb1, lb2, lb3, lb4, lb5, shadow1, shadow2Roach, shadow2Watcher, shadow2Angler, shadow2Hammerhead, shadow2Siren,
		ramText, blindText, grabText, slamText, screamText;
	public GUISkin instructionsSkin, shadow1InstructionsSkin, shadow2InstructionsSkin;
	public enum InstructionsState{
		Lightbearer1,
		Lightbearer2,
		Lightbearer3,
		Lightbearer4,
		Lightbearer5,
		Shadow1,
		Shadow2Roach,
		Shadow2Watcher,
		Shadow2Angler,
		Shadow2Hammerhead,
		Shadow2Siren
	};
	public InstructionsState state = InstructionsState.Lightbearer1;
	
	public float secondsPerLongSlide = 12.0f;
	public float secondsPerShortSlide = 7.0f;
	private float timer = 0.0f;
	private bool onLongSlide = false;
	private bool onShortSlide = false;
	
	private string lb1Text1 = "Greetings Lightbearer,";
	private string lb1Text2 = "\nThe darkness has returned and with it, the Shadows." +
		"You must stop them from taking over! Light the beacons placed around our home to ward off the darkness and bring back the light!";
	private string lb1Text3 = "\n\n\n\n\n\n-Elder";
	private string lb2Text1 = "Driver:\nTilt the left thumbstick on your controller left and right to turn. Hold down the left trigger to slow down.";
	private string lb2Text2 = "Lighter:\nAim at the screen with your controller to aim your light. Hold down the A button to turn the light on.";
	private string lb3Text = "Work together to light beacons. Shining light on them will cause them to fill up.";
	private string lb4Text = "Shadow creatures will try to stop you. You can banish them using your light.";
	private string lb5Text1 = "Lighting beacons fills the CHAIN GAUGE. Earn more points by lighting more beacons before it depletes.";
	private string lb5Text2 = "The RADAR shows you beacons and Shadows that are nearby.";
	private string lb5Text3 = "The LIGHT METER drains while your light is on and recharges otherwise.";
	private string shadow1Text1 = "Stop the Lightbearer from lighting beacons!";
	private string shadow1Text2 = "\n\nControl your character by TILTING your tablet.";
	private string shadow1Text3 = "\n\nFORWARD and BACK control speed.";
	private string shadow1Text4 = "\n\nLEFT and RIGHT control steering.";
	private string shadow1Text5 = "The Lightbearer's light drains your HEALTH.";
	private string shadow1Text6 = "The RADAR and POINTER can help you find the Lightbearer.";
	private string shadow1Text7 = "Your POWER must be full to attack.";
	private string shadow2GeneralText = "Tap the screen to";
	private string shadow2RoachText = "Ram the Lightbearer to earn points!";
	private string shadow2WatcherText = "Blind the Lightbearer with a cloud of darkness to earn points!";
	private string shadow2AnglerText = "Grab and throw the Lightbearer to earn points!";
	private string shadow2HammerheadText = "Slam the ground and spin the Lightbearer in midair to earn points!";
	private string shadow2SirenText = "Deafen and disoreient the Lightbearer to earn points!";
	
	private int shadowsReady = 0;
	
	public StartGameAction gameStarter;

	// Use this for initialization
	void Start () {
		if (Spawner.character == "Lightbearer")
			started = true;
		onLongSlide = true;
	}
	
	// Update is called once per frame
	void Update () {
		if (started){
			timer += Time.deltaTime;
			
			if ((onLongSlide && timer >= secondsPerLongSlide) || (onShortSlide && timer >= secondsPerShortSlide)){
				switch(state){
					case InstructionsState.Lightbearer1:
						state = InstructionsState.Lightbearer2;
						break;
					case InstructionsState.Lightbearer2:
						onLongSlide = false;
						onShortSlide = true;
						state = InstructionsState.Lightbearer3;
						break;
					case InstructionsState.Lightbearer3:
						state = InstructionsState.Lightbearer4;
						break;
					case InstructionsState.Lightbearer4:
						onLongSlide = true;
						onShortSlide = false;
						state = InstructionsState.Lightbearer5;
						break;
					case InstructionsState.Lightbearer5:
						break;
					case InstructionsState.Shadow1:
						onLongSlide = false;
						onShortSlide = true;
						if (Spawner.character == "Roach")
							state = InstructionsState.Shadow2Roach;
						else if (Spawner.character == "Watcher")
							state = InstructionsState.Shadow2Watcher;
						else if (Spawner.character == "Angler")
							state = InstructionsState.Shadow2Angler;
						else if (Spawner.character == "Hammerhead")
							state = InstructionsState.Shadow2Hammerhead;
						else if (Spawner.character == "Siren")
							state = InstructionsState.Shadow2Siren;
						break;
					default:
						Debug.Log("HERE");
						networkView.RPC("IncrementShadowsReady", RPCMode.Server);
						break;
				}
				timer = 0.0f;
			}
		}
	}
	
	[RPC]
	void IncrementShadowsReady(){
		shadowsReady++;
		if (shadowsReady >= Network.connections.Length){
			if (gameStarter){
				gameStarter.forceStart = false;
				gameStarter.Action();
			}
		}
	}
	
	void OnGUI(){
		if (started){
			GUI.depth = -1000;
			if (instructionsSkin)
				GUI.skin = instructionsSkin;
			switch(state){
				case InstructionsState.Lightbearer1:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), lb1);
					GUI.Label(new Rect(Screen.width/8, Screen.height/4, Screen.width/2-Screen.width/8, Screen.height-Screen.height/4), lb1Text1);
					GUI.Label(new Rect(Screen.width/2-Screen.width/4, Screen.height/4, Screen.width/2, Screen.height-Screen.height/4), lb1Text2);
					GUI.Label(new Rect(Screen.width/2+Screen.width/8, Screen.height/4, Screen.width/4, Screen.height-Screen.height/4), lb1Text3);
					break;
				case InstructionsState.Lightbearer2:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), lb2);
					GUI.Label(new Rect(Screen.width/16, 40, Screen.width/2-Screen.width/8, Screen.height/2-40), lb2Text1);
					GUI.Label(new Rect(Screen.width/2+Screen.width/16, 40, Screen.width/2-Screen.width/8, Screen.height/2-40), lb2Text2);
					break;
				case InstructionsState.Lightbearer3:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), lb3);
					GUI.Label(new Rect(Screen.width/8, 40, Screen.width-Screen.width/4, Screen.height/2-40), lb3Text);
					break;
				case InstructionsState.Lightbearer4:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), lb4);
					GUI.Label(new Rect(Screen.width/8, 40, Screen.width-Screen.width/4, Screen.height/2-40), lb4Text);
					break;
				case InstructionsState.Lightbearer5:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), lb5);
					GUI.Label(new Rect(Screen.width/32, Screen.height/3, Screen.width/3-Screen.width/16, Screen.height-Screen.height/3), lb5Text1);
					GUI.Label(new Rect(Screen.width/3+Screen.width/32, Screen.height/6, Screen.width/3-Screen.width/16, Screen.height-Screen.height/6), lb5Text2);
					GUI.Label(new Rect(Screen.width-Screen.width/3+Screen.width/32, Screen.height/3, Screen.width/3-Screen.width/16, Screen.height-Screen.height/3), lb5Text3);
					break;
				case InstructionsState.Shadow1:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), shadow1);
					GUI.Label(new Rect(0, 40, Screen.width, Screen.height/4), shadow1Text1);
					if (shadow1InstructionsSkin)
						GUI.skin = shadow1InstructionsSkin;
					GUI.Label(new Rect(Screen.width/64, 40, Screen.width/3-Screen.width/32, Screen.height/4), shadow1Text2);
					GUI.Label(new Rect(Screen.width/3+Screen.width/64, 40, Screen.width/3-Screen.width/32, Screen.height/4), shadow1Text3);
					GUI.Label(new Rect(Screen.width-Screen.width/3+Screen.width/64, 40, Screen.width/3-Screen.width/32, Screen.height/4), shadow1Text4);
					GUI.Label(new Rect(0, Screen.height-Screen.height/4, Screen.width/3, Screen.height/4), shadow1Text5);
					GUI.Label(new Rect(Screen.width/3, Screen.height-Screen.height/3-20, Screen.width/3, Screen.height/3+20), shadow1Text6);
					GUI.Label(new Rect(Screen.width-Screen.width/3, Screen.height-Screen.height/4, Screen.width/3, Screen.height/4), shadow1Text7);
					break;
				case InstructionsState.Shadow2Roach:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), shadow2Roach);
					GUI.Label(new Rect(0, Screen.height-120, Screen.width, Screen.height/4), shadow2RoachText);
					if (shadow2InstructionsSkin)
						GUI.skin = shadow2InstructionsSkin;
					GUI.Label(new Rect(256, 92, Screen.width/2, Screen.height/4), shadow2GeneralText);
					GUI.DrawTexture(new Rect(Screen.width/2-17, 37, ramText.width, ramText.height), ramText);
					break;
				case InstructionsState.Shadow2Watcher:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), shadow2Watcher);
					GUI.Label(new Rect(0, Screen.height-120, Screen.width, Screen.height/4), shadow2WatcherText);
					if (shadow2InstructionsSkin)
						GUI.skin = shadow2InstructionsSkin;
					GUI.Label(new Rect(180, 92, Screen.width/2, Screen.height/4), shadow2GeneralText);
					GUI.DrawTexture(new Rect(Screen.width/2-93, 37, blindText.width, blindText.height), blindText);
					
					break;
				case InstructionsState.Shadow2Angler:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), shadow2Angler);
					GUI.Label(new Rect(0, Screen.height-120, Screen.width, Screen.height/4), shadow2AnglerText);
					if (shadow2InstructionsSkin)
						GUI.skin = shadow2InstructionsSkin;
					GUI.Label(new Rect(199, 92, Screen.width/2, Screen.height/4), shadow2GeneralText);
					GUI.DrawTexture(new Rect(Screen.width/2-69, 37, grabText.width, grabText.height), grabText);
					break;
				case InstructionsState.Shadow2Hammerhead:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), shadow2Hammerhead);
					GUI.Label(new Rect(0, Screen.height-120, Screen.width, Screen.height/4), shadow2HammerheadText);
					if (shadow2InstructionsSkin)
						GUI.skin = shadow2InstructionsSkin;
					GUI.Label(new Rect(203, 92, Screen.width/2, Screen.height/4), shadow2GeneralText);
					GUI.DrawTexture(new Rect(Screen.width/2-65, 33, slamText.width, slamText.height), slamText);
					break;
				case InstructionsState.Shadow2Siren:
					GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), shadow2Siren);
					GUI.Label(new Rect(0, Screen.height-120, Screen.width, Screen.height/4), shadow2SirenText);
					if (shadow2InstructionsSkin)
						GUI.skin = shadow2InstructionsSkin;
					GUI.Label(new Rect(110, 92, Screen.width/2, Screen.height/4), shadow2GeneralText);
					GUI.DrawTexture(new Rect(Screen.width/2-163, 37, screamText.width, screamText.height), screamText);
					break;
			}
		}
	}
}
