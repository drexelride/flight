using UnityEngine;
using System.Collections;

public class NotJoinedBackground : MonoBehaviour {

	public Texture2D bg;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnGUI(){
		if (!Spawner.joined && !Network.isServer){
			GUI.depth = -1000;
			GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), bg);
		}
	}
}
