using UnityEngine;
using System.Collections;

class AccelerationSliders : Wrapper {
	
	public Vector3 accelMins = new Vector3(-25.0f, -25.0f, -25.0f);
	public Vector3 accelMaxs = new Vector3(25.0f, 25.0f, 25.0f);
	public Vector3 velocities = Vector3.zero;
	public Vector3 accels = Vector3.zero;
	public Vector2 rollBounds = new Vector2(-30.0f, 30.0f);
	public float roll = 0.0f;
	public Vector2 pitchBounds = new Vector2(-30.0f, 30.0f);
	public float pitch = 0.0f;
	public int width = 300;
	public int yOffset = 300;
	public int yPadding = 25;

	void Update() {
		velocities.x += accels.x * Time.deltaTime;
		velocities.y += accels.y * Time.deltaTime;
		velocities.z += accels.z * Time.deltaTime;
		simClient.packet.xLinVel = velocities.x;
		simClient.packet.yLinVel = velocities.y;
		simClient.packet.zLinVel = velocities.z;
		simClient.packet.pitchPos = pitch;
		simClient.packet.rollPos = roll;
	}
	
	void OnGUI() {
		int x = Screen.width/2 - width/2;
		int y = Screen.height/2 - yOffset;
		int labelWidth = 150;
		accels.x = GUI.HorizontalSlider(new Rect(x, y, width, 30), accels.x, accelMins.x, accelMaxs.x);
		GUI.Label(new Rect(x - labelWidth, y-8, labelWidth, 30), "X Acceleration: " + accels.x);
		y+=yPadding;
		accels.y = GUI.HorizontalSlider(new Rect(x, y, width, 30), accels.y, accelMins.y, accelMaxs.y);
		GUI.Label(new Rect(x - labelWidth, y-8, labelWidth, 30), "Y Acceleration: " + accels.y);
		y+=yPadding;
		accels.z = GUI.HorizontalSlider(new Rect(x, y, width, 30), accels.z, accelMins.z, accelMaxs.z);
		GUI.Label(new Rect(x - labelWidth, y-8, labelWidth, 30), "Z Acceleration: " + accels.z);
		y+=yPadding;
		roll = GUI.HorizontalSlider(new Rect(x, y, width, 30), roll, rollBounds.x, rollBounds.y);
		GUI.Label(new Rect(x - labelWidth, y-8, labelWidth, 30), "Roll Pos: " + roll);
		y+=yPadding;
		pitch = GUI.HorizontalSlider(new Rect(x, y, width, 30), pitch, pitchBounds.x, pitchBounds.y);
		GUI.Label(new Rect(x - labelWidth, y-8, labelWidth, 30), "Pitch Pos: " + pitch);
	}
}
