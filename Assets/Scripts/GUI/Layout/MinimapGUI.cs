using UnityEngine;
using System.Collections;

public class MinimapGUI : MonoBehaviour {

	public Texture LBAngler;
	public Texture LBHammerhead;
	public Texture LBLightbearer;
	public Texture LBRoach;
	public Texture LBSiren;
	public Texture LBWatcher;
	
	public Texture SAngler;
	public Texture SHammerhead;
	public Texture SLightbearer;
	public Texture SRoach;
	public Texture SSiren;
	public Texture SWatcher;
	
	public Texture smBeacon;
	public Texture smBeaconLit;
	public Texture laBeacon;
	public Texture laBeaconLit;
	
	public GameObject beacons;
	
	public const float MAX_DISTANCE = 250;
	public const float MM_LEFT_DISTANCE = 125;
	public const float MM_TOP_DISTANCE = 134;
	
	// All the players in the game, the last one being the main player
	// on this machine (character[5])!
	private GameObject[] characters;
	
	private Transform[] smallBeacons;
	private Transform[] largeBeacons;
	
	private bool isALightbearer;
	
	private Texture[] LBtextures;
	private Texture[] Stextures;
	private Texture[] wut2draw;
	
	private float lCenter;
	private float tCenter;

	// Use this for initialization
	void Start () {
		characters = new GameObject[6];
		smallBeacons = new Transform[200];
		largeBeacons = new Transform[24];
		
		LBtextures = new Texture[6];
		LBtextures[0] = LBAngler;
		LBtextures[1] = LBHammerhead;
		LBtextures[2] = LBWatcher;
		LBtextures[3] = LBRoach;
		LBtextures[4] = LBSiren;
		LBtextures[5] = LBLightbearer;
		
		Stextures = new Texture[6];
		Stextures[0] = SLightbearer;
		Stextures[1] = SAngler;
		Stextures[2] = SHammerhead;
		Stextures[3] = SWatcher;
		Stextures[4] = SRoach;
		Stextures[5] = SSiren;
		
		lCenter = Screen.width/2;
		tCenter = Screen.height;
		
		Transform[] tchildren = beacons.GetComponentsInChildren<Transform>();
		int i = 0;
		int j = 0;
		foreach(Transform tchild in tchildren) {
			if(tchild.name == "Beacon" || tchild.name == "FixedBeacon") {
				smallBeacons[i] = tchild;
				i++;
			} else if(tchild.name == "LargeBeacon") {
				largeBeacons[j] = tchild;
				j++;
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	}
	
	void OnGUI () {
		// Don't draw if you don't have a character to follow around :S
		if(characters[5]!=null) {
			// Be in front of everything else, because the minimap is SO important!
			GUI.depth = 0;
			
			// Choose which textures to draw
			if(isALightbearer) {
				wut2draw = LBtextures;
			} else {
				wut2draw = Stextures;
			}
			
			// First, draw the beacons
			for(int i = 0; i < smallBeacons.Length; i++) {
				if(smallBeacons[i] != null) {
					GameObject child = smallBeacons[i].gameObject;
					drawObject(child);
				}
			}
			
			for(int i = 0; i < largeBeacons.Length; i++) {
				if(largeBeacons[i] != null) {
					GameObject child = largeBeacons[i].gameObject;
					drawObject(child);
				}
			}
			
			// Then, draw the other players
			for(int i = 0; i < 5; i++) {
				if(characters[i]!=null) {
					drawObject(characters[i]);
				}
			}
			
			// Finally, draw yourself!
			GUI.DrawTexture(new Rect(lCenter-wut2draw[5].width/2,
				tCenter-wut2draw[5].height/2, wut2draw[5].width, wut2draw[5].height), wut2draw[5]);
		}
	}
	
	// Returns the drawing coordinates.  If the first value is 1337, then it shant be drawn!
	private float[] getDrawLoc(GameObject g) {
		float[] returnVals = new float[2];
		returnVals[0] = 1337;
		
		Vector3 dist1 = g.transform.position;
		dist1.y = 0;
		
		Vector3 dist2 = characters[5].transform.position;
		dist2.y = 0;
		
		float actualDist = Vector3.Distance(dist1, dist2);
		
		// If the max distance isn't met ...
		if(actualDist <= MAX_DISTANCE) {
			Vector3 dist3 = characters[5].transform.forward;
			dist3.y = 0;
			// ... Get the angle
			float tempAngle = Vector3.Angle((dist1-dist2), dist3);
			float direction = AngleDir(dist3, (dist1-dist2), Vector3.up);
			
			// And set return values!
			returnVals[0] = direction * Mathf.Cos((tempAngle+90) * Mathf.Deg2Rad) 
				* (MM_LEFT_DISTANCE * (actualDist / MAX_DISTANCE));
			returnVals[1] = Mathf.Sin((tempAngle+90) * Mathf.Deg2Rad) 
				* (MM_TOP_DISTANCE * (actualDist / MAX_DISTANCE));
		}
		
		return returnVals;
	}
	
	// Draws the passed object.  Must be in range.
	private void drawObject(GameObject g) {
		float[] tempVals = new float[2];
		switch(g.name) {
			case "Beacon":
			case "FixedBeacon":
				tempVals = getDrawLoc(g);
				if(g.GetComponent<BeaconBehaviour>().GetFilled() && tempVals[0] != 1337) {
					GUI.DrawTexture(new Rect(lCenter-tempVals[0]-12,tCenter-tempVals[1]-21,24,42),smBeaconLit);
				} else {
					GUI.DrawTexture(new Rect(lCenter-tempVals[0]-7,tCenter-tempVals[1]-16,14,32),smBeacon);
				}
				break;
			case "LargeBeacon":
				tempVals = getDrawLoc(g);
				if(g.GetComponent<BeaconBehaviour>().GetFilled() && tempVals[0] != 1337) {
					GUI.DrawTexture(new Rect(lCenter-tempVals[0]-20,tCenter-tempVals[1]-30,40,61),laBeaconLit);
				} else {
					GUI.DrawTexture(new Rect(lCenter-tempVals[0]-15,tCenter-tempVals[1]-25,30,51),laBeacon);
				}
				break;
			// In theory, the below SHOULD draw all types of units (lightbearer, creatures of darkness)
			default:
				tempVals = getDrawLoc(g);
				if(tempVals[0] != 1337) {
					int arrayVal = findInArray(g.name);
					if(arrayVal != -1)
						GUI.DrawTexture(new Rect(lCenter-tempVals[0]-wut2draw[arrayVal].width/4,
							tCenter-tempVals[1]-wut2draw[arrayVal].height/4,
							wut2draw[arrayVal].width/2, wut2draw[arrayVal].height/2), wut2draw[arrayVal]);
				}
				break;
		}
	}
	
	// Draws the beacons.
	private void drawBeacons(GameObject g) {
		foreach(Transform tchild in smallBeacons) {
			GameObject child = tchild.gameObject;
			drawObject(child);
		}
	}
	
	// Update whether you are the lightbearer or not from
	// a remote source
	public void isLB(bool LB) {
		isALightbearer = LB;
	}
	
	// Sets the character to be saved
	public void setCharacter(GameObject g, bool isMine) {
		if(isMine) {
			characters[5] = g;
			// Switch around the last shadow texture based on what it is
			Texture temp = Stextures[5];
			switch(g.name) {
				case "Angler":
					Stextures[5] = Stextures[1];
					Stextures[1] = temp;
					break;
				case "Hammerhead":
					Stextures[5] = Stextures[2];
					Stextures[2] = temp;
					break;
				case "Roach":
					Stextures[5] = Stextures[4];
					Stextures[4] = temp;
					break;
				case "Watcher":
					Stextures[5] = Stextures[3];
					Stextures[3] = temp;
					break;
				// Don't do anything if the Siren or Lightbearer
				default:
					break;
			}
		}
		else {
			for(int i = 0; i < 5; i++) {
				if(characters[i] == null) {
					characters[i] = g;
					i = 5;
				}
			}
		}
	}
	
	// Determine the angle direction
	private float AngleDir(Vector3 fwd, Vector3 targetDir, Vector3 up) {
		Vector3 perp = Vector3.Cross(fwd, targetDir);
		float dir = Vector3.Dot(perp, up);
		
		if (dir > 0f) {
			return 1f;
		} else if (dir < 0f) {
			return -1f;
		} else {
			return 0f;
		}
	}
	
	// Finds the array place of the texture you are looking for
	private int findInArray(string name) {
		string newName = name.Replace("(Clone)","");
		for(int i = 0; i < wut2draw.Length; i++) {
			if(wut2draw[i].name.Contains(newName))
				return i;
		}
		return -1;
	}
}
