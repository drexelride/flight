using UnityEngine;
using System.Collections;
using System;

public class YEEEEAAAAAAAAAAAAHCOINS : MonoBehaviour {

	public GameObject coin;
	public GameObject[] boundaries;
	public float generateInterval = 1.0f;
	public float coinStartHeight = 100.0f;

	// Use this for initialization
	void Start () {
		if (boundaries.Length == 2){
			StartCoroutine(MakeCoins());
		}
		else{
			Debug.Log("Can't create coins! There must be at exactly 2 boundary objects defined!");
		}
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	IEnumerator MakeCoins(){
		while (true){
			float x = UnityEngine.Random.Range(Math.Min(boundaries[0].transform.position.x, boundaries[1].transform.position.x), 
				Math.Max(boundaries[0].transform.position.x, boundaries[1].transform.position.x));
			float z = UnityEngine.Random.Range(Math.Min(boundaries[0].transform.position.x, boundaries[1].transform.position.x), 
				Math.Max(boundaries[0].transform.position.x, boundaries[1].transform.position.x));
			Instantiate(coin, new Vector3(x, coinStartHeight, z), Quaternion.identity);
			yield return new WaitForSeconds(generateInterval);
		}
	}
}
