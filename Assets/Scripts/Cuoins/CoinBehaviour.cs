using UnityEngine;
using System.Collections;

public class CoinBehaviour : MonoBehaviour {

	private float timer = 0;
	public float timeOut = 10.0f;
	private float downForce = 1.0f;
	public int pointValue = 1;

	// Use this for initialization
	void Start () {
		rigidbody.AddForce(Vector3.down * downForce);
		StartCoroutine(CheckDestroy());
	}
	
	IEnumerator CheckDestroy(){
		while (true){
			if (timer >= timeOut){
				Destroy(gameObject);
			}
			else{
				timer++;
			}
			yield return new WaitForSeconds(1.0f);
		}
	}
	
	void OnCollisionEnter(Collision collision){
		if (collision.gameObject.tag == "Player"){
			Destroy(gameObject);
		}
	}
}
