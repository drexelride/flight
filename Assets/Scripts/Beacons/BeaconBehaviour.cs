using UnityEngine;
using System.Collections;

public class BeaconBehaviour : MonoBehaviour {

	// public fields - various externally editable details
	public int quadrant; // ...yeah
	public float fillTime = 3.0f; // how long beacon takes to fill (in seconds)
	public float freezeTime = 2.0f; // how long beacon remains at current level if light not on it (in seconds)
	public float drainRate = 20.0f; // how many units per second the beacon begins losing after the freeze time
	public int pointValue = 100; // how many points this beacon is worth
	
	// private fields - state of fill, references to relevant UI elements
	private float currentFill = 0.0f; // how much the beacon is currently filled
	private const float FILL_MAX = 100.0f; // maximum fill level - how many units beacon needs to be considered filled
	private LightbearerScore lbScore;
	private LightbearerBeaconCount lbBeaconCount;
	private Chain chain;
	private ShadowBeaconCount sBeaconCount;
	
	// state booleans
	private bool filled = false; // whether this beacon is filled
	private bool filling = false; // whether the player's light is currently filling this beacon
	private bool draining = false; // whether this beacon is currently draining from lack of collision with light beam after not being filled
	private bool frozen = false; // whether this beacon is in the state between filling and draining
	private float frozenTime = 0.0f;
	private float movementTimer = 0.0f;
	
	public const int LIGHT_INTENSITY = 1;
	
	public Texture litTexture;
	
	private Material myMaterial;
	
	// points feedback display
	private Camera lbCamera;
	public GUISkin scoreSkin;
	private bool showPoints;
	private int beaconChainLevel = 0;
	private Vector3 pointsScreenPos;
	private Color purple, gold;
	private float pointsDisplayTimeCounter;
	private const float POINTS_DISPLAY_DIRECTION_CHANGE_TIME = 0.25f;
	private const float POINTS_DISPLAY_END_TIME = 2f;
	
	public ParticleSystem[] particles;
	
	public AudioClip fullSound;

	// Use this for initialization
	void Start () {
		lbScore = GameObject.Find("LightbearerScore").GetComponent<LightbearerScore>();
		lbBeaconCount = GameObject.Find("LightbearerBeaconCount").GetComponent<LightbearerBeaconCount>();
		chain = GameObject.Find("Chain").GetComponent<Chain>();
		sBeaconCount = GameObject.Find("ShadowBeaconCount").GetComponent<ShadowBeaconCount>();
	
		if (quadrant < 1 || quadrant > 4){
			Debug.Log("A beacon has an invalid quadrant! Please make sure the quadrant property is properly set for all beacons.");
		}
		MeshRenderer m = transform.Find("Obelisk").gameObject.GetComponent<MeshRenderer>();
		myMaterial = m.material;
		myMaterial.SetFloat("_YTop", transform.Find("Top").position.y);
		myMaterial.SetFloat("_YBottom", transform.Find("Bottom").position.y);
		
		GameObject lbGameObject = GameObject.FindWithTag("Player");
		if (lbGameObject){
			lbCamera = lbGameObject.transform.Find("Camera").GetComponent<Camera>();
		}
		showPoints = false;
		gold = new Color(212.0f/255.0f, 192.0f/255.0f, 1.0f/255.0f, 0.5f);
		purple = new Color(110.0f/255.0f, 0, 218.0f/255.0f, 0.5f);
	}
	
	// Update is called once per frame
	void Update () {
		// Debug
		/*if (Input.GetKey("down")){
			if (!filled){
				draining = false;
				frozen = false;
				filling = true;
			}
		}
		else if (Input.GetKeyUp("down")){
			if (!filled){
				filling = false;
				frozenTime = 0.0f;
				frozen = true;
			}
		}*/
	
		if (!filled){
			if (filling){
				//Spin();
				// Debug.Log(currentFill + " filling"); // Debug
				Fill();
				AdjustShader();
			}
			else if (frozen){
				//Spin();
				// Debug.Log(currentFill + " frozen"); // Debug
				Freeze();
			}
			else if (draining){
				// Debug.Log(currentFill + " draining"); // Debug
				Drain();
				AdjustShader();
			}
			// Debug
			/*else {
				Debug.Log(currentFill + " empty!");
			}*/
		} else {
			Spin();
		}
		// Debug
		/*else{
			Debug.Log(currentFill + " filled!");
		}*/
		
		if (showPoints){
			pointsDisplayTimeCounter += Time.deltaTime;
			if (pointsDisplayTimeCounter > POINTS_DISPLAY_END_TIME){
				showPoints = false;
			}
			else if (pointsDisplayTimeCounter < POINTS_DISPLAY_DIRECTION_CHANGE_TIME){
				//pointsScreenPos.y -= (Time.deltaTime*400)/(pointsDisplayTimeCounter*100);
				pointsScreenPos.y -= (Time.deltaTime/pointsDisplayTimeCounter)*5;
				purple.a += Time.deltaTime;
				gold.a += Time.deltaTime;
			}
			else{
				pointsScreenPos.y += Time.deltaTime*pointsDisplayTimeCounter*250;
				purple.a -= Time.deltaTime;
				gold.a -= Time.deltaTime;
			}
		}
	}
	
	private void AdjustShader() {
		myMaterial.SetFloat("_FillAmount", currentFill/100.0f);
	}
	
	private void Spin() {
		transform.Rotate(0, 20.0f * Time.deltaTime, 0);
		transform.Translate(0, Mathf.Sin(movementTimer) * 5.0f * Time.deltaTime, 0);
		movementTimer += Time.deltaTime;
		myMaterial.SetFloat("_YTop", transform.Find("Top").position.y);
		myMaterial.SetFloat("_YBottom", transform.Find("Bottom").position.y);
	}
	
	
	public void BeginLighting() {
		networkView.RPC("StartRPC", RPCMode.All);
	}
	
	
	public void StopLighting() {
		networkView.RPC("StopRPC", RPCMode.All);
	}
	
	[RPC]
	private void StartRPC() {
		if (!filled){
			if(Network.isServer && !audio.isPlaying) {
				audio.Play ();
			}
			draining = false;
			frozen = false;
			filling = true;
			particles[0].Stop();
			particles[0].Play();
			particles[1].Stop();
			particles[1].Play();
		}
	}
	
	[RPC]
	private void StopRPC() {
		if (!filled){
			filling = false;
			frozenTime = 0.0f;
			frozen = true;
			particles[0].Stop();
			particles[1].Stop();
		}
	}
	
	[RPC]
	void SwitchShader() {
		myMaterial.shader = Shader.Find("Custom/NoFog/Self-Illumin");
		myMaterial.SetTexture("_MainTex", litTexture);
	}
	
	// STATE BEHAVIOUR FUNCTIONS
	void Fill(){
		currentFill += (FILL_MAX / fillTime) * Time.deltaTime;
		Renderer beaconRenderer = transform.Find("Obelisk").renderer;
		float color = Mathf.Lerp(55F/255F, 1, currentFill/100.0f);
		beaconRenderer.material.color = new Color(color, color, color);
		Light pointLight = transform.Find("Light").GetComponent<Light>();
		pointLight.intensity = Mathf.Lerp(0, LIGHT_INTENSITY, currentFill/100.0f);
		if (currentFill >= FILL_MAX){
			particles[0].Stop();
			particles[1].Stop();
			if(Network.isServer) {
				audio.Stop ();
				audio.PlayOneShot (fullSound);
			}
			currentFill = FILL_MAX;
			filled = true;
			lbScore.AddToScore(pointValue);
			lbBeaconCount.AddToBeaconCount(1);
			if (lbCamera){
				pointsScreenPos = lbCamera.WorldToScreenPoint(gameObject.transform.Find("Top").gameObject.transform.position);
				pointsScreenPos.y = Screen.height - pointsScreenPos.y;
				if (pointsScreenPos.z > 23){
					pointsScreenPos.y -= 25;
				}
				else{
					pointsScreenPos.y = Screen.height/2-Screen.height/4;
				}
				pointsScreenPos.x -= 28;
				beaconChainLevel = chain.GetChain();
				showPoints = true;
			}
			chain.AddToChain(1);
			sBeaconCount.SubtractFromBeaconCount(1);
			//audio.Play();
			filling = false;
			networkView.RPC("SwitchShader", RPCMode.All);
		}
	}
	
	void Freeze(){
		frozenTime += Time.deltaTime;
		if (frozenTime >= freezeTime){
			frozen = false;
			draining = true;
			if(Network.isServer) {
				audio.Stop();
			}
		}
	}
	
	void Drain(){
		currentFill -= drainRate * Time.deltaTime;
		Renderer beaconRenderer = transform.Find("Obelisk").renderer;
		float color = Mathf.Lerp(55F/255F, 1, currentFill/100.0f);
		beaconRenderer.material.color = new Color(color, color, color);
		Light pointLight = transform.Find("Light").GetComponent<Light>();
		pointLight.intensity = Mathf.Lerp(0, LIGHT_INTENSITY, currentFill/100.0f);
		if (currentFill <= 0){
			currentFill = 0;
			draining = false;
			if(Network.isServer) {
				audio.Stop ();	
			}
		}
	}
	
	public bool GetFilled(){
		return filled;
	}
	
	public void DestroySelf(){
		networkView.RPC("DestroySelfRPC", RPCMode.All);
	}
	
	[RPC]
	void DestroySelfRPC(){
		Destroy(gameObject);
	}
	
	void OnGUI(){
		if (scoreSkin){
			GUI.skin = scoreSkin;
		}
		
		if (showPoints){
			GUI.depth = -100;
			int points = pointValue+(beaconChainLevel*chain.chainBonus);
			string pointsStr = "+"+points;

			Color outlineColor = purple;
			GUI.skin.label.normal.textColor = outlineColor;
			GUI.skin.label.hover.textColor = outlineColor;
			GUI.skin.label.active.textColor = outlineColor;
			
			GUI.Label(new Rect(pointsScreenPos.x+1, pointsScreenPos.y, 92, 30), pointsStr);
			GUI.Label(new Rect(pointsScreenPos.x+2, pointsScreenPos.y, 92, 30), pointsStr);
			GUI.Label(new Rect(pointsScreenPos.x-1, pointsScreenPos.y, 92, 30), pointsStr);
			GUI.Label(new Rect(pointsScreenPos.x, pointsScreenPos.y+1, 92, 30), pointsStr);
			GUI.Label(new Rect(pointsScreenPos.x, pointsScreenPos.y-1, 92, 30), pointsStr);
			
			GUI.skin.label.normal.textColor = gold;
			GUI.skin.label.hover.textColor = gold;
			GUI.skin.label.active.textColor = gold;
			GUI.Label(new Rect(pointsScreenPos.x, pointsScreenPos.y, 92, 30), pointsStr);
		}
	}
}
