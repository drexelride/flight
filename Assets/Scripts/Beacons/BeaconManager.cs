using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BeaconManager : MonoBehaviour {
	
	// how many beacons in each quadrant
	public int normalBeaconsPerQuadrant = 10;
	public int largeBeaconsPerQuadrant = 2;

	// lists of beacons by quadrant
	private List<GameObject> firstQuadrantNormalBeacons = new List<GameObject>();
	private List<GameObject> secondQuadrantNormalBeacons = new List<GameObject>();
	private List<GameObject> thirdQuadrantNormalBeacons = new List<GameObject>();
	private List<GameObject> fourthQuadrantNormalBeacons = new List<GameObject>();
	private List<GameObject> firstQuadrantLargeBeacons = new List<GameObject>();
	private List<GameObject> secondQuadrantLargeBeacons = new List<GameObject>();
	private List<GameObject> thirdQuadrantLargeBeacons = new List<GameObject>();
	private List<GameObject> fourthQuadrantLargeBeacons = new List<GameObject>();
	
	public const string BEACON_TAG = "Beacon";
	public const string NORMAL_BEACON_NAME = "Beacon";
	public const string LARGE_BEACON_NAME = "LargeBeacon";

	// Use this for initialization
	// Decide which beacons are on
	void Start () {
		if (Network.isServer){
			foreach(GameObject beacon in GameObject.FindGameObjectsWithTag(BEACON_TAG)){
				if (beacon.name == NORMAL_BEACON_NAME){
					if (beacon.GetComponent<BeaconBehaviour>().quadrant == 1){
						firstQuadrantNormalBeacons.Add(beacon);
					}
					else if (beacon.GetComponent<BeaconBehaviour>().quadrant == 2){
						secondQuadrantNormalBeacons.Add(beacon);
					}
					else if (beacon.GetComponent<BeaconBehaviour>().quadrant == 3){
						thirdQuadrantNormalBeacons.Add(beacon);
					}
					else if (beacon.GetComponent<BeaconBehaviour>().quadrant == 4){
						fourthQuadrantNormalBeacons.Add(beacon);
					}				
				}
				else if (beacon.name == LARGE_BEACON_NAME){
					if (beacon.GetComponent<BeaconBehaviour>().quadrant == 1){
						firstQuadrantLargeBeacons.Add(beacon);
					}
					else if (beacon.GetComponent<BeaconBehaviour>().quadrant == 2){
						secondQuadrantLargeBeacons.Add(beacon);
					}
					else if (beacon.GetComponent<BeaconBehaviour>().quadrant == 3){
						thirdQuadrantLargeBeacons.Add(beacon);
					}
					else if (beacon.GetComponent<BeaconBehaviour>().quadrant == 4){
						fourthQuadrantLargeBeacons.Add(beacon);
					}				
				}
			}
			
			
			// shuffle lists
			firstQuadrantNormalBeacons.Shuffle();
			secondQuadrantNormalBeacons.Shuffle();
			thirdQuadrantNormalBeacons.Shuffle();
			fourthQuadrantNormalBeacons.Shuffle();
			firstQuadrantLargeBeacons.Shuffle();
			secondQuadrantLargeBeacons.Shuffle();
			thirdQuadrantLargeBeacons.Shuffle();
			fourthQuadrantLargeBeacons.Shuffle();
			
			// destroy List.Count-normalBeaconsPerQuadrant beacons in each shuffled list
			for (int i=0; i<(firstQuadrantNormalBeacons.Count-normalBeaconsPerQuadrant); i++){
				firstQuadrantNormalBeacons[i].GetComponent<BeaconBehaviour>().DestroySelf();
			}
			for (int i=0; i<(secondQuadrantNormalBeacons.Count-normalBeaconsPerQuadrant); i++){
				secondQuadrantNormalBeacons[i].GetComponent<BeaconBehaviour>().DestroySelf();
			}
			for (int i=0; i<(thirdQuadrantNormalBeacons.Count-normalBeaconsPerQuadrant); i++){
				thirdQuadrantNormalBeacons[i].GetComponent<BeaconBehaviour>().DestroySelf();
			}
			for (int i=0; i<(fourthQuadrantNormalBeacons.Count-normalBeaconsPerQuadrant); i++){
				fourthQuadrantNormalBeacons[i].GetComponent<BeaconBehaviour>().DestroySelf();
			}
			
			// destroy List.Count-largeBeaconsPerQuadrant beacons in each shuffled list
			for (int i=0; i<(firstQuadrantLargeBeacons.Count-largeBeaconsPerQuadrant); i++){
				firstQuadrantLargeBeacons[i].GetComponent<BeaconBehaviour>().DestroySelf();
			}
			for (int i=0; i<(secondQuadrantLargeBeacons.Count-largeBeaconsPerQuadrant); i++){
				secondQuadrantLargeBeacons[i].GetComponent<BeaconBehaviour>().DestroySelf();
			}
			for (int i=0; i<(thirdQuadrantLargeBeacons.Count-largeBeaconsPerQuadrant); i++){
				thirdQuadrantLargeBeacons[i].GetComponent<BeaconBehaviour>().DestroySelf();
			}
			for (int i=0; i<(fourthQuadrantLargeBeacons.Count-largeBeaconsPerQuadrant); i++){
				fourthQuadrantLargeBeacons[i].GetComponent<BeaconBehaviour>().DestroySelf();
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

// list shuffler
static class MyExtensions{
	static readonly System.Random Random = new System.Random();
	public static void Shuffle<T>(this IList<T> list){
		int n = list.Count;
		while (n > 1){
			n--;
			int k = Random.Next(n + 1);
			T value = list[k];
			list[k] = list[n];
			list[n] = value;
		}
	}
}
