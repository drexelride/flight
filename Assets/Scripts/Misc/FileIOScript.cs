using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;

// File IO for scores

public class FileIOScript : MonoBehaviour {

	public int maxNumScores = 10;
	private string fileString = "_scores.txt";
	private string folderName = "scores";

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void AddScore (string initials, int score, string team, string shadowCharacter = "", int lbBeacons = 0, int lbChain = 0, int lbShadows = 0) {
		// call addscore to add a score to the file, and wipe out the last score if one is added
		string filePath = folderName + "\\" + team + fileString;
		string fullPath = System.IO.Path.GetFullPath(filePath);

		string scoreString = initials + " " + score.ToString();

		//Create the file and directory if it doesn't exist
		if (! File.Exists(fullPath) ) {
			Directory.CreateDirectory(folderName);
			using (FileStream fs = File.Create(fullPath)) {
				ASCIIEncoding  encoding = new System.Text.ASCIIEncoding();
				string tempString = "";

				if (team.CompareTo("lightbearer") == 0) {
					tempString = scoreString + " " + lbBeacons + " " + lbChain + " " + lbShadows;
				}
				else {
					tempString = scoreString + " " + shadowCharacter;
				}

				fs.Write(encoding.GetBytes(tempString), 0, encoding.GetByteCount(tempString));
			}
		}
		else {
			try {
				List<string> currentScores = GetScores(team);

				using (StreamWriter writer = new StreamWriter(fullPath)) {
					int i = 0;
					int numScores = 1;
					bool newHighScore = false;
					while (i < currentScores.Count && numScores <= maxNumScores) {
						string scoreEntry = currentScores[i];

						int entryScore = 0;
						Int32.TryParse( scoreEntry.Split(' ')[1], out entryScore );

						if (score > entryScore && !newHighScore) {
							newHighScore = true;
							i -= 1;
							if (team.CompareTo("lightbearer") == 0) {
								writer.WriteLine(scoreString + " " + lbBeacons + " " + lbChain + " " + lbShadows);
							}
							else {
								writer.WriteLine(scoreString + " " + shadowCharacter);
							}
						}
						else {
							writer.WriteLine(scoreEntry);
						}
						i+= 1;
						numScores += 1;
					}

					if (!newHighScore && i < maxNumScores) {
						if (team.CompareTo("lightbearer") == 0) {
								writer.WriteLine(scoreString + " " + lbBeacons + " " + lbChain + " " + lbShadows);
							}
							else {
								writer.WriteLine(scoreString + " " + shadowCharacter);
							}
					}
				}
			}
			catch (Exception e) {
				Debug.Log("File could not be opened: ");
				Debug.Log(e.Message);
			}
		}
		
		// reading files (getting score 0 returns first score)
	}
	
	public List<string> GetScores (string team) {
		List<string> scoreStringList = null;

		string filePath = folderName + "\\" + team + fileString;
		string fullPath = System.IO.Path.GetFullPath(filePath);
		
		try {
			string[] stringArray = File.ReadAllLines(fullPath);
			scoreStringList = new List<string>(stringArray);
		}
		catch{}
		/*catch (Exception e) {
			Debug.Log("File could not be read: ");
			Debug.Log(e.Message);
		}*/
		
		return scoreStringList;
	}	
}
