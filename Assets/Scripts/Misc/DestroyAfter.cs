using UnityEngine;
using System.Collections;

public class DestroyAfter : MonoBehaviour {

	public float waitTimer = 0.5f;

	// Use this for initialization
	void Start () {
		StartCoroutine(DestroyMe());
	}
	
	IEnumerator DestroyMe() {
		yield return new WaitForSeconds(waitTimer);
		Destroy(gameObject);
	}
}
