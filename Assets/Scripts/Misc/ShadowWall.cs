using UnityEngine;
using System.Collections;

public class ShadowWall : MonoBehaviour {
	
	public Transform mapCenter;
	public float showWallAmount = 300.0f;
	
	private GameObject myCharacter;
	private bool showingWall;

	// Use this for initialization
	void Start () {
		StartCoroutine (FindMyCharacter());
	}
	
	// Update is called once per frame
	void Update () {
		if(myCharacter) {
			Vector2 position = new Vector2(myCharacter.transform.position.x-mapCenter.position.x, myCharacter.transform.position.z-mapCenter.position.z);
			if((Mathf.Abs (position.x) >= showWallAmount || Mathf.Abs (position.y) >= showWallAmount)) {
				if(!showingWall) {
					showingWall = true;
					particleSystem.Stop ();
					particleSystem.Play ();

				}
			} else {
				if(showingWall) {
					showingWall = false;
					particleSystem.Stop ();
				}
			}
		}
	}
	
	private IEnumerator FindMyCharacter() {
		yield return new WaitForSeconds(2.0f);
		if(Network.isServer) {
			myCharacter = GameObject.FindWithTag ("Player");
		} else {
			GameObject[] shadows = GameObject.FindGameObjectsWithTag("Shadow");
			foreach(GameObject shadow in shadows) {
				if(shadow.networkView.isMine) {
					myCharacter = shadow;
					break;
				}
			}
		}
	}
}
