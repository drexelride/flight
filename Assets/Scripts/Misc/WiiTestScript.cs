using UnityEngine;
using System;
using System.Collections;
using System.Runtime.InteropServices;

public class WiiTestScript : MonoBehaviour {

	private Vector2 cursorPos = new Vector2(Screen.width/2, Screen.height/2);
	public Texture2D circleTexture;
	public float cursorSpeed = 1.0f;
	public bool useMouse = false;
	public bool useWiiRemote = false;
	private int primaryWiimote = 0;
	private float yOffset = (float) Screen.height;

	[DllImport ("UniWii")]
	private static extern void wiimote_start();
 
	[DllImport ("UniWii")]
	private static extern void wiimote_stop();
 
	[DllImport ("UniWii")]
	private static extern int wiimote_count();

	[DllImport ("UniWii")]	
	private static extern bool wiimote_enableIR( int which );

	[DllImport ("UniWii")]
	private static extern float wiimote_getIrX(int which);
	
	[DllImport ("UniWii")]
	private static extern float wiimote_getIrY(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonA(int which);

	[DllImport ("UniWii")]
	private static extern bool wiimote_getButtonB(int which);
	
	[DllImport ("UniWii")]	
	private static extern bool wiimote_isIRenabled(int which);
	//Last bit added by Colin to temp. fix compiling error

	// Use this for initialization
	void Start () {
		//Initialize Wiimote
		wiimote_start();

		//Enable IR
		if (useWiiRemote) {
			int c = wiimote_count();
			if (c > 0)
				wiimote_enableIR(primaryWiimote);
		}
	}
	
	// Update is called once per frame
	void Update () {
		MoveCursor();
	}

	void OnGUI() {
		//Wii-Remote Debug
		if (useWiiRemote) {
			String display;
			int c = wiimote_count();

			if (c > 0) {
				display = "Wii-Remote found!\n";
				wiimote_enableIR(primaryWiimote);
			}
			else
				display = "Press the '1' and '2' buttons on your Wii-Remote.";

			GUI.Label(new Rect(10, Screen.height-120, 500, 100), display);
		}

		//Adjust Height
		float height = 0F;
		if(cursorPos.y >= Screen.height/2) {
			height = Screen.height/2 - Mathf.Abs(Screen.height/2 - cursorPos.y);
		}
		else {
			height = Screen.height/2 + Mathf.Abs(Screen.height/2 - cursorPos.y);
		}

		//Draw Reticle
		GUI.DrawTexture(new Rect(cursorPos.x - 10, height - 10, 20, 20), circleTexture);

		//Debug Info
		String cursorPosString = "";
		cursorPosString += "( "+cursorPos.x +", "+cursorPos.y+" )";
		GUI.Label(new Rect(10, Screen.height-110, 500, 100), "Cursor Position: "+cursorPosString);
		GUI.Label(new Rect(10, Screen.height-100, 500, 100), "Wiimote IR Pos: "+"( "+wiimote_getIrX(primaryWiimote)+", "+wiimote_getIrY(primaryWiimote)+" )");
		if (wiimote_isIRenabled(primaryWiimote))
			GUI.Label(new Rect(10, Screen.height-90, 500, 100), "true");
		else
			GUI.Label(new Rect(10, Screen.height-90, 500, 100), "false");
	}

	private void MoveCursor() {
		if (useWiiRemote) {
			int c = wiimote_count();
			if (c > 0) {
				float wiiIRX = wiimote_getIrX(primaryWiimote);
				float wiiIRY = wiimote_getIrY(primaryWiimote) * -1.0f;

				if ( (wiiIRX != -100) && (wiiIRY != -100) ) {
					float tmpX = ((wiiIRX + (float) 1.0)/ (float)2.0) * (float) Screen.width;
				 	float tmpY = yOffset - (((wiiIRY + (float) 1.0)/ (float)2.0) * (float) Screen.height);
					tmpX = Mathf.RoundToInt(tmpX);
					tmpY = Mathf.RoundToInt(tmpY);
					cursorPos = new Vector2(tmpX, tmpY);
				}
			}
		}
		else if (useMouse) {
			cursorPos = Input.mousePosition;
		} 
		else {
			float horizontal = Input.GetAxis("Lightbearer Light Horizontal");
			float vertical = Input.GetAxis("Lightbearer Light Vertical");
			cursorPos += new Vector2(horizontal, -vertical) * cursorSpeed;
			cursorPos.x = Mathf.Clamp(cursorPos.x, 0, Screen.width);
			cursorPos.y = Mathf.Clamp(cursorPos.y, 0, Screen.height);
		}
	}

	void OnApplicationQuit() {
		wiimote_stop();
	}
}
