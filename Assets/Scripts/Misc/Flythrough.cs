using UnityEngine;
using System.Collections;

public class Flythrough : MonoBehaviour {
	
	public float moveSpeed = 20.0f;
	public bool fast = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if(Input.GetKey (KeyCode.LeftShift)) {
			fast = true;
		} else {
			fast = false;	
		}
		float currentSpeed = moveSpeed * (fast ? 2.0f : 1.0f) * Time.deltaTime;
		if(Input.GetKey(KeyCode.W)) {
			transform.Translate (0, 0, currentSpeed);	
		}
		if(Input.GetKey(KeyCode.S)) {
			transform.Translate (0, 0, -currentSpeed);	
		}
		if(Input.GetKey(KeyCode.D)) {
			transform.Translate (currentSpeed, 0, 0);	
		}
		if(Input.GetKey(KeyCode.A)) {
			transform.Translate (-currentSpeed, 0, 0);	
		}
		if(Input.GetKey (KeyCode.Space)) {
			transform.Translate (0, currentSpeed, 0);	
		}
		if(Input.GetKey (KeyCode.LeftControl)) {
			transform.Translate (0, -currentSpeed, 0);	
		}
	}
}
