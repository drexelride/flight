using UnityEngine;
using System.Collections;

public class TerrainManager : MonoBehaviour {

	public Terrain quadrant1;
	public Terrain quadrant2;
	public Terrain quadrant3;
	public Terrain quadrant4;

	// Use this for initialization
	void Start () {
		Vector3 size = quadrant1.collider.bounds.size;
		Vector3 quadrant1Pos = quadrant1.transform.position;
		Vector3 quadrant2Pos = new Vector3(quadrant1Pos.x - size.x, quadrant1Pos.y, quadrant1Pos.z);
		Vector3 quadrant3Pos = new Vector3(quadrant1Pos.x - size.x, quadrant1Pos.y, quadrant1Pos.z - size.z);
		Vector3 quadrant4Pos = new Vector3(quadrant1Pos.x, quadrant1Pos.y, quadrant1Pos.z - size.z);
		quadrant2.transform.position = quadrant2Pos;
		quadrant3.transform.position = quadrant3Pos;
		quadrant4.transform.position = quadrant4Pos;
	}
	
}
