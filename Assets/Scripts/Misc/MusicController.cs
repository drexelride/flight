using UnityEngine;
using System.Collections;

public class MusicController : MonoBehaviour {

	public AudioClip bgm1, bgm2, bgm3;
	public static string objectName;

	// Use this for initialization
	void Start () {
		objectName = gameObject.name;
		DontDestroyOnLoad(gameObject);
		audio.Stop();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void PlayBGM1(){
		if (!(audio.isPlaying && (audio.clip == bgm1))){
			audio.clip = bgm1;
			audio.loop = true;
			audio.Play();
		}
	}
	
	public void PlayBGM2(){
		if (!(audio.isPlaying && (audio.clip == bgm2))){
			audio.clip = bgm2;
			audio.loop = false;
			audio.Play();
		}
	}
	
	public void PlayBGM3(){
		if (!(audio.isPlaying && (audio.clip == bgm3))){
			audio.clip = bgm3;
			audio.loop = true;
			audio.Play();
		}
	}
	
	public void Stop(){
		audio.Stop();
	}
	
	public static MusicController GetInstance(){
		return GameObject.Find(objectName).GetComponent<MusicController>();
	}
}
