using UnityEngine;
using System.Collections;

public class NetworkManager_OLD : MonoBehaviour {
	// spawning shouldn't be part of the network manager
	/*public GameObject playerPrefab;
	public GameObject roachPrefab;
	public GameObject watcherPrefab;
	public Transform playerSpawnPoint;
	public Transform shadowSpawnPoint;
	*/
	
	public int numClients = 0;
	public int expectedClients = 5;
	
	private string gameName = "UniqueGameDemoCookie";
	private bool refreshing = false;
	private HostData[] hostData;
	
	private float btnX;
	private float btnY;
	private float btnW;
	private float btnH;

	// Use this for initialization
	void Start () {
		//DontDestroyOnLoad(gameObject);
		MasterServer.ipAddress = "144.118.204.16";
		MasterServer.port = 8889;
		hostData = MasterServer.PollHostList();
		btnX = Screen.width * 0.05F;
		btnY = Screen.height * 0.05F;
		btnW = Screen.width * 0.1F;
		btnH = Screen.width * 0.1F;
	}
	
	// Main functions
	void startServer () {
		Network.InitializeServer(32, 25002, !Network.HavePublicAddress());
		MasterServer.RegisterHost(gameName, "Flight Demo", "Demo Connection!");
	}
	
	void refreshHostList () {
		MasterServer.RequestHostList(gameName);
		refreshing = true;
	}
	
	void OnServerInitialized() {
		Debug.Log("Server Initialized!");
		//spawnPlayer();
	}
	
	void OnMasterServerEvent(MasterServerEvent msEvent) {
		if(msEvent == MasterServerEvent.RegistrationSucceeded) {
			Debug.Log("Server registered");
		}
	}
	
	void Update () {
		if(refreshing) {
			// Only populate list if not empty
			if(MasterServer.PollHostList().Length > 0) {
				refreshing = false;
				Debug.Log(MasterServer.PollHostList().Length);
				hostData = MasterServer.PollHostList();
			}
		}
	}
	
	void OnPlayerConnected(){
		numClients++;
		if (numClients >= expectedClients)
			networkView.RPC("LoadShadowSelect", RPCMode.All);
	}
	
	[RPC]
	void LoadShadowSelect(){
		Application.LoadLevel("ShadowSelect");
	}
	
	// GUI
	void OnGUI () {
		if(!Network.isClient && !Network.isServer) {
			// Connection GUI
			if(GUI.Button(new Rect(btnX, btnY, btnW * 2F, btnH), "Server Starter (vehicle only)")) {
				Spawner.character = "Lightbearer";
				Debug.Log("Starting the server");
				startServer();
			}
			
			if(GUI.Button(new Rect(btnX, btnY * 1.2F + btnH, btnW, btnH), "Refresher")) {
				Debug.Log("Refreshing ...");
				refreshHostList();
			}
			
			if(hostData.Length != 0) {
				for(int i = 0; i < hostData.Length; i++) {
					if(GUI.Button(new Rect(btnX * 1.5F + btnW, btnY * 1.2F + (btnH * i), btnW * 3F, btnH * 0.5F), hostData[i].gameName)) {
						Network.Connect(hostData[i]);
					}
				}
			}
		}
	}
}