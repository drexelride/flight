using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ConnectionManager : MonoBehaviour {

	public const string GAME_SCENE = "Game";
	public const string TITLE_SCENE = "Title";
	public const string SHADOW_SELECT_SCENE = "ShadowSelect";
	public const int PORT = 25000;

	public string connectToIP = "127.0.0.1";
	public int expectedClients = 5;
	
	private int numClients = 0;	
	private string state = "mode";
	public static bool soloMode = false;
	
	public static List<string> undecidedClients;
	
	public GUISkin guiSkin;

	void Start () {
		undecidedClients = new List<string>();
	}	
	
	//Host functions
	private void StartServer() {
		Network.InitializeServer(expectedClients, PORT, false);
	}
	
	void OnServerInitialized() {
		if(!soloMode) {
			state = "waitingForPlayers";
			Spawner.character = "Lightbearer";
		}
	}
	
	void OnPlayerConnected() {
		numClients++;
		if(numClients == expectedClients) {
			LoadShadowSelect();
		}
	}
	
	//Client functions
	private void JoinServer() {
		Network.Connect(connectToIP, PORT);
	}
	
	void OnConnectedToServer() {
		Debug.Log("CONNECTED TO SERVER");
		state = "waitingForServer";
	}
	
	void OnFailedToConnect() {
		Debug.LogError("FAILED TO CONNECT TO SERVER");
		state = "mode";
	}
	
	//Solo mode functions
	private void StartSoloMode(bool isLightbearer) {
		Spawner.character = isLightbearer ? "Lightbearer" : "Roach";
		soloMode = true;
		StartServer();
		if (isLightbearer)
			LoadScene(GAME_SCENE);
		else
			LoadScene(SHADOW_SELECT_SCENE);
	}
	
	private void LoadShadowSelect() {
		if (Network.isServer){
			foreach (NetworkPlayer n in Network.connections){
				undecidedClients.Add(n.guid);
			}
		}
		networkView.RPC("LoadScene", RPCMode.All, TITLE_SCENE);
	}
	
	[RPC]
	private void LoadScene(string sceneName) {
		Application.LoadLevel(sceneName);
	}
	
	void OnGUI () {
		if (guiSkin)
			GUI.skin = guiSkin;
	
		if(state == "mode") {
			int heightInc = Screen.height / 5;
			int widthInc = Screen.width / 4;
			int optionHeight = 20;
			Rect buttonRect = new Rect(Screen.width / 5, heightInc, 200, Screen.height / 6);
			Rect optionsRect = new Rect(buttonRect.x + widthInc, buttonRect.y + (buttonRect.height/2)-(optionHeight/2), Screen.width/5, optionHeight);
			
			//HOST
			if(GUI.Button(buttonRect, "Host\nExpected Clients: " + expectedClients)) {
				Debug.Log("START SERVER");
				state = "";
				StartServer();
			}
			expectedClients = (int)GUI.HorizontalSlider(optionsRect, expectedClients, 1F, 5F);
			
			//JOIN
			buttonRect.y += heightInc;
			optionsRect.y += heightInc;
			connectToIP = GUI.TextField(optionsRect, connectToIP, 15);
			if(GUI.Button(buttonRect, "Connect")) {
				Debug.Log("JOIN SERVER");
				state = "joining";
				JoinServer();
			}			
			
			//SOLO LIGHTBEARER
			buttonRect.y += heightInc;
			if(GUI.Button(buttonRect, "Solo (Lightbearer)")) {
				Debug.Log("SOLO LIGHTBEARER MODE");
				state = "";
				StartSoloMode(true);
			}
			
			// SOLO SHADOW
			buttonRect.x += widthInc;
			if(GUI.Button(buttonRect, "Solo (Shadow)")) {
				Debug.Log("SOLO SHADOW MODE");
				state = "";
				StartSoloMode(false);
			}
			
		} else if(state == "waitingForPlayers") {
			DrawWaitingBox("Waiting for players:\n" + numClients + "/" + expectedClients + " connected.");
		} else if(state == "waitingForServer") {
			DrawWaitingBox("Waiting for other players\nto connect...");
		} else if(state == "joining") {
			DrawWaitingBox("Trying to connect to server...");
		}
	}
	
	private void DrawWaitingBox(string message) {
		float boxWidth = Screen.width / 5;
		float boxHeight = Screen.height / 10;
		Rect messageRect = new Rect(Screen.width / 2 - boxWidth/2, Screen.height / 2 - boxHeight/2, boxWidth, boxHeight);
		GUI.Box(messageRect, message);
	}
}