Shader "Custom/gui/guage_shader" {
	Properties {
        _MainTex ("Base", 2D) = "white" {}
		_FillTex ("Fill Texture", 2D) = "black" {}
		_FillAmount ("Fill Amount", Range(0, 1.0)) = 0
		_YTop ("World Y Top", float) = 0
		_YBottom ("World Y Bottom", float) = 0
		_Illum ("Illumin (A)", 2D) = "white" {}
		_EmissionLM ("Emission (Lightmapper)", Float) = 0
		_Color ("Main Color", Color) = (1,1,1,1)
    }
    SubShader {
		Tags { "RenderType" = "Opaque" }
		CGPROGRAM
		#pragma surface surf Lambert
		struct Input {
			float2 uv_MainTex;
			float2 uv_Illum;
			float2 uv_FillTex;
			float3 worldPos;
		};
		sampler2D _MainTex;
		sampler2D _FillTex;
		sampler2D _Illum;
		fixed4 _Color;
		float _FillAmount;
		float _YTop;
		float _YBottom;
		
		void surf(Input IN, inout SurfaceOutput o) {
			float position = (IN.worldPos.y - _YBottom)/(_YTop - _YBottom);
			if(position <= _FillAmount) {
				fixed4 tex = tex2D(_FillTex, IN.uv_FillTex);
				fixed4 c = tex * _Color;
				o.Albedo = c.rgb;
				o.Emission = c.rgb * UNITY_SAMPLE_1CHANNEL(_Illum, IN.uv_Illum);
				o.Alpha = c.a;
			} else {
				o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
			}
		}
		ENDCG
	}
	Fallback "Diffuse"
}